﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class CareersCurrentOpeningDetails : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? CareersId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CareersId"] != null)
            {
                CareersId = Convert.ToInt32(Page.Request.QueryString["CareersId"]);
            }
            loadrecords();

        }
    }

    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.Careers
                           where p.CareersID==CareersId
                           select p;
            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["CareersId"] != null)))
        {
            CareersId = Convert.ToInt32(this.ViewState["CareersId"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["CareersId"] = CareersId;
        return (base.SaveViewState());
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       
    }
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
    protected void btnSubmitResume_Click(object sender, EventArgs e)
    {
        Response.Redirect("CareersSubmitResume.aspx");

    }
}
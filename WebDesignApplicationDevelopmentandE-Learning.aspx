﻿<%@ Page Title="Web Design, Application Development, and E-Learning. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="WebDesignApplicationDevelopmentandE-Learning.aspx.cs" Inherits="ExpertiseELearning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

<style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>

       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
  </script> 

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double stick-wrapper">
        <div class="row"> 
            <div id="side-sticky" class="col-md-6" >
               <div class="theiaStickySidebar"> <h1 class="bigh1">Web Design,<br /> Application Development,<br /> and E-Learning</h1>
                <div class="cs_button_holder">
                <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success" />--%>
                     <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
              </div>  
            </div>
            <div class="col-md-6" id="main-content">
                <p> Synergy designs attractive and efficient web sites and enterprise-level applications that streamline business processes. Our innovative solutions provide powerful custom-developed tools and interactive modules to manage data that guide policy and implementation decisions for our clients. Our approach is also comprehensive: Synergy’s web specialists provide complete design and development services—needs and requirements assessments, graphic design, development of wireframes, architecture design, user-interface development, and usability testing. </p>
               
      <!-- Start Collapse-->


           
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Web Design and Development</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p> Synergy designs and builds fully responsive Section 508-compliant sites and applications. We use the system development lifecycle process to develop web applications and offer high-availability hosting solutions in an operating environment with multiple, continuously monitored high-speed application and data servers. These servers meet exacting Authority-to-Operate specifications, and provide 99 percent uptime for our federal clients’ sites. </p>
                
                                    <p> Synergy develops reliable, data-driven applications that draw on multiple data sets. We determine the best formats and mediums to design, store, and secure data and develop the most effective way to integrate and visually interpret scientific and statistical information. Synergy’s IT professionals conduct gap analyses to determine how data can be restructured and designed for optimal project performance. Our data analysts work with our web designers and programmers to develop engaging and effective ways to present complex information. Examples of completed projects include:  </p>
                                        <ul>
                                            <li>2D animations that explain school safety statistics </li>
                                            <li>3D animations that explain neuroscience and drug abuse  </li>
                                            <li>Geographic information systems and mapping displays that use national data sources to describe complex socioeconomic, educational, and cultural interactions at a neighborhood level</li>
                                       
                                        </ul>
                                                                        
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Interactive E-Learning Programs</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p> Synergy’s information technology, communications, and doctoral-level instructional design experts use industry best practices to build state-of-the-art tools to teach skills and concepts in a variety of subject areas. Our blended approach integrates subject matter expertise with innovative technology and effective instructional design. We start with learner analysis, needs analysis, objective and assessment development, and media selection. Our subject matter experts and plain language writers help develop storyboards for each instructional step. We use multiplatform and multiscreen design techniques to develop engaging, customized information architecture plans for self-paced and timed courses. Our experts also ensure that download times are manageable, and that the privacy and security of module users are protected at all times. Our instructional developers work hand-in-hand with our internal user experience experts to provide the best possible look for each e-learning project we deliver. </p>
                
                     <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>
                        
                     
                    </div>
                </div>
            
       
<!--End Collapse -->
     
     
         <!--   <h3  class="expand"  onclick=" ExpandCollapse('list1');">Web Design and Development</h3>
              <div id="list1" style="display:none"> 
 			       <p>Synergy designs and builds fully responsive 508 compliant sites and applications. We use the system development life cycle process to develop Web applications and offer high-availability hosting solutions in an operating environment with multiple continuously monitored high-speed application and data servers that meet exacting Authority to Operate specifications for various federal agencies, and provide 99 percent uptime for client sites.</p>
                   <p>We develop reliable data-driven applications that draw on multiple data sets. We determine the best formats and mediums to design, store, and secure data and we develop the most effective way to integrate and visually interpret scientific and statistical information. Synergy’s IT professionals conduct gap analyses to determine how data need to be restructured and designed for optimal project performance. Our data analysts work with our web designers and programmers to develop engaging and effective ways to present complex information: 2D animations that explain school safety statistics; 3D animations that explain neuroscience and drug abuse; geographic information systems and mapping displays that use national data sources to describe complex socioeconomic, educational, and cultural interactions at a neighborhood level.</p>
              </div>             
			  
        <h3  class="expand"  onclick=" ExpandCollapse('list2');">Interactive E-Learning Programs</h3>
              <div id="list2" style="display:none"> 			
                <p>Our information technology, communication, and doctorate level instructional design experts use industry best practices to build state-of-the-art tools that teach key skills and concepts in a variety of subject areas. Our blended approach integrates deep subject matter expertise with innovative technology and effective instructional design. We start with learner analysis, needs analysis, objective and assessment development, and media selection. Our subject matter experts and plain language writers help develop storyboards for each instructional step. We use multiplatform and multiscreen design techniques when developing engaging, customized information architecture plans for self-paced and timed courses that often contain multiple modules. Our experts also ensure that download times are manageable, and that the privacy and security of module users are protected at all times. Our instructional developers work hand in hand with our internal user experience experts to provide the best possible look and for each e-learning project we deliver.</p>
              </div> -->
           </div>
        </div>
    </div>

    <div class="expertise_elearning_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p><span>Synergy blends IT experience, innovative design skills, and in-depth subject matter expertise to produce award-winning web portals and digital tools.</span></p>
         </div>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AwardsAndRecognition.aspx.cs" Inherits="AwardsRecognition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
<div class="container landing_padding_double">
	<div class="row awardsrow">
    	<div class="col-md-5">
				<h1 class="bigh1">Awards and Recognition</h1>     
        </div>
        <div class="col-md-6">

            <p>Synergy has received numerous prestigious awards and recognitions for outstanding work performance over the last decade. We have gained recognition for our creativity, innovative approach and excellent business practices from our peers, clients, and industry alike. 
                </p>
        </div>
        </div>
        <div class="row">
    	<div class="col-md-6">
        	<div class="award_contain">
            	<div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            	<div class="trophytext"><h3>Blue Pencil and Gold Screen Awards</h3>
                    <p>Synergy won three 2014 Blue Pencil and Gold Screen awards and six 2013 Blue Pencil and Gold Screen awards from the National Association of Government Communicators</p></div><div class="clear"></div>
            </div>
            <div class="award_contain">
            <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            	<div class="trophytext"><h3>AVA Digital Awards</h3>
                    <p>Synergy earned two 2014 AVA Digital Gold Awards for the development of educational videos.</p></div><div class="clear"></div>
            </div>
            <div class="award_contain">
            <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            	<div class="trophytext"><h3>Telly Awards</h3>
                    <p>Synergy won a 2012 Telly Award for five videos produced in collaboration with BlueSky Films and Abt Associates for the Substance Abuse and Mental Health Services Administration's 8th Annual Prevention Day.</p></div><div class="clear"></div>
            </div>
            <div class="award_contain">
            <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            	<div class="trophytext"><h3>GovCon Awards</h3>
                  <%--  <p>Synergy was nominated and named a top-five finalist for the 2010 GovCon Contractor of the Year and Executive of the Year Awards.</p>--%>
                    <p>
2010 GovCon Contractors of the Year, named as top five finalists.
     2010 GovCon Executive of the Year, named as top five finalists.
</p>
                    <h3>AWARDS FOR WRITING, TRAININGS, AND MEETINGS</h3>

                       <%-- <ul>
                            <li>5 National Institutes of Health Plain Language/Clear Communication Awards</li>
                            <li>4 Golden Quill Awards </li>
                            <li><i>EdPress&nbsp; </i>Distinguished Publication Award</li>
                            <li>Gold Hermes Creative Awards</li>
                            <li>West Virginia Commission on the Arts, Fellowship Award in Literature</li>
                            <li>4 American Society for Training and Development, President’s Award for Professional Excellence</li>
                            <li>Society of Government Meeting Professionals, Planner of the Month</li>
                            <li>DoDEA Shoestring Budget and Electronic Publication Awards</li>
                        </ul>--%>

                        <ul>
                            <li>Multiple NIH Plain Language/Clear Communication Awards</li>
                            <li>Golden Quill Awards</li>
                            <li><i>EdPress&nbsp; </i>Distinguished Publication Award</li>
                            <li>Gold Hermes Creative Awards</li>                           
                            <li>American Society for Training and Development, President’s Award for Professional Excellence</li>
                            <li>Society of Government Meeting Professionals, Planner of the Month Award</li>                            
                        </ul>


            	</div><div class="clear"></div>
            </div>
            
                    </div>
                    <div class="col-md-6">
                    <div class="award_contain">
                    <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            		<div class="trophytext"><h3>Small Business Administration</h3>
                    	<p>Synergy’s president was honored as the 2009 Washington Metropolitan Area District Small Business Person of the Year by the Small Business Administration.</p></div><div class="clear"></div>
                    </div>
                    <div class="award_contain">
                    <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            		<div class="trophytext"><h3>Montgomery County Council</h3>
                    <p>Synergy’s president was nominated and named a top-five finalist for the 2010 GovCon Executive of the Year Award.</p></div><div class="clear"></div>
                    </div>
                    <div class="award_contain">
                    <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            			<div class="trophytext"><h3>Washington Technology</h3>
                    <p>Synergy ranked 14 on <i>Washington Technology's</i> 2008 list of Top 25 Nationwide 8(a) Firms.</p></div><div class="clear"></div>
                    </div>
                    <div class="award_contain">
                    <div class="trophy"><img src="../images/trophy_temp.jpg"/></div>
            			<div class="trophytext"><h3>Outstanding Contribution Award</h3>
                    <p>Synergy’s president received an Outstanding Contribution Award by the National Association of Professional Asian American Women in 1994, 1996, 1997, 2000, and 2002.</p></div><div class="clear"></div>
                    </div>
            </div>
        </div>
    </div>

<div class="awards_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<p>Synergy’s success has many measures, including recognition by Federal agencies, national professional associations, and industry award panels. </p>
</div>



</asp:Content>


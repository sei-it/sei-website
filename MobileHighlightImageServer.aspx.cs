﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class MobileHighlightImageServer : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? HighlightsID;
    string imageName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["HighlightsID"] != null)
            {
                HighlightsID = Convert.ToInt32(Page.Request.QueryString["HighlightsID"]);
            }
        }
        GetImageName(HighlightsID);

        Response.ContentType = "image/jpeg"; // for JPEG file
        string physicalFileName = ConfigurationManager.AppSettings["ImagePath"].ToString();
        //string physicalFileName = @"C:\Users\vkothale\Documents\Visual Studio 2012\Projects\05_April_2015_projects\SEI_CMS\SEISiteAdmin\Docs\4e77ce52-eaa5-4d8b-8a2b-86026d1bf4a6-CSAP_desktop.jpg";
        physicalFileName = physicalFileName + imageName;
        Response.WriteFile(physicalFileName);


    }

    private void GetImageName(int? HighlightsID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.vw_HighlightsAndImages
                           where p.HighlightsID == HighlightsID
                           select p;
            foreach (var oCase in vCourses)
            {
                imageName = oCase.PhysicalFileName;
            }
        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["HighlightsID"] != null)))
        {
            HighlightsID = Convert.ToInt32(this.ViewState["HighlightsID"]);
        }


    }
    protected override object SaveViewState()
    {
        this.ViewState["HighlightsID"] = HighlightsID;

        return (base.SaveViewState());
    }


}
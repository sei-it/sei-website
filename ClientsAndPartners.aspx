﻿<%@ Page Title="Clients and Partners. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClientsAndPartners.aspx.cs" Inherits="ClientsPartners" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-4">
                <h1 class="bigh1">Clients and Partners</h1>
            </div>
            <div class="col-md-8">
               
                   <%--<p> Synergy helps clients in more than a dozen federal agencies. At each of these client agencies, our relationships have expanded into new offices, centers, and branches because we build partnerships that last. We form productive teaming relationships to ensure that each client—federal agency, nonprofit, or commercial—is supported by the expertise and innovation their important work deserves. Synergy’s clients and partners include:</p>
                   --%>
                <p>We form productive teaming relationships to ensure that each client—federal agency, nonprofit, or commercial—is supported by the expertise and innovation their important work deserves. Synergy helps clients in more than a dozen federal agencies. At each of these client agencies, our relationships have expanded into new offices, centers, and branches because we build partnerships that last. Synergy’s clients and partners include:</p>
                 <ul>
                        <li>Universities</li>
                        <li>Foundations and private organizations</li>
                        <li>Federal, state, and local health and education agencies</li>
                        <li>Community-based organizations</li>
                        <li>Treatment providers</li>
                        <li>The White House</li>
                    </ul>
               
               </div>
        </div>
    </div>

    <div class="clients_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
       <%-- <p>“Since 2004, Synergy has consistently provided a high level of customer service and flawless products to NIDA’s Director and NIDA Program Officials.” — NIDA Project Officer, 2014</p>
  --%>

        <p>Synergy’s energy and commitment to our organization has led to breakthroughs that were unthinkable just prior to their engagement. Not only are they expert problem solvers, but they are fully committed to improving educational outcomes for all. —ED/OESE Project Officer, 2015</p>

    </div>

</asp:Content>


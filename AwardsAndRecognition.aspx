﻿<%@ Page Title="Awards and Recognition. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AwardsAndRecognition.aspx.cs" Inherits="AwardsRecognition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double">
        <div class="row awardsrow">
            <div class="col-md-5">
                <h1 class="bigh1">Awards and Recognition</h1>
            </div>
            <div class="col-md-6">

                <p>
                    Synergy has received numerous prestigious awards and has been consistently recognized for outstanding work performance over the last decade. We have gained a stellar reputation for our creativity, innovative approaches, and excellent business practices with partners, clients, and industry alike. 
                </p>
            </div>
        </div>
        <div class="row">
        <div class="leaderline"></div>
            <div class="col-md-6">
                    <div class="trophytext_left">
                    	<div class="trophyimg"><img src="../images/trophies.png" /></div>
                        <h3>Awards</h3>
                        <ul>
                        <li>2015, GD USA, American Graphic Design Awards</li>
                        <li>2014 and 2013, NAGC, Blue Pencil and Gold Screen Awards</li>
                        <li>2014 AVA Digital Awards</li>
                        <li>2012 and 2013 Telly Awards</li>
                        <li>Multiple NIH Plain Language/Clear Communication Awards</li>
                        <li>Golden Quill Awards</li>
                        <li><i>EdPress</i> Distinguished Publication Awards</li>
                        <li>Gold Hermes Creative Awards</li>
                        <li>American Society for Training and Development, President’s Award for Professional Excellence</li>
                        <li>Society of Government Meeting Professionals, Planner of the Month Award</li>
                        </ul>
                </div>
            </div>
            <div class="col-md-6">
            	
                <div class="award_contain">
                    <div class="trophytext">
                    	<!--<div class="awardsLogo"><img src="../images/logos/whiteHouse_seal.png" /></div>-->
                        <h4>The White House </h4>
                        <p>In 2010, President Barack Obama invited the Synergy CEO to a meeting in the Oval Office, followed by a press conference in the Rose Garden, to laud Synergy as a successful information technology small business and a model of American entrepreneurship.</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="award_contain">
                    <div class="trophytext">
                    	<!--<div class="awardsLogo"><img src="../images/govcon_logo.jpg" /></div>-->
                        <h4>The Government Contractors Network</h4>
                        <p>
                           In 2010, Synergy was named a top five GovCon Contractor of the Year. Our CEO and President was named a top five GovCon Executive of the Year. 
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="award_contain">
                    <div class="trophytext">
                    	<!--<div class="awardsLogo"><img src="../images/sba_logo.png" /></div>-->
                        <h4>U.S. Small Business Administration</h4>
                        <p>In 2009, our CEO and President was named the Small Business Person of the Year for Maryland and the Washington, DC Area by the Small Business Administration.</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="award_contain">
                    <div class="trophytext">
                    <!--<div class="awardsLogo"><img src="../images/wt_logo.png" /></div>-->
                        <h4>Washington Technology</h4>
                        <p>In 2008, Washington Technology ranked Synergy 14th on the list of top 25 8(a) contractors of the year.</p>
                    </div>
                    <div class="clear"></div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="awards_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy’s success has many measures, including recognition by federal agencies, national professional associations, and industry award panels. </p>
    </div>



</asp:Content>


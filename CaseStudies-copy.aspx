﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseStudies.aspx.cs" Inherits="CaseStudies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">Case Studies</h1>
            </div>
            <div class="col-md-6">
                <p>Synergy is a small business that brings big skills and deep commitment to critical private and public social initiatives. The list below is a select representation of our service areas and is not exhaustive of our capabilities.</p>
                <div class="sort_contain">
                    <div class="sort">
                        <asp:Label ID="lbllist" Visible="false" runat="server"></asp:Label>
                        <h3>Filter By:</h3>
                    </div>
                    <div class="sort_box col-md-5">
                        <%-- <asp:DropDownList ID="ddlExpertise" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExpertise_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Expertise</asp:ListItem>
                            </asp:DropDownList>--%>
                        <%--<asp:DropDownList ID="ddlServiceArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceArea_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Service Area</asp:ListItem>
                            </asp:DropDownList>--%>

                        <asp:ListBox ID="lbServiceArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lbServiceArea_SelectedIndexChanged" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>

                    </div>
                    <div class="sort_box col-md-5">
                        <asp:DropDownList ID="ddlAgency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" CssClass="form-control">
                            <asp:ListItem>Agency/Office</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="container sort_row">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2 class="cs_sorted">
                    <asp:Label ID="lblxyz" runat="server"></asp:Label>
                    <asp:Label ID="lbltext" runat="server"></asp:Label>
                    <asp:Label ID="lblAgency" runat="server"></asp:Label></h2>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row csrow">
            <asp:HiddenField ID="rval" runat="server" />
            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                <ItemTemplate>
                    <asp:Literal ID="litHeader" runat="server"></asp:Literal>
                    <div class="col-md-4">
                        <div class="casestudy_card">
                        	<div class="cs_logo_contain"><img src="images/logo_standin.png" class="cs_logo" /></div>
                            <h4><%# Eval("CaseStudyTitle") %></h4>
                            <!-- start child repeater -->
                            <ul>
                                <asp:Repeater ID="Repeater2" Visible="false" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <%# Eval("AgencyName")%>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <!-- end child repeater -->
                            <asp:Button ID="btnViewCaseStudy" runat="server" CommandName="buy" CommandArgument='<%# Eval("CaseStudiesID") %>' Text="Go To Case Study" CssClass="btn cs_btn" />
                        </div>
                    </div>
                    <asp:Literal ID="litFooter" runat="server"></asp:Literal>

                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>


</asp:Content>


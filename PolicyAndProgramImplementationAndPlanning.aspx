﻿<%@ Page Title="Policy and Program Implementation and Planning. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PolicyAndProgramImplementationAndPlanning.aspx.cs" Inherits="Exp_PolicyProgImplementation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">Policy &amp; Program Implementation &amp; Planning</h1>
                <div class="cs_button_holder">
                <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click"  CssClass="btn btn-success" />--%>

                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
            </div>
            <div class="col-md-6">

                <p>
                    Important health, education, and other public initiatives require rigorous evaluation, monitoring and implementation designs to ensure that activities align with program statutes and goals. Effective plans must be grounded in evidence-based research and practice—while being flexible enough to respond to shifting priorities and emerging issues. Synergy’s doctorate-level researchers and evaluators identify the most appropriate evaluation designs and apply state-of-the art research and data collection and analysis methodologies to help define or answer your policy and evaluation questions.
                </p>
                <p>We help train the people whose effectiveness is critical to the success of your program. We make valuable data available when and where you need it, in a secure format that maximizes its usefulness. The links below provide more specific descriptions of how Synergy‘s policy and planning skills help clients develop and deliver effective programs</p>

                <h3>Technical assistance</h3>
                <p>We are experts at developing and maintaining comprehensive technical assistance (TA) centers that combine in-person training with Web-housed resources and e-learning tools that build capacity, improve skills and assist grantees and end-users achieve their program goals.  Our blended approach to TA and training integrates deep subject-matter expertise with innovative technology and effective instructional design. Our proven approach to comprehensive TA is built on knowledge collection and management; knowledge creation and product development; knowledge transfer and dissemination; and, project management and evaluation. </p>
                    
                    
                    <h3>Research and Evaluation</h3>
                <p>Synergy’s research and evaluation team includes subject- matter researchers, statisticians, and epidemiologists with a broad range of health and education related expertise.  Our team provides state-of-the-art, rigorous, and cost-effective evaluation research designs and methods and expertise on clearance standards of the Government Performance and Results Act (GPRA) and Office of Management and Budget (OMB). Our staff members have experience designing and implementing experimental and quasi-experimental studies, trend analyses, gap analyses, cost-benefit analyses, environmental scans, and in-depth case studies using a broad range of data collection methods and tools. Our analytic skills range from the quantitative—such as multiple-regression modeling, multivariate techniques, and causal modeling—to qualitative approaches such as content analysis. We bring rigor and scientific objectivity to design and implementation of needs assessments; policy analysis; epidemiological studies; implementation analysis; and process, outcome, and impact evaluations. </p>

            </div>
        </div>
    </div>


    <div class="expertise_policy_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy provides an unmatched combination of nimble, small-business practices and powerful, big-business skills and expertise.</p>
    </div>

</asp:Content>


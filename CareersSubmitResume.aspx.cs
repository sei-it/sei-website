﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Net.Mail;  

public partial class CareersSubmitResume : System.Web.UI.Page
{
     string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
     int lngPkID;
     int lngImage;
     int IngResume;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["ResumeID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["ResumeID"]);
            }
            if (Page.Request.QueryString["IngResume"] != null)
            {
                IngResume = Convert.ToInt32(Page.Request.QueryString["IngResume"]);
            }

            if (Page.Request.QueryString["lngImage"] != null)
            {
                lngImage = Convert.ToInt32(Page.Request.QueryString["lngImage"]);
            }
        }
    }

    //----Display/Update------------//
    protected void displayRecords()
    {
        object objVal = null;

        using (DataClassesDataContext db = new DataClassesDataContext())
        {

        }
        // displayGrid(lngPkID);
    }
    public void updateUser()
    {
       // bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            #region old code
            //SubmitResume oCase = (from c in db.SubmitResumes
            //                     where c.ResumeID == lngPkID
            //                     select c).FirstOrDefault();
            //if ((oCase == null))
           // {
                 //  SubmitResume oCase = new SubmitResume();
                 //  blNew = true;

            // }          always store a new record
            #endregion


            SubmitResume oCase = new SubmitResume();
            oCase.Name = txtName.Text;
            oCase.Email = txtEmail.Text;
            oCase.Phone= txtPhone.Text;
            oCase.Position= txtPosition.Text;
            oCase.SalaryRequirement = txtSalaryRequirement.Text;           

            
            oCase.created_date = DateTime.Now;
            oCase.created_by = "";


            #region old code2
            // if (blNew == true)
           // {
            //    db.SubmitResumes.InsertOnSubmit(oCase);

            //  }
            #endregion

            db.SubmitResumes.InsertOnSubmit(oCase);

            db.SubmitChanges();
            lngPkID = oCase.ResumeID;
            saveCoverLetter();
            saveResume(); 

        }

        lblMessage.Text = "Your information has been submitted for review.";
        

    }

    private void saveCoverLetter()
    {
        if (FU1CoverLetter.HasFile)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                try
                {
                    string newFileName = Guid.NewGuid().ToString() + "-" + FU1CoverLetter.FileName;                   
                   // FU1CoverLetter.SaveAs(Server.MapPath("~/Docs/") + newFileName);
                    string sourceFile = ConfigurationManager.AppSettings["SourceFilePath"].ToString();
                    FU1CoverLetter.SaveAs(sourceFile + newFileName);
                   // string Filepath = "~/Docs/" + newFileName;  
                    string Filepath = sourceFile + newFileName;
                    CoverLetter attachment = (from c in db.CoverLetters select c).FirstOrDefault();
                    attachment = new CoverLetter();
                    attachment.OriginalFileName = FU1CoverLetter.FileName;
                    attachment.PhysicalFileName = newFileName;                   
                    attachment.CreatedDate = DateTime.Now;
                    attachment.ResumeID_FK = lngPkID;

                    db.CoverLetters.InsertOnSubmit(attachment);
                    db.SubmitChanges();
                    lngImage = attachment.CoverLetterID;
                }
                catch (Exception ex)
                {
                    lblcoverMess.Text = " Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
               // lblcoverMess.Text = "Your cover letter has been submitted for review.";
            }
        }
    }

    private void saveResume()
    {
        if (FU2Resume.HasFile)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                try
                {
                    string newFileName = Guid.NewGuid().ToString() + "-" + FU2Resume.FileName;
                    //FU2Resume.SaveAs(Server.MapPath("~/Docs/") + newFileName);
                    //string Filepath = "~/Docs/" + newFileName;

                    string sourceFile = ConfigurationManager.AppSettings["SourceFilePath"].ToString();
                    FU2Resume.SaveAs(sourceFile + newFileName);
                    string Filepath = sourceFile + newFileName;

                    CareersResume attachment = (from c in db.CareersResumes select c).FirstOrDefault();
                    attachment = new CareersResume();
                    attachment.OriginalFileName = FU2Resume.FileName;
                    attachment.PhysicalFileName = newFileName;
                    attachment.CreatedDate = DateTime.Now;
                    attachment.ResumeID_FK = lngPkID;

                    db.CareersResumes.InsertOnSubmit(attachment);
                    db.SubmitChanges();
                    IngResume = attachment.CareersResumeID;
                }
                catch (Exception ex)
                {
                    lblcoverMess.Text = " Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
               // lblcoverMess.Text = "Your resume has been submitted for review.";
            }
        }
    }   

    //-------View State----------------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["ResumeID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["ResumeID"]);
        }
        if (((this.ViewState["IngResume"] != null)))
        {
            IngResume = Convert.ToInt32(this.ViewState["IngResume"]);
        }
        if (((this.ViewState["lngImage"] != null)))
        {
            lngImage = Convert.ToInt32(this.ViewState["lngImage"]);
        }


    }

    protected override object SaveViewState()
    {
        this.ViewState["ResumeID"] = lngPkID;
        this.ViewState["IngResume"] = IngResume;
        this.ViewState["lngImage"] = lngImage;

        return (base.SaveViewState());
    }


    private bool DataIsValid()
    {
        bool returnValue = false;
        bool validName = false;
        bool validEmail = false;
        bool validPhone = false;
        bool validPosition = false;
        bool validSalary = false;
        bool validResume = false;

        RequiredFieldName.Validate();
        validName = (RequiredFieldName.IsValid);

        RequiredFieldEmail.Validate();
        validEmail = (RequiredFieldEmail.IsValid);

        RequiredFieldPhone.Validate();
        validPhone = (RequiredFieldPhone.IsValid);

        RequiredFieldPosition.Validate();
        validPosition = (RequiredFieldPosition.IsValid);

        RequiredFieldSalary.Validate();
        validSalary = (RequiredFieldSalary.IsValid);

        RequiredFieldResume.Validate();
        validResume = (RequiredFieldResume.IsValid);

        returnValue = validName && validEmail && validPhone && validPosition && validSalary && validResume;
        return returnValue;  
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        lblMessage.Text = "";
        if (DataIsValid())
        {
            updateUser();
            SendEmail();
     
            txtName.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtPosition.Text = "";
            txtSalaryRequirement.Text = "";
            FU1CoverLetter = new FileUpload();
            FU2Resume = new FileUpload();
        }
        
    }


    private void SendEmail()
    {

        int resumeID = 0;
        string resumeFilename = "";
        string coverLetterFileName = "";

        System.Net.Mail.MailMessage sendEmail = new System.Net.Mail.MailMessage();

        sendEmail.From = new System.Net.Mail.MailAddress("info@seiservices.com");
        sendEmail.To.Add("FLacerda@seiservices.com");    
        // sendEmail.CC.Add("info@remstacenter.org");
       // sendEmail.Bcc.Add("sspinney@seiservices.com");
       // sendEmail.Bcc.Add("sjoseph@seiservices.com");
        sendEmail.Subject = "Synergy Applicant Information";
    

        string strBody = System.IO.File.OpenText(Server.MapPath("EmailMessage.htm")).ReadToEnd();

        strBody = strBody.Replace("[tName]", txtName.Text);

        strBody = strBody.Replace("[tEmail]", txtEmail.Text);

        strBody = strBody.Replace("[tPhone]", txtPhone.Text);

        strBody = strBody.Replace("[tPosition]", txtPosition.Text);

        strBody = strBody.Replace("[tSalary]", txtSalaryRequirement.Text);       

        sendEmail.Body = strBody;
        sendEmail.IsBodyHtml = true;


        #region old code
        //AlternateView htmlView;
        //htmlView = AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

        //LinkedResource imagelink = new LinkedResource(Server.MapPath("Images/REMS-ESignature-banner_v2.jpg"), "image/jpg");

        //imagelink.ContentId = "imageId";
        //imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
        //htmlView.LinkedResources.Add(imagelink);

        //sendEmail.AlternateViews.Add(htmlView);
        #endregion


        //function to return max resumeID from table submitresume ---  use this as the ResumeID_FK from careersResume and coverletter tables
        resumeID = GetResumeID();
        if (resumeID > 0)
        {
            resumeFilename = GetResumeFileName(resumeID);

            Attachment attach1 = new Attachment(Server.MapPath("Docs/" + resumeFilename));
            sendEmail.Attachments.Add(attach1);

            coverLetterFileName = GetCoverLetterFileName(resumeID);

            if (coverLetterFileName.Trim().Length > 0)
            {
                Attachment attach2 = new Attachment(Server.MapPath("Docs/" + coverLetterFileName));
                sendEmail.Attachments.Add(attach2);
            }
        }


        //Attachment attach1 = new Attachment(Server.MapPath("Docs/EOP_ASSIST_InstallationManual.pdf"));

        //Attachment attach2 = new Attachment(Server.MapPath("Docs/EOPAssist.zip"));

        //sendEmail.Attachments.Add(attach1);
        //sendEmail.Attachments.Add(attach2);

        

        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        try
        {
            smtp.Host = "mail2.seiservices.com";
            smtp.Send(sendEmail);

        }
        catch (Exception ex)
        {
            lblMessage.Text = "error trying to send the email";
            //ViewState["foundError"] = "true";

          
        }
        finally
        {
            System.IO.File.OpenText(Server.MapPath("EmailMessage.htm")).Dispose();
            System.IO.File.OpenText(Server.MapPath("EmailMessage.htm")).Close();
        }

    }


    private int GetResumeID()
    {
        string resumeID = "";
        string sqlString = "SELECT MAX(ResumeID) AS resumeID FROM Submitresume ";


        SqlConnection Con = new SqlConnection(connString);
        SqlDataReader rdr = null;
        try
        {
            Con.Open();
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            // Cmd.Parameters.AddWithValue("trnID", trainingsID);
            Cmd.CommandText = sqlString;
            Cmd.CommandType = CommandType.Text;

            rdr = Cmd.ExecuteReader();

            while (rdr.Read())
                resumeID = rdr[0].ToString();

            return int.Parse(resumeID);
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error trying to retrieve the last record in the Submitresume table";
            return 0;
        }
        finally
        {
            if (!rdr.IsClosed)
                rdr.Close();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }
    }


    private string GetResumeFileName(int resumeID)
    {
        string resumeFileName = "";
        string sqlString = "SELECT PhysicalFileName AS resumeName FROM CareersResume where ResumeID_FK = @p1 ";


        SqlConnection Con = new SqlConnection(connString);
        SqlDataReader rdr = null;
        try
        {
            Con.Open();
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.Parameters.AddWithValue("p1", resumeID);
            Cmd.CommandText = sqlString;
            Cmd.CommandType = CommandType.Text;

            rdr = Cmd.ExecuteReader();

            while (rdr.Read())
                resumeFileName = rdr[0].ToString();

            return resumeFileName;
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error trying to retrieve the PhysicalFileName from the CareersResume table";
            return "";
        }
        finally
        {
            if (!rdr.IsClosed)
                rdr.Close();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }
    }


    private string GetCoverLetterFileName(int resumeID)
    {
        string coverLetterFileName = "";
        string sqlString = "SELECT PhysicalFileName AS coverLetterName FROM CoverLetter where ResumeID_FK = @p1 ";


        SqlConnection Con = new SqlConnection(connString);
        SqlDataReader rdr = null;
        try
        {
            Con.Open();
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.Parameters.AddWithValue("p1", resumeID);
            Cmd.CommandText = sqlString;
            Cmd.CommandType = CommandType.Text;

            rdr = Cmd.ExecuteReader();

            while (rdr.Read())
                coverLetterFileName = rdr[0].ToString();

            return coverLetterFileName;
        }
        catch (Exception ex)
        {
            lblMessage.Text = "Error trying to retrieve the PhysicalFileName from the CoverLetter table";
            return "";
        }
        finally
        {
            if (!rdr.IsClosed)
                rdr.Close();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }
    }       


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;


public partial class SubjectMatterExperts : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    string expertise;
    string sText;
    string agency;
    int iCaseCount = 0;
    int iTotalCaseCount = 0;
    int iTotalCaseCountTmp = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Page.Request.QueryString["expertise"] != null)
            {
                expertise = Convert.ToString(Page.Request.QueryString["expertise"]);
                lblxyz.Text = expertise;
               
            }
            else
            {
                expertise = "";
                lblxyz.Text = expertise;
                
            }

            if (Page.Request.QueryString["agency"] != null)
            {
                agency = Convert.ToString(Page.Request.QueryString["agency"]);
                lblAgency.Text = agency;
               
            }
            else
            {
                agency = "";
                lblAgency.Text = agency;
                
            }


            if (Page.Request.QueryString["expertise"] == null && Page.Request.QueryString["agency"] == null)
            {
                sText = "Subject Matter Experts";
                lbltext.Text = sText;
            }
           

            loadrecords();
            Repeater1.DataSource = SubjectMatterData(expertise, agency);
            Repeater1.DataBind();
        }
        Page.MaintainScrollPositionOnPostBack = true;
    }
    private DataTable SubjectMatterData(string expertise,string agency)
    {
        SqlConnection con = new SqlConnection(connString);
        con.Open();
        string sql = "SELECT distinct SubMatterID, SubMatterExTitle, SubMattDescription,SubMatterExLastName FROM vw_SME_try where ActiveStatus=1 and Expertise like '%' + @Expertise + '%' and AgencyName like '%' + @AgencyName + '%' order by SubMatterExLastName";
        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@Expertise", expertise);
        cmd.Parameters.Add("@AgencyName", agency);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
       
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        iTotalCaseCount = dt.Rows.Count;
        return dt;
    }  
    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from pg in db.Expertises
                      select pg;
            ddlExpertise.DataSource = qry;
            ddlExpertise.DataTextField = "Expertise1";
            ddlExpertise.DataValueField = "ExpertiseID";
            ddlExpertise.DataBind();
            ddlExpertise.Items.Insert(0, "Select Expertise");


            var qry1 = from pg in db.Agencies
                       select pg;
            ddlAgency.DataSource = qry1;
            ddlAgency.DataTextField = "AgencyName";
            ddlAgency.DataValueField = "AgencyID";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, "Select Agency");

           
        }
    }


    //
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["agency"] != null)))
        {
            agency = Convert.ToString(this.ViewState["agency"]);
        }

        if (((this.ViewState["expertise"] != null)))
        {
            expertise = Convert.ToString(this.ViewState["expertise"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["agency"] = agency;
        this.ViewState["expertise"] = expertise;

        return (base.SaveViewState());
    }
    //
    
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


            iCaseCount = iCaseCount + 1;
            iTotalCaseCountTmp = iTotalCaseCountTmp + 1;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                Literal litHeader = (Literal)e.Item.FindControl("litHeader");
                Literal litFooter = (Literal)e.Item.FindControl("litFooter");
                if (iCaseCount == 1)
                {

                    litHeader.Text = @"    	<div class=""row"">";
                    litFooter.Text = @"";
                }
                if (iCaseCount == 2)
                {

                    iCaseCount = 0;
                    litHeader.Text = @"  ";
                    litFooter.Text = @"    	    </div>";
                }

                if (iTotalCaseCount == iTotalCaseCountTmp)
                {
                    litHeader.Text = @"    	<div class=""row"">";
                    litFooter.Text = @"";
                }

            }
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int subMatterID = Convert.ToInt32(drv["SubMatterID"]);
            //Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater2");
            //Repeater2.DataSource = ProductData(subMatterID);
            //Repeater2.DataBind();


        }
    }
    private DataTable ProductData(int subMatterID)
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT Expertise FROM vw_SubjectMatterExp WHERE SubMatterID=" + subMatterID;

        SqlCommand cmd = new SqlCommand(sql, con);

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        con.Close();

        return dt;
    }
    protected void ddlExpertise_SelectedIndexChanged(object sender, EventArgs e)
    {
         DisplayFilterText();
         Repeater1.DataSource = SubjectMatterData(expertise, agency);
         Repeater1.DataBind();

    }

    private void DisplayFilterText()
    {
        if (ddlExpertise.SelectedItem.Text == "Select Expertise" && ddlAgency.SelectedItem.Text == "Select Agency")
        {
            expertise = "";
            lblxyz.Text = expertise;

            sText = "Subject Matter Experts";
            lbltext.Text = sText;

            agency = "";
            lblAgency.Text = agency;
        }
        else if (ddlExpertise.SelectedItem.Text != "Select Expertise" && ddlAgency.SelectedItem.Text != "Select Agency")
        {
            expertise = ddlExpertise.SelectedItem.Text;
            lblxyz.Text = expertise;

            sText = "Subject Matter Experts with the";
            lbltext.Text = sText;

            agency = ddlAgency.SelectedItem.Text;
            lblAgency.Text = agency;
        }
        else if (ddlExpertise.SelectedItem.Text == "Select Expertise" && ddlAgency.SelectedItem.Text != "Select Agency")
        {
            expertise = "";
            lblxyz.Text = expertise;

            sText = "Subject Matter Experts: ";
            lbltext.Text = sText;

           
            agency = ddlAgency.SelectedItem.Text;
            lblAgency.Text = agency;

        }
        else if (ddlExpertise.SelectedItem.Text != "Select Expertise" && ddlAgency.SelectedItem.Text == "Select Agency")
        {
            agency = "";
            lblxyz.Text = agency;

            sText = "Subject Matter Experts in";
            lbltext.Text = sText;            

            expertise = ddlExpertise.SelectedItem.Text;
            lblAgency.Text = expertise;

        }

    }  

    
    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {       

        //if (ddlExpertise.SelectedItem.Text == "Select Expertise" && ddlAgency.SelectedItem.Text == "Select Agency")
        //{
        //    expertise = "";
        //    lblxyz.Text = expertise;

        //    sText = "All Subject Matter Experts";
        //    lbltext.Text = sText;

        //    agency = "";
        //    lblAgency.Text = agency;
        //}        
        //else if (ddlExpertise.SelectedItem.Text != "Select Expertise" && ddlAgency.SelectedItem.Text != "Select Agency")
        //{
        //    expertise = ddlExpertise.SelectedItem.Text;
        //    lblxyz.Text = expertise;

        //    sText = "Subject Matter Experts with the";
        //    lbltext.Text = sText;

        //    agency = ddlAgency.SelectedItem.Text;
        //    lblAgency.Text = agency;
        //}
        //else if (ddlExpertise.SelectedItem.Text == "Select Expertise" && ddlAgency.SelectedItem.Text != "Select Agency")
        //{
        //    agency = ddlAgency.SelectedItem.Text;
        //    lblxyz.Text = agency;

        //    sText = "Subject Matter Experts";
        //    lbltext.Text = sText;

        //    expertise = "";
        //    lblAgency.Text = expertise;

        //}
        //else if (ddlExpertise.SelectedItem.Text != "Select Expertise" && ddlAgency.SelectedItem.Text == "Select Agency")
        //{
        //    expertise = ddlExpertise.SelectedItem.Text;
        //    lblxyz.Text = expertise;

        //    sText = "Subject Matter Experts";
        //    lbltext.Text = sText;

        //    agency = "";
        //    lblAgency.Text = agency;

        //}

        DisplayFilterText();
        Repeater1.DataSource = SubjectMatterData(expertise, agency);
        Repeater1.DataBind();
    }

    
}
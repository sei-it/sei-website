﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CareersCurrentOpeningDetails.aspx.cs" Inherits="CareersCurrentOpeningDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

     <div class="container landing_padding">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="basic_content">

                <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                    <ItemTemplate>
                        <h2><%# Eval("JobTitle") %></h2>
                        <%# Eval("JobDescription") %>
                    </ItemTemplate>
                </asp:Repeater>
                     
                <div>
                    <asp:Button ID="btnSubmitResume" runat="server" Text="Submit Your Resume" OnClick="btnSubmitResume_Click" CssClass="btn btn-primary" />

                </div>
                <div>

                    <a href="CareersWithUs.aspx">Back</a>

                </div>
            </div>
        </div>
    </div>
         </div>

</asp:Content>


﻿<%@ Page Title="Contract Vehicles and Certifications. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ContractVehiclesAndCertifications.aspx.cs" Inherits="ContractVehicles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">Contract Vehicles and Certifications</h1>
            </div>
            <div class="col-md-6">
                <p>Synergy is certified by the Small Business Administration as a Woman-Owned Disadvantaged Small Business (WODSB). Synergy’s services can be procured through the Government-Wide Acquisition Vehicles listed below. </p>
              
                  <p>To learn how Synergy can help your agency, institution, or company meet its goals, please contact Annie McCormick via e-mail at <a href="mailto:amccormick@seiservices.com">amccormick@seiservices.com</a> or call 240.485.1700, ext. 1707.</p>
            </div>
        </div>
        <div class="row cvc_intro">
            <div class="col-sm-12">
                <h3>Synergy is able to provide services under the following certifications and contract vehicles:</h3>
            </div>
        </div>
        <div class="row cvc_row">
            <div class="col-md-6">
                <div class="cvc_set cvc_small">
                <div class="cvcLogo"><img alt="SBA Logo" src="../images/sba_logo.png" /></div>
                    <h4>Small Business Administration</h4>
                    <ul>
                        <li>Woman-Owned Disadvantaged Small Business</li>
                    </ul>
                </div>
                </div>
                
                <div class="col-md-6">
                <div class="cvc_set cvc_small">
                	<div class="cvcLogo"><img alt="SAMHSA Logo" src="../images/samhsa-logo.gif" /></div>
                    <h4>Department of Health and Human Services, Substance Abuse and Mental Health Services Administration (SAMHSA)</h4>
                    <ul>
                        <li>Indefinite Delivery/Indefinite Quantity (IDIQ) Contract: 09/30/2010-09/28/2015, Domain IV for provision of technical assistance and training.</li>
                    </ul>
                </div>
                </div>
                </div>
                
                <div class="row cvc_row">
                <div class="col-md-6">
                <div class="cvc_set cvc_big">
                	<div class="cvcLogoPad"><img alt="GSA Logo" src="../images/GSAStarMarkweblogopolicy3333.jpg" /></div>
                    <h4>General Services Administration (GSA)</h4>
                    <div class="cvc_box">
                        <ul>
                            <li>07F-0436W</li>
                            <li>541-3</li>
                            <li>541-4D</li>
                            <li>541-4F</li>
                        </ul>
                    </div>
                    <div class="cvc_box">
                        <ul>
                            <li>541-2000</li>
                            <li>874-7</li>
                            <li>874-7RC</li>
                            <li>874-1</li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="cvc_set cvc_big">
                <div class="cvcLogo"><img alt="DHHS logo" src="../images/dhhs.png" /></div>
                    <h4>U.S. Department of Health and Human Services, Program Support Center</h4>
                    <ul>
                        <li>IDIQ Contract: 05/04/2015-03/15/2020. Small Business Prime for a range of services, such as policy assessment and analysis, evaluation design and data collection, performance measurements, technical assistance and training, web design, publication support, and 508 compliance.  </li>
                    </ul>
                </div>
                </div>
                </div>
                
                <div class="row cvc_row">
                <div class="col-md-6">
                <div class="cvc_set cvc_big">
                	<div class="cvcLogo"><img alt="NAICS Logo" src="../images/naics.png" /></div>
                    <h4>North American Industry Classification System Codes (NAICS)</h4>
                    <div class="cvc_box">
                        <ul>
                            <li>518210</li>
                            <li>519190</li>
                            <li>541511</li>
                            <li>541512</li>
                       
                            <li>541513</li>
                            <li>541519</li>
                            <li>541611</li>
                            <li>541614</li>
                        </ul>
                    </div>
                    <div class="cvc_box">
                        <ul>
                            <li>541618</li>
                            <li>541890</li>
                            <li>541990</li>
                            <li>561410</li>
                     
                            <li>561499</li>
                            <li>561920</li>
                            <li>611430</li>
                            <li>611710</li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cvc_set cvc_big">
                	<div class="cvcLogoPad"><img alt="DoE Seal" src="../images/US-DeptOfEducation-Seal.png" /></div>
                    <h4>Department of Education, Education Statistics Support Institute Network (ESSIN)</h4>
                    <ul>
                        <li> IDIQ Contract: 03/23/2012-03/22/2017. Small Business Prime selected to provide research, data collection, statistical analysis, annual report preparation, 508 compliance, mobile application development, strategic communications planning, and publications dissemination activities.  </li>
                    </ul>
                </div>
                </div>
                </div>
                
                <div class="row cvc_row">
                <div class="col-md-6">
                <div class="cvc_set cvc_small">
                <div class="cvcLogo"><img alt="CMS Logo" src="../images/cms_logo.jpg" /></div>
                    <h4>Center for Medicare &amp; Medicaid Services (CMS)</h4>
                    <ul>
                        <li>CMS BPA (Conference Planning Support): 09/30/2014-09/29/2019. Services range from providing pre-, onsite, and post-meeting activities for small, medium, and large workshops and conferences for training and technical assistance.  </li>                        
                    </ul>
                </div>
                </div>
                </div>
                
                </div>


    <div class="contract_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>We can quickly and cost-effectively meet the needs of federal clients through a streamlined procurement process that accesses all of Synergy’s services and expertise. </p>
    </div>

</asp:Content>


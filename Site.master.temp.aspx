﻿<%@ Master Language="C#" AutoEventWireup="true" CodeFile="Site.master.cs" Inherits="SiteMaster" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
    <title>Synergy Enterprises Incorporated</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/normalize.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/slider.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/hover.css" type="text/css" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css'>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	

    <asp:ContentPlaceHolder runat="server" ID="HeadContent" />
	
	
			<style>
		.stick-wrapper #main-content {
				position: relative;
				 overflow: visible;
			  box-sizing: border-box;
			  min-height: 1px;
}

			.stick-wrapper #side-sticky {

				position: relative;
				  overflow: visible;
				  box-sizing: border-box;
				  min-height: 1px;
}
		
			.stick-wrapper .theiaStickySidebar{
			padding-top: 50px !important; 
			padding-bottom: 0 !important; 
			position: static; 
			top: 60px; 
			}
			
			.stick-wrapper .landing_padding_double {
 			 padding-top: 10%;
}
			
		</style> 
	
</head>
<body>

    <form runat="server">
        <div id="skrollr-body">
		
	
<div id="bars">
                        <div id="bar1"></div>
                        <div id="bar2"></div>
                        <div id="bar3"></div>
                        <div id="bar4"></div>
                        <div id="bar5"></div>
                        <div id="bar6"></div>
                    </div>
					
					
                <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                    
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" rel="home" href="/" title="Synergy Enterprises Inc.">
                            <img src="images/sei_logo.png" /></a>
                    </div>
                    
                    <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="OurProfile.aspx" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Profile<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="TheCompany.aspx">The Company</a></li>
                <li><a href="ContractVehiclesAndCertifications.aspx">Contract Vehicles &amp; Certifications</a></li>
                <li><a href="Leadership.aspx">Leadership</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="OurExpertise.aspx" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Expertise<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="ProgramEvaluationAndImplementation.aspx">Program Evaluation &amp; Implementation</a></li>
                <li><a href="MultimediaCommunication.aspx">Multimedia Communication</a></li>
                <li><a href="InformationTechnologyIntegration.aspx">Information Technology Integration</a></li>
                <li><a href="WebDesignApplicationDevelopmentandE-Learning.aspx">Web Design, Application Development, &amp; E-Learning</a></li>
                <li><a href="ManagementConsultingAndLogistics.aspx">Management Consulting &amp; Logistics</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="OurWork.aspx" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Work<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="CaseStudies.aspx">Case Studies</a></li>
                <li><a href="SubjectMatterExperts.aspx">Subject Matter Experts</a></li>
                <li><a href="AwardsAndRecognition.aspx">Awards &amp; Recognition</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Contact Us<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
			  	<li><a href="ContactUs.aspx">Contact Us</a></li>
                <li><a href="PartnerWithUs.aspx">Partner With Us</a></li>
                <li><a href="CareersWithUs.aspx">Careers With Us</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->

              
                </div>
				




            <div id="body">
                <asp:ContentPlaceHolder runat="server" ID="FeaturedContent" />

                <section class="content-wrapper main-content clear-fix">

                    <asp:ContentPlaceHolder runat="server" ID="MainContent" />
                </section>
            </div>



<div id="news_highlights">
	<div class="container">
    <div class="row">
    	<div class="col-sm-7 col-md-7">
        	<h5>Highlights</h5>
                        <div class="nh_contain">
                        	<div class="nh_date">
                            	<p class="nh_day">14</p>
                                <p class="nh_month">Aug</p>
                            </div>
                           
                            <div class="nh_text">
                                <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                                    <ItemTemplate>

                                        <%--<asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="HighlightsDetails" Visible="true" CommandArgument='<%# Eval("HighlightsID") %>'>
                                                                <h6><%# Eval("HighlightsTitle") %></h6></asp:LinkButton>

                                        <p>
                                            <%# Eval("HighlightsShortDescription")%>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="HighlightsDetails" Visible="true" CommandArgument='<%# Eval("HighlightsID") %>'>
                                                                read more..</asp:LinkButton>
                                        </p>--%>

                                         <h6><%# Eval("HighlightsTitle") %></h6>

                                        <!--<p>
                                           <%-- <%# Eval("HighlightsShortDescription")%>--%>
                                             <%# FormatText(DataBinder.Eval(Container.DataItem, "HighlightsShortDescription") as string) %> </p>-->
                                            
                                       

                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="clear"></div>
                        </div>
        </div>
        <div class="col-sm-5 col-md-5">
        <h5 class="twitterh5">Follow Us</h5>
                        
                        <div id="twittercontainer">
                            <a class="twitter-timeline" href="https://twitter.com/SEIServices" data-widget-id="601016878186659841">Tweets by @SEIServices</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
        </div>
    </div>
    </div>
</div>

            

            <div id="footer">
                <p>©2015. Synergy Enterprises, Inc. All rights reserved. A woman-owned small business.</p>
            </div>
        </div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

		<script type="text/javascript" src="../js/theia-sticky-sidebar.js"></script>

        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/skrollr.min.js"></script>
		<script type="text/javascript">
    $(document).ready(function(){
      if ($(window).width() > 1050) {
          var s = skrollr.init()
      }
    });
  </script>

        <!--[if lt IE 9]>
	<script type="text/javascript" src="skrollr.ie.min.js"></script>
	<![endif]-->
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/responsiveslides.min.js"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 4
                $("#slider4").responsiveSlides({
                    auto: false,
                    pager: false,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                        $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                    }
                });
            });
        </script>
		

	<script>
$(window).load(function()
{
// Define the style variables for the TWITTER WIDGET! //////////////////////////////////////
var $background_color = "#E7E7E7";
var $font = "'Helvetica Neue',Helvetica,Arial,sans-serif";
var $font_weight = "normal";
var $border_color = "#711471";
var $border_radius = "0";
var $text_color = "#555555";
var $link_color = "#B161A6";
var $name_color = "#2d936d";
var $subtext_color = "#939393"; // Colour of any small text
var $sublink_color = "#2d936d"; // Colour of smaller links, eg: @user, date, expand/collapse links
var $avatar_border = "0px solid #36b787";
var $avatar_border_radius = "50%";
var $icon_color = "#D0D0D0"; // Color of the reply/retweet/favourite icons
var $icon_hover_color = "#36b787"; // Hover color the reply/retweet/favourite icons
var $header_background = "#9d489d";
var $header_text_color = "#ffffff";
var $follow_button_link_color = "#5ea9dd";
var $footer_background = "#9d489d";
var $footer_tweetbox_background = "#5A2164";
var $footer_tweetbox_textcolor = "#ffffff";
var $footer_tweetbox_border ="0px";
var $load_more_background ="#2d936d";
var $load_more_text_color = "#232323";

// Apply the styles
$("iframe").contents().find('head').append('<style>.html, body, h1, h2, h3, blockquote, p, ol, ul, li, img, iframe, button, .tweet-box-button{font-family:'+$font+' !important;font-weight:'+$font_weight+' !important;} .timeline{border-radius: ' + $border_radius + '!important;} .thm-dark .retweet-credit,.h-feed, .stats strong{color:' + $text_color + ' !important;}a:not(.follow-button):not(.tweet-box-button):not(.expand):not(.u-url), .load-more{color:' + $link_color + ' ;} .follow-button{color:' + $follow_button_link_color + ' !important;} .timeline-header{background:' + $header_background + '; border-radius:' + $border_radius + ' ' + $border_radius + ' 0px 0px;} .timeline-header h1 a{color:' + $header_text_color + ' !important;} .timeline-footer{border-radius:0px 0px ' + $border_radius + ' ' + $border_radius + ' !important; background:' + $footer_background + ' !important;} .tweet-box-button{background-color:' + $footer_tweetbox_background + ' !important; color:' + $footer_tweetbox_textcolor + ' !important; border:' + $footer_tweetbox_border + ' !important;} .timeline .stream, .tweet-actions{background:' + $background_color + ' !important;} .tweet-actions{box-shadow:0 0 10px 5px' + $background_color + ' !important;} .ic-mask{background-color:' + $icon_color + ' !important;} a:hover .ic-mask, a:focus .ic-mask{background-color:' + $icon_hover_color + ' !important;} .header .avatar{border-radius: '+ $avatar_border_radius + ' !important; border:' + $avatar_border + ' !important;} .p-name{color:'+$name_color+' !important;} .customisable-border{border-color:' + $border_color + ' !important;} span.p-nickname, .u-url, .expand{color:'+$sublink_color+' !important;} .load-more, .no-more-pane {background-color:' + $load_more_background + ' !important; color:' + $load_more_text_color + '!important;} .retweet-credit{color:' + $subtext_color + ' !important;}</style>');
});
</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

	

    </form>

<script>
			$(document).ready(function() {
				$('#side-sticky, #main-content, .rightSidebar')
					.theiaStickySidebar({
						additionalMarginTop: 60
					});
			});
		</script>
</body>
</html>

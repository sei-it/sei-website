﻿<%@ Page Title="Who We Are. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="WhoWeAre.aspx.cs" Inherits="WhoWeAre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="interior_landing" class="whoweare_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="interior_landing_text">
                        <h1>who<br />
                            <span class="land_purpleh1">we are</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="basic_content">
                    <%--<p>Synergy is a team of dedicated individuals who bring subject matter expertise, customized technology tools, and award-winning communications services to projects that help connect and strengthen people, programs, communities, and countries.</p>
           --%>
                    <%--<p>We are deeply committed to supporting organizations in our communities, our nation, and across the world. We are a team of dedicated individuals who bring subject matter expertise, customized technology tools, and award-winning communications services to projects that help people, communities, and countries.</p>
                    <p>We help clients advance initiatives locally, nationally, and internationally through the following services.</p>
                    <p>Synergy builds partnerships that last.</p>--%>
                    <p>Our global experience, cultural knowledge, and multilingual skills add depth to our support and guarantee success to our clients. We are deeply committed to supporting organizations in our neighborhood, our nation, and across the world.  Our dedicated  staff brings subject matter expertise, customized technology tools, and award-winning communications to projects that help people, communities, and countries.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container landing_icons">
        <div class="row">
            <div class="col-sm-2 col-sm-offset-2">
                <div class="landing_card">
                    <a href="TheCompany.aspx">
                        <img src="images/company_icon.png" class="landicon" /></a>
                    <h4><a href="TheCompany.aspx">The Company</a></h4>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="landing_card">
                    <a href="ContractVehiclesAndCertifications.aspx">
                        <img src="images/contract_icon.png" class="landicon" /></a>
                    <h4><a href="ContractVehiclesAndCertifications.aspx">Contract Vehicles &amp; Certifications</a></h4>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="landing_card">
                    <a href="Leadership.aspx">
                        <img src="images/leadership_icon.png" class="landicon" /></a>
                    <h4><a href="Leadership.aspx">Leadership</a></h4>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="landing_card">
                    <a href="ClientsAndPartners.aspx">
                        <img src="images/partners_icon.png" class="landicon" /></a>
                    <h4><a href="ClientsAndPartners.aspx">Clients and Partners</a></h4>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


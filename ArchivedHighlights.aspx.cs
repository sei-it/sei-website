﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ArchivedHighlights : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        loadrecords();

    }


    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.Highlights
                           where p.ActiveStatus == true
                           orderby p.DisplayOrderID_FK 
                           select p;
                           
            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "HighlightsDetails")
        {

            int sHighlightsID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("HighlightsDetails.aspx?HighlightsID=" + sHighlightsID);


        }
    }
}
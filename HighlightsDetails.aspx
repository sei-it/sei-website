﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="HighlightsDetails.aspx.cs" Inherits="HighlightsDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


     <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-4">
                <h1 class="bigh1">Synergy Highlights</h1>
            </div>
            <div class="col-md-8">
               
                <%-- <h1>SEI Highlights</h1>--%>
                   <asp:Repeater ID="Repeater1" runat="server">
                      <ItemTemplate>
                            
                          <h3><%# Eval("HighlightsTitle") %></h3>
                         <%# Eval("HighlightsShortDescription")%>   
                        <%# Eval("HighlightsLongDescription")%>  
                      </ItemTemplate>
                 </asp:Repeater>
                
           </div>
        </div>
    </div>

</asp:Content>


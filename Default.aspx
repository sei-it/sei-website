﻿<%@ Page Title="Public Health and Public Education. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
   
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div id="video_area">
    </div>

    
    <div id="tagline">
    	<div class="tagline_contain">
        	<h1>We bring deep commitment to<br /><span class="bigtag">Public Health &amp; Public Education</span></h1>
        </div>
        <div class="dotted_connector"></div>
    </div>

    <div id="who_we_are">
        <div class="container">
            <h2>Our Profile</h2>
            <div class="row">
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="TheCompany.aspx" class="learn_btn1"><img src="images/diverse_staff.png" /></a>-->
                    <h3><a href="TheCompany.aspx">The Company</a></h3>
                      <p>A decade of collaborative growth. A team of committed professionals. We are your partner for innovation and excellence.</p>
                    	<div class="arrow_btn"><a href="TheCompany.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow" /></a></div>
                    </div>
                </div>
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="ClientsAndPartners.aspx" class="learn_btn1"><img src="images/partners_icon.png" class="main_icon" /></a>-->
                    <h3><a href="ClientsAndPartners.aspx">Clients &amp; Partners</a></h3>
                    <p>We ensure that each client receives the highest level of support their work deserves. We build partnerships that last.</p>
                    <div class="arrow_btn"><a href="ClientsAndPartners.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow"  /></a></div>
                    </div>
                </div>
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="Leadership.aspx" class="learn_btn1"><img src="images/IT_team.png" class="main_icon" /></a>-->
                    <h3><a href="Leadership.aspx">Leadership</a></h3>
                    <p>Synergy’s team is guided by a simple vision: to demand the best of ourselves and provide the best to our clients. </p>
                    <div class="arrow_btn"><a href="Leadership.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow" /></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


	
    <div id="our_expertise" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <div id="exholder">
            <div id="exh2holder"><h2>Our<br />Expertise</h2></div>
            <div id="exulholder">
                <ul class="expertise">
                    <li><img src="images/1.png" class="numimg" alt="1"  /><a href="ProgramEvaluationAndImplementation.aspx">Research and Evaluation and Technical Assistance</a></li>
                    <li><img src="images/2.png" class="numimg" alt="2"/><a href="MultimediaCommunication.aspx">Multimedia Communication</a></li>
                    <li><img src="images/3.png" class="numimg" alt="3" /><a href="InformationTechnologyIntegration.aspx">Information Technology Integration</a></li>
					<li><img src="images/4.png" class="numimg" alt="4" /><a href="WebDesignApplicationDevelopmentandE-Learning.aspx">Web Design, Application Development, and E-Learning</a></li>
					<li><img src="images/5.png" class="numimg" alt="5" /><a href="ManagementConsultingAndLogistics.aspx">Management Consulting and Logistics</a></li>
                    </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
 



    <div id="our_work">
        <div class="container">
            <h2>Our Work</h2>
            <div class="row">
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="CaseStudies.aspx"><img src="images/case_studies.png" class="main_icon" /></a>-->
                    <h3><a href="CaseStudies.aspx">Case Studies</a></h3>
                    <p>See how we have helped clients advance their initiatives locally, nationally, and globally.</p>
                    <div class="arrow_btn"><a href="CaseStudies.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow" /></a></div>
                    </div>
                </div>
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="SubjectMatterExperts.aspx"><img src="images/phd_experts.png" class="main_icon" /></a>-->
                    <h3><a href="SubjectMatterExperts.aspx">Subject Matter Experts</a></h3>
                    <p>Synergy has human assets, technical skills, and subject matter expertise that few small businesses can match.</p>
                    <div class="arrow_btn"><a href="SubjectMatterExperts.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow" /></a></div>
                    </div>
                </div>
                <div class="col-sm-4 main_icon">
                	<div class="mi_contain">
                    <!--<a href="AwardsAndRecognition.aspx"><img src="images/awards.png" class="main_icon" /></a>-->
                    <h3><a href="AwardsAndRecognition.aspx">Awards and Recognition</a></h3>
                    <p>Our success has many measures. See what others say about our cutting-edge creativity and performance.</p>
                    <div class="arrow_btn"><a href="AwardsAndRecognition.aspx"><img src="images/link_arrow_purple.svg" class="arrimg" alt="arrow" /></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="flag_area" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy is a small business with big skills bringing positive outcomes to communities across the country and globally.</p>

    </div>

    <!--<div id="lets_talk">
	<div class="container">
		<div class="row">
            <div class="col-md-4 col-md-offset-2">
                <h4><a href="PartnerWithUs.aspx">Partner<br />
                    <span class="wus">With Us</span></a></h4>
            </div>
            <div class="col-md-4">
                <h4><a href="CareersWithUs.aspx">Careers<br />
                    <span class="wus">With Us</span></a></h4>
            </div>
        </div>
    </div>
</div>-->


    <video autoplay poster="images/landingBG_tablet.jpg" id="bgvid" loop>
        <source src="videos/SEIvid_white.webm" type="video/webm">
        <source src="videos/SEIvid_white.mp4" type="video/mp4">
    </video>

</asp:Content>

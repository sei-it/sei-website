﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseStudiesDetail.aspx.cs" Inherits="C_CaseStudiesDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container clearfix nomax case_detail_landing">
        <div class="grid_12">
            <div id="caseheader">
                <h1>Case Studies:</h1>
                <asp:Repeater ID="CaseStudyTitle2" runat="server">
                    <ItemTemplate>
                        <h2><%# Eval("CaseStudyTitle")%></li></h2>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <div class="container clearfix nomax case_slider">
        <div class="callbacks_container">
            <ul class="rslides" id="slider4">
                <li>
                    <div class="slide_desktop">
                        <asp:Repeater ID="Repeater3" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField Value='<%# Eval("CaseStudiesID") %>' ID="HiddenField1" runat="server" />
                                <img src='<%#"ImageServer.aspx?CaseStudiesID="+ DataBinder.Eval(Container.DataItem, "CaseStudiesID")%>' />
                                <%--<img src='<%#"ImageServer.aspx?CaseStudiesImagesID="+ DataBinder.Eval(Container.DataItem, "CaseStudiesImagesID")%>' /> --%>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="slide_mobile">
                        <asp:Repeater ID="Repeater4" runat="server">
                            <ItemTemplate>

                                <asp:HiddenField Value='<%# Eval("CaseStudiesID") %>' ID="HiddenField2" runat="server" />
                                <img src='<%#"MobileImageServer.aspx?CaseStudiesID="+ DataBinder.Eval(Container.DataItem, "CaseStudiesID")%>' />

                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                      
                        <asp:Repeater ID="Solution" runat="server">
                           <ItemTemplate>   
                                 <p class="caption">                             
                                <span class="cd_h1">Solution</span><br />
                               <%# FormatText(DataBinder.Eval(Container.DataItem, "Solution") as string) %>  
                                  </p>
                            </ItemTemplate>
                        </asp:Repeater>
                   

                </li>

                <li id="hide" runat="server">
                    <div class="slide_desktop">
                        <asp:Repeater ID="Repeater5" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField Value='<%# Eval("CaseStudiesID") %>' ID="HiddenField3" runat="server" />
                                <img src='<%#"ImageServer.aspx?CaseStudiesImagesID="+ DataBinder.Eval(Container.DataItem, "CaseStudiesImagesID")%>' />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="slide_mobile">
                        <asp:Repeater ID="Repeater6" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField Value='<%# Eval("CaseStudiesID") %>' ID="HiddenField4" runat="server" />
                                <img src='<%#"MobileImageServer.aspx?CaseStudiesMobileImagesID="+ DataBinder.Eval(Container.DataItem, "CaseStudiesMobileImagesID")%>' />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>

                     
                        
                        <asp:Repeater ID="Solution2" runat="server">
                             <ItemTemplate>   
                                 <p class="caption">                             
                                <span class="cd_h1">Solution</span><br />
                               <%# FormatText(DataBinder.Eval(Container.DataItem, "Solution") as string) %>  
                                  </p>
                            </ItemTemplate>
                        </asp:Repeater>
                 

                   <%-- <p class="caption">
                        <span class="cd_h1">App Build</span><br />
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>--%>
                </li>
                <%--   <li>
                    <div class="slide_desktop">
                        <img src="images/cd_slide3.jpg" alt="">
                    </div>
                    <div class="slide_mobile">
                        <img src="images/cd_slide_mobile.jpg" alt="">
                    </div>
                    <p class="caption">
                        <span class="cd_h1">Online Video</span><br />
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                </li>--%>
            </ul>
        </div>
    </div>

    <div class="container clearfix case_detail">
        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
            <ItemTemplate>
                <div class="grid_4">
                    <div class="ataglance">
                        <h3>Project At-A-Glance</h3>
                        <ul class="pag_ul">
                            <li><span class="pag_title">Agency: </span>
                                <br />
                                <asp:Repeater ID="Repeater2" runat="server">
                                    <ItemTemplate>
                                        <%# Eval("AgencyName")%></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <li><span class="pag_title">Office: </span>
                                    <br />
                                    <%# Eval("Office")%></li>
                            <li><span class="pag_title">Need: </span>
                                <br />
                                <%# FormatText(DataBinder.Eval(Container.DataItem, "Need") as string) %>
                                <%--<li><span class="pag_title">Solution: </span>
                                 <br />
                                <%# FormatText(DataBinder.Eval(Container.DataItem, "Solution") as string) %>                             
                            </li>--%>
                        </ul>
                    </div>
                </div>
                <div class="grid_8 omega">
                    <div class="cd_intro">
                        <h2>Synergy's Approach</h2>
                        <%# Eval("CaseDescription")%>
                    </div>
                </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

    </div>
    <div class="case_back">
        <asp:Button ID="backtoCaseStudies" runat="server" Text="Back to Case Studies" OnClick="backtoCaseStudies_Click" CssClass="bcs_btn" />
    </div>



    <script type="text/javascript" src="js/skrollr.min.js"></script>
    <script type="text/javascript">
        var s = skrollr.init();
    </script>
    <!--[if lt IE 9]>
	<script type="text/javascript" src="skrollr.ie.min.js"></script>
	<![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu').slicknav();
        });
    </script>

    <script src="js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: false,
                pager: false,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });
        });
    </script>
</asp:Content>


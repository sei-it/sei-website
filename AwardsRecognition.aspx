﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AwardsRecognition.aspx.cs" Inherits="AwardsRecognition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
<div class="container landing_padding_double">
	<div class="row awardsrow">
    	<div class="col-md-5">
				<h1 class="bigh1">Corporate Awards and Recognition</h1>     
        </div>
        </div>
        <div class="row">
    	<div class="col-md-6">
            	<h3>Blue Pencil and Gold Screen Awards</h3>
                    <p>Synergy won three 2014 Blue Pencil and Gold Screen awards and six 2013 Blue Pencil and Gold Screen awards from the National Association of Government Communicators</p>
            	<h3>AVA Digital Awards</h3>
                    <p>Synergy earned two 2014 AVA Digital Gold Awards for the development of educational videos.</p>
            	<h3>Telly Awards</h3>
                    <p>Synergy won a 2012 Telly Award for five videos produced in collaboration with BlueSky Films and Abt Associates for the Substance Abuse and Mental Health Services Administration's 8th Annual Prevention Day.</p>
            	<h3>GovCon Contractor of the Year</h3>
                    <p>Synergy was nominated and named a top-five finalist for the 2010 GovCon Contractor of the Year Award.</p>
            	<h3>GovCon Executive of the Year</h3>
                    <p>Synergy’s president was nominated and named a top-five finalist for the 2010 GovCon Executive of the Year Award.</p>
                    </div>
                    <div class="col-md-6">
            	<h3>Small Business Administration</h3>
                    <p>Synergy’s president was honored as the 2009 Washington Metropolitan Area District Small Business Person of the Year by the Small Business Administration.</p>
            	<h3>Montgomery County Council</h3>
                    <p>Synergy’s president was nominated and named a top-five finalist for the 2010 GovCon Executive of the Year Award.</p>
            	<h3>Washington Technology</h3>
                    <p>Synergy ranked 14 on <i>>Washington Technology's</i> 2008 list of Top 25 Nationwide 8(a) Firms.</p>
            	<h3>Outstanding Contribution Award</h3>
                    <p>Synergy’s president received an Outstanding Contribution Award by the National Association of Professional Asian American Women in 1994, 1996, 1997, 2000, and 2002.</p>
            </div>
        </div>
    </div>

<div class="awards_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<p>Synergy’s success has many measures, including recognition by Federal agencies, national professional associations, and industry award panels. </p>
</div>



</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProgramEvaluationAndImplementation.aspx.cs" Inherits="Exp_PolicyProgImplementation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

<style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>


       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
</script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double stick-wrapper">
        <div class="row">
            
            
            <div class="col-md-6" id="side-sticky">
                <div class="theiaStickySidebar">  <h1 class="bigh1">Program Evaluation and Implementation</h1>
                <div class="cs_button_holder">
                    <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click"  CssClass="btn btn-success" />--%>

                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
              </div>  
            </div>
            <div class="col-md-6" id="main-content">
                <p>
                    Important health, education, and other public initiatives require rigorous evaluation designs to ensure that implementation activities align with program statutes and goals. Effective evaluation plans must be grounded in evidence-based research and practice—while being flexible enough to respond to shifting priorities and emerging challenges. Synergy’s doctorate-level researchers and evaluators develop the most appropriate evaluation designs using state-of-the art data sampling, collection, and analysis methods to answer critical evaluation questions and guide program implementation decisions. We can apply these methods  at the community, state, regional, and federal levels.
                </p>

                
		  <h3  class="expand"  onclick=" ExpandCollapse('list1');">Research and Evaluation</h3>
            <div id="list1" style="display:none"> 
                <p>Synergy’s evaluation and research professionals provide state-of-the-art, rigorous, and cost-effective research designs and methods. We emphasize collaboration to create evidence-based policy, implement programs, and sustain capacity building. Synergy’s research and evaluation team includes subject-matter researchers, statisticians, and epidemiologists with a broad range of health and education expertise. </p>
                <p>Our research and evaluation designs and methods consider the Government Performance and Results Act (GPRA) and Office of Management and Budget (OMB) requirements and standards. Synergy’s evaluators and researchers design needs assessments; experimental and quasi-experimental studies; trend, gap, and cost-benefit analyses; environmental scans; and case studies using a range of methods.</p>
                <p>Our data collection methods are varied:</p>
                <ul>
                    <li>Ethnographic observation</li>
                    <li>Site visits</li>
                    <li>Focus groups</li>
                    <li>In-depth interviews</li>
                    <li>Online surveys</li>
                    <li>In-person surveys</li>
                    <li>Systematic literature reviews  </li>

                </ul>
                <p>Our analytic skills range from the quantitative—such as multiple-regression modeling, multivariate techniques, and causal modeling—to qualitative approaches such as content analysis. What’s more, we bring rigor and objectivity to all our work, by following the position statements on ethical and cultural competency set forth by the American Educational Research Association, the American Evaluation Association, the Agency for Healthcare Research and Quality, and the Health Resources and Services Administration. </p>
            </div> 
				
		    <h3  class="expand"  onclick=" ExpandCollapse('list2');">Technical Assistance for Program Implementation</h3>
              <div id="list2" style="display:none"> 
				
                <p>Synergy develops technical assistance (TA) strategies that include powerful and diverse resources and dissemination vehicles. The foundation of our TA is collaboration with clients to identify activities and tools that will strengthen the quality of programs or services. </p>
                <p>Our proven approach to comprehensive TA is built on four key components, including: </p>
                <ul>
                    <li>Knowledge collection and management </li>
                    <li>Knowledge creation and product development </li>
                    <li>Knowledge transfer and dissemination </li>
                    <li>Project management and evaluation</li>
                </ul>
                <p>We understand the need for scaled and flexible TA that provides: </p>
                <ul>
                    <li>Global services to broadly disseminate information and resources </li>
                    <li>Targeted services that focus on specific groups and are delivered through webinars, conferences, or other approaches</li>
                    <li>Intensive services targeted to specific groups and issues </li>
                </ul>
                <p>Through comprehensive centers, we combine in-person training with Web-housed information and skill building to disseminate current, evidence-based knowledge to policy makers, administrators, practitioners, and families. Our TA centers provide custom-built strategies that consider adult-learning practices, offering services based on the needs of the target audience. We flexibly respond to diverse needs and requests, provide continuous refinements, and offer progressive and sustained TA. Our blended approach to TA and training integrates deep subject-matter expertise with innovative technology and effective instructional design. </p>

            </div>
        </div>
    </div>


    <div class="expertise_policy_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Providing comprehensive research services and technical assistance to guide decisions and build capacity.</p>
    </div>

</asp:Content>


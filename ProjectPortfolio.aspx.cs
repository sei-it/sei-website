﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;


public partial class ProjectPortfolio : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadrecords();
        }
        Repeater1.DataSource = ProjectPortfolioData();
        Repeater1.DataBind();
    }

    private DataTable ProjectPortfolioData()
    {
        SqlConnection con = new SqlConnection(connString);
        con.Open();
        string sql = "SELECT PortfolioID, ProjectPortTitle FROM C_Portfolio where ActiveStatus=1";
        SqlCommand cmd = new SqlCommand(sql, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;
    }


    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from pg in db.Expertises
                      select pg;
            ddlExpertise.DataSource = qry;
            ddlExpertise.DataTextField = "Expertise1";
            ddlExpertise.DataValueField = "ExpertiseID";
            ddlExpertise.DataBind();
            ddlExpertise.Items.Insert(0, "Select Expertise");


            var qry1 = from pg in db.Agencies
                       select pg;
            ddlAgency.DataSource = qry1;
            ddlAgency.DataTextField = "AgencyName";
            ddlAgency.DataValueField = "AgencyID";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, "Select Agency");
        }
    }




    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            DataRowView drv = (DataRowView)e.Item.DataItem;

            int portfolioID = Convert.ToInt32(drv["PortfolioID"]);

            Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater2");

            Repeater2.DataSource = ProjectfortAgencyData(portfolioID);

            Repeater2.DataBind();

        }
    }

    private DataTable ProjectfortAgencyData(int portfolioID)
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT AgencyName FROM vw_ProjectPortfolioAgency WHERE PortfolioID=" + portfolioID;

        SqlCommand cmd = new SqlCommand(sql, con);

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        con.Close();

        return dt;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class CareersWithUs : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? CarrersId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CarrersId"] != null)
            {
                CarrersId = Convert.ToInt32(Page.Request.QueryString["CarrersId"]);
            }
            loadrecords();

        }
    }

    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.Careers
                           where p.ActiveStatus == true
                           select p;
            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();

        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["CarrersId"] != null)))
        {
            CarrersId = Convert.ToInt32(this.ViewState["CarrersId"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["CarrersId"] = CarrersId;
        return (base.SaveViewState());
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "CareersDetails")
        {

            int CareersID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CareersCurrentOpeningDetails.aspx?CareersID=" + CareersID);


        }
    }
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

}
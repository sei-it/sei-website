﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CareersSubmitResume.aspx.cs" Inherits="CareersSubmitResume" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-5">
                <h1 class="bigh1">Submit Resume</h1>
            </div>
               <div class="col-md-6 submitresume">
                <p>
                    <div class="label-align"><asp:Label ID="lblName" runat="server" Text="Name"></asp:Label><span style="color: red;display:inline-block">*</span> </div>
                    <asp:TextBox ID="txtName" runat="server" CausesValidation="True"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldName" runat="server" 
                  Text=" Name is Required"   ControlToValidate="txtName" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator> </p>

                <p>
                    <div class="label-align"><asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label><span style="color: red;display:inline-block">*</span> </div>
                    <asp:TextBox ID="txtEmail" runat="server" CausesValidation="True"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldEmail" runat="server" 
                  Text=" Email is Required"   ControlToValidate="txtEmail" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>  </p>

                 <p>
                    <div class="label-align"><asp:Label ID="lblPhone" runat="server" Text="Phone"></asp:Label><span style="color: red;display:inline-block">*</span></div>
                    <asp:TextBox ID="txtPhone" runat="server" CausesValidation="True"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldPhone" runat="server" 
                  Text=" Phone number is Required"   ControlToValidate="txtPhone" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>  </p>

                 <p>
                    <div class="label-align"><asp:Label ID="lblPosition" runat="server" Text="Position"></asp:Label><span style="color: red;display:inline-block">*</span></div>
                    <asp:TextBox ID="txtPosition" runat="server" CausesValidation="True"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldPosition" runat="server" 
                  Text=" Position is Required"   ControlToValidate="txtPosition" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator> </p>

                 <p>
                    <div class="label-align"><asp:Label ID="lblSalaryRequirement" runat="server" Text="Salary Requirement"></asp:Label><span style="color: red;display:inline-block">*</span></div>
                    <asp:TextBox ID="txtSalaryRequirement" runat="server" CausesValidation="True"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldSalary" runat="server" 
                  Text=" Salary Requirement is Required"   ControlToValidate="txtSalaryRequirement" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator> </p>
         
                <p class="formnopad">
                     <asp:Label ID="lblCover" runat="server" Text="Upload Cover Letter (only .doc, .docx, or.pdf)"></asp:Label>
                    <asp:FileUpload ID="FU1CoverLetter" runat="server" /></p>
                <asp:RegularExpressionValidator ID="revCover" runat="server"
                    ControlToValidate="FU1CoverLetter" ErrorMessage="only .doc, .docx, or.pdf files are allowed"
                    ValidationExpression="(.*?)\.(doc|pdf|DOC|PDF|docx|DOCX)$" ForeColor="Red" ValidationGroup="Save">
                </asp:RegularExpressionValidator>

                 <p class="formnopad">
                      <asp:Label ID="lblResume" runat="server" Text="Upload Resume (only .doc, .docx, or.pdf)"></asp:Label><span style="color: red;display:inline-block">*</span>
                    <asp:FileUpload ID="FU2Resume" runat="server" />
             
             <asp:RegularExpressionValidator ID="revResume" runat="server"
                    ControlToValidate="FU2Resume" ErrorMessage="only .doc, .docx, or.pdf files are allowed"
                    ValidationExpression="(.*?)\.(doc|pdf|DOC|PDF|docx|DOCX)$" ForeColor="Red" ValidationGroup="Save">
             </asp:RegularExpressionValidator><br />
             <asp:RequiredFieldValidator ID="RequiredFieldResume" runat="server" 
                  Text=" Resume is Required"   ControlToValidate="FU2Resume" Font-Bold="False" 
                     SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>   </p>

                <p><asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Save" OnClick="btnSubmit_Click" CssClass="btn btn-primary" /></p>
                 <p><asp:Label ID="lblcoverMess" runat="server"></asp:Label></p>
                 <p><asp:Label ID="lblResMess" runat="server"></asp:Label></p>
                <p><asp:Label ID="lblMessage" runat="server"></asp:Label></p>
            </div>
        </div>
    </div>

</asp:Content>


﻿<%@ Page Title="Information Technology Integration. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="InformationTechnologyIntegration.aspx.cs" Inherits="ExpertiseItIntegration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

<style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>

       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
  </script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double  stick-wrapper">
        <div class="row">
            <div  id="side-sticky" class="col-md-5">
            
            <div class="theiaStickySidebar"> 
                <h1 class="bigh1">Information Technology Integration</h1>
                <div class="cs_button_holder">
                <%-- <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success" />--%>

                     <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
               </div> 
            </div>
            <div id="main-content" class="col-md-7">

               <p> Synergy creates information technology (IT) solutions and strategies that help our clients disseminate critical materials and facilitate research on public education and health. Our IT staff members are certified in systems security, information security management, project management, and risk/information systems control. Synergy’s integrated suite of IT solutions links critical aspects of our clients’ IT, business, and management processes to ensure extraordinary outcomes. </p>
                
                
 <!-- Start Collapse-->


          
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Enterprise Architecture</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p> Synergy offers performance-enhancing infrastructure design and security solutions that expertly align technology systems with business objectives. From data flow analysis, sequencing plan development, and code generation to system integration, hardware recommendations, current state assessment, and enterprise transition planning, we constantly strengthen our architecture of information to match emerging technologies. </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Business Process Enhancement</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p> Synergy’s business process enhancement solution takes full advantage of our in-depth industry expertise to deliver process-based solutions. Our proprietary framework, methodologies, and technology partnerships help our clients develop their internal business and IT processes so they can create lasting organizational change and efficiencies. </p>    
                
                <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>
                        
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Security and IT Governance</a>
                                </h3>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p> Synergy’s security and IT governance practices focus on helping clients identify technology requirements that support network security. We provide infrastructures that are secure, scalable, and reliable, based on Federal Information Security Management Act (FISMA)-compliant best practices. We configure networks and servers to meet the meticulous security standards required for certification and Authorization-to-Operate (ATO) clearances. This allows clients to securely host .Gov websites. Synergy provides monitoring capabilities (e.g., dashboards and macro-level analyses) that provide real-time information on business, risk, and compliance metrics. We also support our clients by developing web-based portals for documented business processes, policies, control objectives, and risks. </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Project Management Office (PMO) Integration</a>
                                </h3>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <p> Synergy’s PMO integration process leverages our internal PMO development methodology, which allows us to quickly assess the need for a PMO or the health of existing structures. We bring a holistic view to PMO development, which ensures that all aspects of the organization are considered during PMO assessment and implementation. Our PMO solution helps senior leadership assess resource demands across projects and gauge the robustness of management processes. From data flow analysis, sequencing plan development, and code generation to system integration, model simulation, and methodology training, Synergy is constantly strengthening and redesigning the architecture of information to match emerging technologies and client needs.  </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
          
        </div>
<!--End Collapse -->
                
                
                
                


            </div>
        </div>
    </div>
    <div class="expertise_integration_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
       <p>Synergy continually strengthens and redesigns the architecture of information to match emerging technologies and client needs.</p>
    </div>
</asp:Content>


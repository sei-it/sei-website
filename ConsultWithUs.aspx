﻿<%@ Page Title="Consult With Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ConsultWithUs.aspx.cs" Inherits="ConsultWithUs1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
<div class="container landing_padding_double">
	<div class="row">
    	<div class="col-md-4">
        	<h1 class="bigh1">Consult With Us</h1>
        </div>
        <div class="col-md-8">
            <p>To learn more about how Synergy can help your agency, institution, or company meet your goals, contact </p>
        </div>
    </div>
</div>

<div class="consult_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<p>We form productive teaming relationships to ensure that each client—Federal agency, nonprofit, or commercial—is supported by the expertise and innovation their important work deserves. </p>
</div>

</asp:Content>


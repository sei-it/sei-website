﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class HighlightsDetails : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int sHighlightsID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["HighlightsID"] != null)
            {
                sHighlightsID = Convert.ToInt32(Page.Request.QueryString["HighlightsID"]);
            }
            loadrecords(sHighlightsID);
        }
    }

    private void loadrecords(int sHighlightsID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.Highlights
                           where p.HighlightsID == sHighlightsID
                           select p;

            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();
        }
    }


    


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["HighlightsID"] != null)))
        {
            sHighlightsID = Convert.ToInt32(this.ViewState["HighlightsID"]);
        }
       
    }
    protected override object SaveViewState()
    {
        this.ViewState["HighlightsID"] = sHighlightsID;
       
        return (base.SaveViewState());
    }
}
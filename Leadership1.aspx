﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Leadership.aspx.cs" Inherits="Leadership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container clearfix landing_intro">
        <div class="grid_12">
            <div class="basic_content">
                <h1>LEADERSHIP</h1>
                <p>
                    Suspendisse eleifend nulla vitae ante pellentesque, quis ornare sapien vestibulum. Maecenas eleifend
                    augue ipsum, et pellentesque elit consectetur at. Suspendisse efficitur porta pellentesque. Proin quis
                    gravida elit, at pulvinar turpis. Praesent sed posuere nulla, et porta odio.
                </p>
            </div>
        </div>
    </div>

     <div class="container clearfix cardgrid">
        <div class="grid_3">
            <div class="cd_card">
                <div class="icon_2">
                        <a href="#">
                            <img src="images/icon.gif" /></a>
                        <h3>Prachee Devadas</h3>
                        <p>President/CEO</p>

                        <%--<div> <p>
                                Planning for growth, delivering for clients, and hands-on managing: These have propelled Prachee Devadas, 
                                founder and CEO of Synergy Enterprises, Inc. to business success and the rapid rise of her Silver Spring,
                                 Md. consulting firm. From a two-person shop at a kitchen table in 2003, Synergy has grown into a floor of 
                                offices with some 110 full-time employees and 50 federal government contracts, largely in education and 
                                public health. The Small Business Administration in 2009 named Devadas the Small Business Person of the 
                                Year for Maryland. The next year, President Barack Obama cited Devadas for her business acumen at a 
                                Rose Garden event. In 2013 the Small and Emerging Contractors Advisory Forum nominated Synergy for Small Business 
                                of the Year Contractor (under $14 million category). Amid all her commitments, Devadas gives back through 
                                So Others Might Eat, among several causes she supports here and abroad.
                            </p></div>--%>
                    </div>
                    <div class="clear"></div>
            </div>
        </div>      
    </div>

     <div class="container clearfix cardgrid">
        <div class="grid_3">
            <div class="cd_card">
                <a href="#">
                            <img src="images/icon.gif" /></a>
                        <h3>Christine Hartley</h3>
                        <p>Vice President/Division Director</p>
                        <%-- <div>
                            <p>
                                For seasoned leaders, one executive hat often isn’t enough. At Synergy, Christine Hartley 
                              wears two: vice president of operations since 2005, and director of our Division of Conference 
                              and Peer-Review Services, for which she oversees some 30 staff members. Drawing on a quarter 
                              century of experience in federal contracting, Hartley has developed quality assurance policies 
                              and standard operating procedures for our grant-review and conference work. The upshot: 
                              streamlined activities and lower costs, so Synergy can deliver top-drawer services within 
                              contract guidelines to all clients. Hartley oversees contract deliverables for a host of 
                              projects—from grant reviews for several agencies within the U.S. Department of Education; 
                              to scientific meeting and conference services for the National Institutes of Health. Before 
                              joining Synergy, Hartley was vice president for the Information and Dissemination Division 
                              of DB Consulting Group, in Silver Spring, Md., where she managed conferences and logistical 
                              services, scientific writing and publication production, and contract budget tracking.
                            </p>
                        </div>--%>
            </div>
        </div>
        <div class="grid_3">
            <div class="cd_card">
                <a href="#">
                            <img src="images/icon.gif" /></a>
                        <h3>Roy Walker</h3>
                        <p>Vice President/Division Director</p>
                        <%--<div>
                            <p>
                                Versatile is the word for Roy Walker, who brings a wide-ranging background of managerial 
                              work on projects involving everyone from American Indians to methamphetamine users and 
                              pregnant women. As director of our Assessment and Evaluation Services Division since 2008, 
                              Walker has overseen such varied contracts as the 21st Century Learning Centers for the U.S. 
                              Department of Education, technical assistance for the Office of AIDS Research at the National 
                              Institute on Drug Abuse (NIDA), and the Prevention Fellowship Program for the Center for 
                              Substance Abuse Prevention of the Substance Abuse and Mental Health Services Administration. 
                              Earlier, he was corporate monitor for Data Collection and Analysis for Alternatives to Restraint 
                              and Seclusion Grant Program at Center for Mental Health Services, where he oversaw planning, 
                              implementation, and quality assurance. Walker was also a project director at NIDA, where 
                              he managed training and technical assistance to substance abuse and HIV projects.
                            </p>
                        </div>--%>
            </div>
        </div>       
    </div>
     <div class="container clearfix cardgrid">
        <div class="grid_3">
            <div class="cd_card">
                <a href="#">
                            <img src="images/icon.gif" /></a>
                        <h3>Patrick Zickler</h3>
                        <p>Division Director</p>
                        <%--<div>
                            <p>
                                When words meet visuals online or on the page, success takes someone who knows 
                              the path from inspiration to publication. Patrick Zickler, director of communications 
                              services at Synergy, manages a team of more than 15 writers, editors, graphic designers, 
                              and other specialists. He also directs our multiyear project to migrate scientific 
                              content for the National Institute of Diabetes and Digestive and Kidney Diseases 
                              into a new SharePoint Web environment. Over three decades, Zickler has developed 
                              publications, written, and edited for such agencies as the Substance Abuse and 
                              Mental Health Services Administration, the National Institute on Drug Abuse, and 
                              the National Cancer Institute’s Tobacco Control Research Branch. His expertise 
                              ranges from drug addiction to health care disparities. Zickler won the 2010 
                              National Institutes of Health (NIH) Plain Language Award and the 2008 Gold Hermes 
                              Creative Award from the Association of Marketing and Communications Professionals.
                            </p>

                        </div>--%>
                 <div class="clear"></div>
            </div>
        </div>
        <div class="grid_3">
            <div class="cd_card">
                <a href="#">
                            <img src="images/icon.gif" /></a>
                        <h3>Robbie Levenberry</h3>
                        <p>Division Director</p>
                        <%--<div>
                            <p>
                                Synergy needs a dynamic manager to keep ahead of rapid changes in information technology (IT). 
                              As our director of IT strategy, Robbie Levenberry manages the IT division and contributes to 
                              strategy and growth for the division and for the company overall. Leading a technical team of 
                              15–20 staff members, he ensures that operations plans, policies, procedures, and transition 
                              and migration plans match company goals. Levenberry also serves as in-house advisor on IT 
                              requirements for contracts, helping set timelines and recommending technology. Levenberry 
                              oversees a gamut of policies, guidelines, standards, and procedures for IT operations from 
                              system security to risk mitigation and quality assurance. Before joining Synergy, he worked 
                              with several consulting firms, where he reviewed, managed, and deployed resources to ensure 
                              the quality of IT delivery. Throughout his career, Levenberry has worked with one overarching 
                              aim: to synchronize business and IT so as to produce the best value for clients.
                            </p>
                        </div>--%>
                 <div class="clear"></div>
            </div>
        </div>       
    </div>

    



</asp:Content>


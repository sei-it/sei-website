﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MultimediaCommunications.aspx.cs" Inherits="MultimediaCommunications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">MULTIMEDIA COMMUNICATION</h1>
                <div class="cs_button_holder">
                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
            </div>
            <div class="col-md-6">
                <p>Synergy produces award-winning communications products that make our clients' visions come alive on paper and on screen. Our communications products (2-D, 3-D animations, videos, and Public Service Announcements or PSAs) conform to the client’s branding and technical specifications. Our print packages meet full Government Printing Office specifications. Every Web page and document is tested to ensure compliance with Section 508 of the Rehabilitation Act of 1973.</p>

                <h3>Promotional Campaigns </h3>
                <p>
                    Synergy provides full service integrated marketing and communications services to help our clients design, enhance, and promote national outreach campaigns. Our services include developing strategic communication plans aimed at disseminating knowledge to change attitudes, and behaviors.
                </p>
               <p> Using a mixed methodology to gather information about the target audience such as interviews and environmental scans, we customize a campaign’s branding and craft effective communication strategies for maximal appeal to the key audience. Synergy’s communications strategies are aimed at mobilizing partners and stakeholder to expand the reach and enhance the impact of campaign messaging.  Our communications staff also designs, tests, and evaluates products; and conducts market analysis and testing. </p>

                <h3>Writing and Editing </h3>

                <p>Whether it is a congressionally mandated report, white paper, or educational video, our writers, editors, and graphic designers are equipped with the skills and experience to create visually appealing multimedia products. Synergy’s researchers, writers, and editors bring specific subject-matter expertise to client projects and are published in as many as 200 journals, books, and magazines. They understand the nuances that matter when writing about complex technical and scientific subjects, and can draft and polish messages that are clear, concise, and comprehensive for varied audiences. Synergy reviews publications for grammar, style, consistency, and substance using agency-specific guides and industry-standard style manuals. We are experienced in performing meticulous and accurate quality control reviews of data-rich documents.</p>

                <h3>Graphic Design</h3>

                <p>Our staff of graphic designers ensures that communications products intelligently convey messages in a visually engaging, culturally sensitive format . Graphic designers use a variety of state-of-the-art illustration tools and also provide hand-drawn illustrations to add clarity and style to a range of graphic products such as exhibits, books, conference materials, posters, and other promotional products. </p>

                <h3>508 Compliance </h3>

                <p>Our staff includes full-time Section 508 staff that ensure universal access to information housed on government websites. Synergy’s Web developers and designers, as well as graphic designers and publications specialists continually evaluate  the latest technical developments and other trends in accessibility.  Their efforts ensure that we provide the best and most cost-effective methods for our clients to meet their goals with fully compliant static and dynamic pages.</p>

                <p>We can provide formal training or simple tips and guidance to help clients prepare documents that simplify 508 compliance testing and assurance. Synergy 508 specialists conduct quality assurance reviews using the most up-to-date versions of “gold standard” tools including Job Access With Speech (JAWS) and Adobe Professional 508 compliance diagnostic software. We also evaluate a full range of hardware compatibility considerations such as usability testing with keyboard and mouse.</p>
            </div>
        </div>
    </div>


   <div class="expertise_stats_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Bringing creative and technical expertise to public engagement and education.</p>
    </div>
</asp:Content>


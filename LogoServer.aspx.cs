﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class LogoServer : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString1"].ToString();
    int? CaseStudiesID;
    int? CaseStudyTitleImageID;
    string imageName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CaseStudiesID"] != null)
            {
                CaseStudiesID = Convert.ToInt32(Page.Request.QueryString["CaseStudiesID"]);
            }
            if (Page.Request.QueryString["CaseStudyTitleImageID"] != null)
            {
                CaseStudyTitleImageID = Convert.ToInt32(Page.Request.QueryString["CaseStudyTitleImageID"]);
            }
        }
        GetImageName(CaseStudiesID);
        GetImageName2(CaseStudyTitleImageID);
    }

    private void GetImageName2(int? CaseStudyTitleImageID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {


            var vCourses = from p in db.vw_CaseStudyAndLogos
                           where p.CaseStudyTitleImageID == CaseStudyTitleImageID
                           select p;

            foreach (var oCase in vCourses)
            {
                imageName = oCase.PhysicalFileName;
                // int ids = oCase.CaseStudiesImagesID;
                Response.ContentType = "image/jpeg"; // for JPEG file
                string physicalFileName = ConfigurationManager.AppSettings["SourceFilelogoPath"].ToString();

                physicalFileName = physicalFileName + imageName;
                Response.WriteFile(physicalFileName);
            }
        }

    }

    private void GetImageName(int? CaseStudiesID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = (from p in db.vw_CaseStudyAndLogos
                            where p.CaseStudiesID == CaseStudiesID
                            select p).Take(1);

            foreach (var oCase in vCourses)
            {
                imageName = oCase.PhysicalFileName;
                Response.ContentType = "image/jpeg"; // for JPEG file
                string physicalFileName = ConfigurationManager.AppSettings["SourceFilelogoPath"].ToString();

                physicalFileName = physicalFileName + imageName;
                Response.WriteFile(physicalFileName);
            }
        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["CaseStudiesID"] != null)))
        {
            CaseStudiesID = Convert.ToInt32(this.ViewState["CaseStudiesID"]);
        }
        if (((this.ViewState["CaseStudyTitleImageID"] != null)))
        {
            CaseStudyTitleImageID = Convert.ToInt32(this.ViewState["CaseStudyTitleImageID"]);
        }


    }
    protected override object SaveViewState()
    {
        this.ViewState["CaseStudiesID"] = CaseStudiesID;
        this.ViewState["CaseStudyTitleImageID"] = CaseStudyTitleImageID;

        return (base.SaveViewState());
    }

}
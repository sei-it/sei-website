﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SubjectMatterExperts.aspx.cs" Inherits="SubjectMatterExperts" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div class="container landing_padding_double">
        <div class="row">
            <div class="new-width">
                    <h1 class="bigh1">Subject Matter Experts</h1>
                    </div>
                    <div class="new-width">
                    	<p> Synergy staff members provide in-depth knowledge of a wide range of public health and public education content areas. In the field of public health, we offer specialized expertise in such areas as tobacco cessation;
                            underage drinking; fetal alcohol syndrome; substance abuse prevention, use, misuse, addiction, and recovery; mental health research and treatment; HIV/AIDS research; homelessness programs; suicide prevention; eliminating
                            health disparities; and improving child health. Our expertise in education includes, but is not limited to, magnet and charter school programs, the needs of English learners (ELs), afterschool programs, foreign languages, early childhood, 
                            STEM programs (science, technology, engineering, and mathematics), special education, adult sexual misconduct (ASM), readiness and emergency management for schools (REMS), and teacher quality programs. </p>

                        <p>Select the filter buttons below to learn more about the expertise and contributions of our key staff members in specific service areas:</p>

                  <%--  <ul>
                    <li>Advanced degrees: 19 doctoral degrees and 34 master's degrees;</li>
                    <li>Certifications in conference planning, project management, and government meeting planning; and</li>
                    <li>IT credentials that include certifications in Information Systems Security, Information Security Management, Risk and Information Systems Control, and Cisco and Microsoft Network Certification.</li>
                    </ul>--%>
                    
                    
                    
                   <div class="sort_contain">
                        <div class="sort">
                            <h3>Filter By:</h3>
                        </div>
                        <div class="sort_box col-md-5">
                            <asp:DropDownList ID="ddlExpertise" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExpertise_SelectedIndexChanged" CssClass="form-control" Width="300">
                                <asp:ListItem>Expertise</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                       <%-- <div class="sort_box col-md-5">
                            <asp:DropDownList ID="ddlAgency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Agency/Office</asp:ListItem>
                            </asp:DropDownList>
                        </div>--%>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        
        
     <div class="container">
        
        <div class="row smrow">

            
   

    <div class="container sort_row">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                    <h2 class="cs_sorted">
                        <asp:Label ID="lblxyz" runat="server"></asp:Label>
                        <asp:Label ID="lbltext" runat="server"></asp:Label> </h2>
                       <%-- <asp:Label ID="lblAgency" runat="server"></asp:Label>--%>
            </div>
        </div>
    </div>



    <%--<div class="container">
	<div class="row smrow">
	<asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
	<ItemTemplate>
    	<div class="col-md-4">
        	<div class="subject_card">
            	<h4><%# Eval("SubMatterExTitle") %></h4>
                	<ul>
                        <!-- start child repeater -->
                        <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>                              
                        	<li>
                            	<%# Eval("Expertise")%>
                            </li>                               
                        </ItemTemplate>
                        </asp:Repeater>
                        <!-- end child repeater -->
                   	</ul>
            </div>
        </div>
	</ItemTemplate>
	</asp:Repeater>
    </div>
</div>--%>

    <div class="container">
        <%--    	<div class="row">--%>
        <div class="row smrow">

            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                <ItemTemplate>
                    <asp:Literal ID="litHeader" runat="server"></asp:Literal>
                    <div class="new-width">
                        <div class="subject_card">
                            <h4><%# Eval("SubMatterExTitle") %></h4>
                            <!-- start child repeater -->
                           <%-- <ul>
                                <asp:Repeater ID="Repeater2" runat="server">
                                    <ItemTemplate>

                                        <li>
                                            <%# Eval("Expertise")%>
                                        </li>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>--%>
                             <p><%# Eval("SubMattDescription") %></p>
                            <!-- end child repeater -->
                        </div>
                    </div>
                    <asp:Literal ID="litFooter" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <%--      </div>--%>
    </div>

</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;


public partial class C_CaseStudiesDetails : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? CaseStudiesID;
    //string url;
    //string val1;
    //int CStydyId;
    //string imageName;
    //string MobileimageName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CaseStudiesID"] != null)
            {
                CaseStudiesID = Convert.ToInt32(Page.Request.QueryString["CaseStudiesID"]);
            }

            if (Page.Request.QueryString["Other"] != null)   //for office = 'Other', do not show Office
            {
                Repeater1.Visible = false;
                Repeater7.Visible = true;
            }
            else
            {
                Repeater1.Visible = true;
                Repeater7.Visible = false;
            }


            Repeater1.DataSource = CaseStudyDetail();
            Repeater1.DataBind();

            Repeater7.DataSource = CaseStudyDetail();
            Repeater7.DataBind();

            CaseStudyTitle2.DataSource = CaseStudyDetail();
            CaseStudyTitle2.DataBind();

            Repeater3.DataSource = CaseStudyImages();
            Repeater3.DataBind();

            Repeater4.DataSource = CaseStudyMobileImages();
            Repeater4.DataBind();

           // CaseStudyImages2();
           // CaseStudyMobileImages2();


            Solution.DataSource = CaseStudySolution();
            Solution.DataBind();

         //   CaseStudySolution2();

        }
    }

  //  protected void CaseStudySolution2()
  //  {
        //SqlConnection con = new SqlConnection(connString);
        //con.Open();

        ////old code string sql = "SELECT Top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK order by CaseStudiesImagesID desc";
        //string sql = "SELECT  top 2 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK except SELECT  top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK";

        //SqlCommand cmd = new SqlCommand(sql, con);
        //cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);
        //SqlDataAdapter da = new SqlDataAdapter(cmd);

        //DataTable dt = new DataTable();

        //da.Fill(dt);
        //con.Close();

        //if (dt != null)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        hide.Visible = true;
        //        Solution2.DataSource = dt;
        //        Solution2.DataBind();
        //    }
        //    else
        //    {
        //        hide.Visible = false;
        //    }
        //}
   // }

    private object CaseStudySolution()
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        // string sql = "SELECT Top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=" + CaseStudiesID;
        string sql = "SELECT  top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK";
        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dtsolution = new DataTable();
        da.Fill(dtsolution);
        con.Close();
        return dtsolution;
    }

    public static string FormatText(string testString)
    {
        testString = testString.Replace("<p>", "");
        return testString.Replace("</p>", "");
    }
    private object CaseStudyProjectData(int caseStudiesID)
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT AgencyName FROM vw_CaseStudyProjectAgency WHERE CaseStudiesID=@caseStudiesID";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@CaseStudiesID", caseStudiesID);
        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        con.Close();

        return dt;
    }

     private DataTable CaseStudyImages()
    {

        SqlConnection con = new SqlConnection(connString);

        con.Open();

       // string sql = "SELECT Top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=" + CaseStudiesID;
        string sql = "SELECT  top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);
        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);
        con.Close();
        return dt;
    }
    private DataTable CaseStudyMobileImages()
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT top 1 * FROM vw_CaseStudyAndMobileImage WHERE CaseStudiesID_FK=@CaseStudiesID_FK";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);
        con.Close();
        return dt;
    }

  //  private void CaseStudyMobileImages2()
  //  {
        //SqlConnection con = new SqlConnection(connString);

        //con.Open();

        //string sql = "SELECT top 2 * FROM vw_CaseStudyAndMobileImage WHERE CaseStudiesID_FK=@CaseStudiesID_FK except SELECT top 1 * FROM vw_CaseStudyAndMobileImage WHERE CaseStudiesID_FK=@CaseStudiesID_FK";

        //SqlCommand cmd = new SqlCommand(sql, con);
        //cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);

        //SqlDataAdapter da = new SqlDataAdapter(cmd);

        //DataTable dt = new DataTable();

        //da.Fill(dt);
        //con.Close();
        //if (dt != null)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        hide.Visible = true;
        //        Repeater6.DataSource = dt;
        //        Repeater6.DataBind();
        //    }
        //    else
        //    {
        //        hide.Visible = false;
        //    }
        //}

   // }
   
    //protected void CaseStudyImages2()
   // {
        //SqlConnection con = new SqlConnection(connString);

        //con.Open();

        ////old code string sql = "SELECT Top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK order by CaseStudiesImagesID desc";
        //string sql = "SELECT  top 2 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK except SELECT  top 1 * FROM vw_CaseStudyAndImages WHERE CaseStudiesID_FK=@CaseStudiesID_FK";

        //SqlCommand cmd = new SqlCommand(sql, con);
        //cmd.Parameters.Add("@CaseStudiesID_FK", CaseStudiesID);
        //SqlDataAdapter da = new SqlDataAdapter(cmd);

        //DataTable dt = new DataTable();

        //da.Fill(dt);
        //con.Close();

        //if (dt != null)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        hide.Visible = true;
        //        Repeater5.DataSource = dt;
        //        Repeater5.DataBind();
        //    }
        //    else
        //    {
        //        hide.Visible = false;
        //    }
        //}

   // }

    private object CaseStudyDetail()
    {
        SqlConnection con = new SqlConnection(connString);
        con.Open();
        string sql = "SELECT CaseStudiesID, CaseStudyTitle,Office,Solution,Need,CaseDescription FROM vw_CaseStudySearch where ActiveStatus=1 and CaseStudiesID=@CaseStudiesID";
        SqlCommand cmd = new SqlCommand(sql, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        cmd.Parameters.Add("@CaseStudiesID", CaseStudiesID);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;
    }


    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            DataRowView drv = (DataRowView)e.Item.DataItem;

            int caseStudiesID = Convert.ToInt32(drv["CaseStudiesID"]);

            Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater2");

            Repeater2.DataSource = CaseStudyProjectData(caseStudiesID);

            Repeater2.DataBind();
        }

    }


    protected void Repeater7_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            DataRowView drv = (DataRowView)e.Item.DataItem;

            int caseStudiesID = Convert.ToInt32(drv["CaseStudiesID"]);

            Repeater Repeater7A = (Repeater)e.Item.FindControl("Repeater7A");

            Repeater7A.DataSource = CaseStudyProjectData(caseStudiesID);

            Repeater7A.DataBind();
        }

    }



    //-------View State----------------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["CaseStudiesID"] != null)))
        {
            CaseStudiesID = Convert.ToInt32(this.ViewState["CaseStudiesID"]);
        }


    }
    protected override object SaveViewState()
    {
        this.ViewState["CaseStudiesID"] = CaseStudiesID;

        return (base.SaveViewState());
    }
   
    protected void backtoCaseStudies_Click(object sender, EventArgs e)
    {
        Response.Redirect("CaseStudies.aspx");
    }
}
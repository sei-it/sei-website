﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div id="video_area">
        <div class="container clearfix nomax">
            <div id="landing_text">
                <h1>we embrace our client<br />
                    commitments to social initiatives in<br />
                    <span class="phe">public health</span><br />
                    <span class="and">&amp;</span> <span class="phe">education</span></h1>
            </div>
        </div>
    </div>
    <%-- <div id="who_we_are">
        <h2 class="greenHeading">Who We Are</h2>
        <div class="container clearfix">
            <div class="threeicon_contain clearfix">
                <div class="icon_contain icfirst">
                    <img src="images/icon.gif" />
                    <h3>Diverse Staff</h3>
                    <p><a href="DiverseStaff.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain">
                    <img src="images/icon.gif" />
                    <h3>PH.D. - Level Experts</h3>
                    <p><a href="PHDLevelExperts.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain iclast">
                    <img src="images/icon.gif" />
                    <h3>Innovative IT Team</h3>
                    <p><a href="InnovativeItTeam.aspx" class="learn_btn">Learn More</a></p>
                </div>
            </div>
            <p>Synergy is a woman-owned small business that brings big skills and deep commitment to public health, public education, and other critical national and global initiatives.</p>
        </div>
    </div>--%>

    <div id="who_we_are">
        <h2>Who We Are</h2>
        <div class="container clearfix">
            <div class="threeicon_contain clearfix">
                <div class="icon_contain icfirst">
                    <img src="images/diverse_staff.png" />
                    <h3>Diverse Staff</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="DiverseStaff.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain">
                    <img src="images/phd_experts.png" />
                    <h3>PH.D. - Level Experts</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="PHDLevelExperts.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain iclast">
                    <img src="images/IT_team.png" />
                    <h3>Innovative IT Team</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="InnovativeItTeam.aspx" class="learn_btn">Learn More</a></p>
                </div>
            </div>
        </div>
    </div>

    <%--<div id="our_expertise">
        <div id="exholder">
            <div id="exh2holder">
                <h2>Our<br />
                    Expertise</h2>
            </div>
            <div id="exulholder">
                <ul class="expertise">
                    <li>
                        <img src="images/1.png" class="numimg" /><a href="ExpertisePolicyProgImplementation.aspx"> Policy and Program Implementation and Planning</a>
                    </li>
                    <li>
                        <img src="images/2.png" class="numimg" /><a href="ExpertiseManagConsultingLogis.aspx">Management Consulting and Logistics</a></li>
                    <li>
                        <img src="images/3.png" class="numimg" /><a href="ExpertiseStatisticalVisualization.aspx">Statistical Data Visualization, Analysis and Reporting</a></li>
                    <li>
                        <img src="images/4.png" class="numimg" /><a href="ExpertiseELearning.aspx">E-Learning, Web Tool, and Application Development</a></li>
                    <li>
                        <img src="images/5.png" class="numimg" /><a href="ExpertiseMultimediaComm.aspx">Multimedia Communications</a></li>
                    <li>
                        <img src="images/6.png" class="numimg" /><a href="ExpertiseItIntegration.aspx">Information Technology Integration</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>--%>

    <div id="our_expertise" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <div id="exholder">
            <div id="exh2holder">
                <h2>Our<br />
                    Expertise</h2>
            </div>
            <div id="exulholder">
                <ul class="expertise">
                    <li>
                        <img src="images/1.png" class="numimg" /><a href="ExpertisePolicyProgImplementation.aspx"> Policy and Program Implementation and Planning</a>
                    </li>
                    <li>
                        <img src="images/2.png" class="numimg" /><a href="ExpertiseManagConsultingLogis.aspx">Management Consulting and Logistics</a></li>
                    <li>
                        <img src="images/3.png" class="numimg" /><a href="ExpertiseStatisticalVisualization.aspx">Statistical Data Visualization, Analysis and Reporting</a></li>
                    <li>
                        <img src="images/4.png" class="numimg" /><a href="ExpertiseELearning.aspx">E-Learning, Web Tool, and Application Development</a></li>
                    <li>
                        <img src="images/5.png" class="numimg" /><a href="ExpertiseItIntegration.aspx">Information Technology Integration</a>
                        
                        
                 <%--   <li>
                        <img src="images/6.png" class="numimg" /><a href="ExpertiseMultimediaComm.aspx">Multimedia Communications</a></li>

                    </li>--%>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <%--<div id="our_work">
        <h2 class="greenHeading">Our Work</h2>
        <div class="container clearfix">
            <div class="threeicon_contain clearfix">
                <div class="icon_contain icfirst">
                    <img src="images/icon.gif" />
                    <h3>Project Portfolio</h3>
                    <p><a href="ProjectPortfolio.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain">
                    <img src="images/icon.gif" />
                    <h3>Case Studies</h3>
                    <p><a href="CaseStudies.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain iclast">
                    <img src="images/icon.gif" />
                    <h3>Subject Matter Experts</h3>
                    <p><a href="SubjectMatterExperts.aspx" class="learn_btn">Learn More</a></p>
                </div>
            </div>
        </div>
    </div>--%>

    <div id="our_work">
        <h2>Our Work</h2>
        <div class="container clearfix">
            <div class="threeicon_contain clearfix">
               <%-- <div class="icon_contain icfirst">
                    <img src="images/icon4.png" />
                    <h3>Project Portfolio</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="ProjectPortfolio.aspx" class="learn_btn">Learn More</a></p>
                </div>--%>
                <div class="icon_contain ic2_left">
                    <img src="images/case_studies.png" />
                    <h3>Case Studies</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="CaseStudies.aspx" class="learn_btn">Learn More</a></p>
                </div>
                <div class="icon_contain ic2_right iclast">
                    <img src="images/subject_expert.png" />
                    <h3>Subject Matter Experts</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p><a href="SubjectMatterExperts.aspx" class="learn_btn">Learn More</a></p>
                </div>
            </div>
        </div>
    </div>
    <%--<div id="flag_area">
        <p>Founded in 2003, Synergy has become the small business partner-of-choice for complex, high-profile —and high-stakes—projects that bring positive outcomes to neighborhoods across the country and around the world.</p>
    </div>--%>

    <div id="flag_area" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Founded in 2003, Synergy has become the small business partner-of-choice for complex, high-profile —and high-stakes—projects that bring positive outcomes to neighborhoods across the country and around the world.</p>
    </div>

     <div id="lets_talk">
        <div class="container clearfix">
            <div class="grid_4">
                <h4><a href="ConsultWithUs.aspx">Consult<br />
                    <span class="wus">With Us</span></a></h4>
            </div>
            <div class="grid_4">
                <h4><a href="PartnerWithUs.aspx">Partner<br />
                    <span class="wus">With Us</span></a></h4>
            </div>
            <div class="grid_4 omega">
                <h4><a href="CareersWithUs.aspx">Careers<br />
                    <span class="wus">With Us</span></a></h4>
            </div>
        </div>
    </div>

    
    <video autoplay poster="images/SEIvid_smoky.jpg" id="bgvid" loop>
        <source src="videos/SEIvid_smoky.webm" type="video/webm">
        <source src="videos/SEIvid_smoky.mp4" type="video/mp4">
    </video>
    <script type="text/javascript" src="js/skrollr.min.js"></script>
    <script type="text/javascript">
        var s = skrollr.init();
    </script>
    <!--[if lt IE 9]>
	<script type="text/javascript" src="skrollr.ie.min.js"></script>
	<![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu').slicknav();
        });
	</script>

    <script src="../responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: false,
                pager: false,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });
        });
  </script>

</asp:Content>

﻿<%@ Page Title="Our Expertise. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="OurExpertise.aspx.cs" Inherits="OurExpertise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


<div id="interior_landing" class="ourexpertise_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<div class="container">
		<div class="row">
        	<div class="col-md-12">
				<div id="interior_landing_text">
					<h1>our<br /><span class="land_purpleh1">expertise</span></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container landing_intro">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
           <%-- <div class="basic_content">--%>
                  <p>Our services help health research programs speed up the translation of laboratory breakthroughs to treatment options in a neighborhood clinic. We support programs that help students learn and help teachers teach.  Our services incorporate years of project-specific experience, innovations that improve outcomes, and an uncompromising commitment to excellence. We help clients advance initiatives locally, nationally, and internationally through the following services.</p>
                
                <%-- </div>--%>
        </div>
    </div>
</div>
    
<div class="container landing_icons">
	<div class="row">
    	<div class="col-xs-2 col-xs-15">
        	<div class="landing_card">
            	<a href="ProgramEvaluationAndImplementation.aspx"><img alt="PEI Logo" src="images/policy_planning.png" class="landicon" /></a>
                <h4><a href="ProgramEvaluationAndImplementation.aspx">Program Evaluation and Implementation</a></h4>
            </div>
        </div>
         <div class="col-xs-2 col-xs-15">
        	<div class="landing_card">
            	<a href="MultimediaCommunication.aspx"><img alt="Multimedia Comm Logo" src="images/statistics_icon.png" class="landicon" /></a>
                <h4><a href="MultimediaCommunication.aspx">Multimedia Communication</a></h4>
            </div>
        </div>

        <div class="col-xs-2 col-xs-15">
        	<div class="landing_card">
            	<a href="InformationTechnologyIntegration.aspx"><img alt="IT Logo" src="images/info_tech_icon.png" class="landicon" /></a>
                <h4><a href="InformationTechnologyIntegration.aspx">Information Technology Integration</a></h4>
            </div>
        </div>

        <div class="col-xs-2 col-xs-15">
        	<div class="landing_card">
            	<a href="WebDesignApplicationDevelopmentandE-Learning.aspx"><img alt="Web Design Logo" src="images/elearning_icon.png" class="landicon" /></a>
                <h4><a href="WebDesignApplicationDevelopmentandE-Learning.aspx">Web Design, Application Development, and E-Learning</a></h4>
            </div>
        </div>
        

        <div class="col-xs-2 col-xs-15">
        	<div class="landing_card">
            	<a href="ManagementConsultingAndLogistics.aspx"><img alt="Management Consulting Logo" src="images/management_icon.png" class="landicon" /></a>
                <h4><a href="ManagementConsultingAndLogistics.aspx">Management Consulting and Logistics</a></h4>
            </div>
        </div>
    	

       
    </div>
</div>
    
    
</asp:Content>


﻿<%@ Page Title="Research and Evaluation and Technical Assistance" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProgramEvaluationAndImplementation.aspx.cs" Inherits="Exp_PolicyProgImplementation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double stick-wrapper">
        <div class="row">
            
            
            <div class="col-md-6" id="side-sticky">
                <div class="theiaStickySidebar">  <h1 class="bigh1"> Research and Evaluation and Technical Assistance   </h1>
                <div class="cs_button_holder">
                    <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click"  CssClass="btn btn-success" />--%>

                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
              </div>  
            </div>
            <div class="col-md-6" id="main-content">
                <p>
                  The country’s health, education, and other public initiatives require rigorous evaluation designs to ensure that their development and implementation phases remain aligned with program and service goals. Effective evaluation plans must be grounded in evidence-based research and practice—while being flexible enough to respond to shifting priorities and emerging practices in the field. Synergy’s doctorate-and masters-level researchers and evaluators develop the most appropriate evaluation designs using state-of-the art sampling, data collection, and analysis methods to answer critical questions and guide future program decisions. We have successfully applied these methods at the community, state, regional, and federal levels. 
                </p>
                
                
<!-- Start Collapse-->

          
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                      <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo_A">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo_A" aria-expanded="false" aria-controls="collapseTwo_A">Research and Evaluation
                                    </a>
                                </h3>
                            </div>
                            <div id="collapseTwo_A" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_A">
                                <div class="panel-body">
                                   
                                     <p> Synergy’s research and evaluation professionals provide state-of-the-art, rigorous, cost-effective designs and methods. We emphasize collaboration as we support evidence-based programs 
                                        around the country with data that helps them improve performance and build capacity. Our team includes researchers, statisticians, and epidemiologists with a broad range of subject matter
                                        expertise in areas such as public health and education.  </p>

                                    <p> Synergy’s evaluation team designs needs assessments; experimental and quasi-experimental studies; trend, gap, and cost-benefit analyses; environmental scans; and case studies using the methods 
                                        most appropriate for the program. Our data collection approaches include: </p>

                <ul>
                    <li>Site visits</li>
                    <li>Focus groups</li>
                    <li>Cognitive interviews</li>
                    <li>Multimodal surveys, including online surveys</li>
                    <li>Geostatistical mapping</li>
                    <li>Literature reviews</li>
                    <li>Environmental scans  </li>

                </ul>

                 <p>In addition, our research and evaluation designs and methods build in compliance with Government Performance and Results Act (GPRA) and Office of Management and Budget (OMB) requirements and standards.  </p>

                <p> Our analytic skills range from the quantitative—such as national estimates using complex survey and sampling design, experiments embedded in complex surveys, appropriate tests of significance, and multivariate 
                    data analyses—to qualitative approaches, such as content analysis, coding of open-ended questions, and focus groups. We bring rigor and objectivity to all our work by following the position statements on ethical 
                    and cultural competency set forth by the American Educational Research Association, the American Evaluation Association, the Agency for Healthcare Research and Quality, the Health Resources and Services Administration, 
                    and the American Association of Public Opinion Researchers (AAPOR).  </p>
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo_A" aria-expanded="false" aria-controls="collapseTwo_A"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Technical Assistance</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p> Synergy develops technical assistance (TA) strategies that include powerful and diverse resources and dissemination vehicles. We believe the foundation of effective TA is close collaboration with clients so that we can identify the activities and tools that will best strengthen programs and improve service quality.  </p>
                <p>Our proven approach to comprehensive TA is built on four key components:  </p>
                <ul>
                    <li>Knowledge collection and management </li>
                    <li>Knowledge creation and product development </li>
                    <li>Knowledge transfer and dissemination </li>
                    <li>Project management and evaluation</li>
                </ul>
                <p> Synergy’s scaled and flexible TA can provide:  </p>
                <ul>
                    <li>Targeted services that focus on specific groups, delivered through webinars, conferences, site visits, and other approaches </li>
                    <li>Intensive services that address specific issues  </li>
                    <li>Broad dissemination of information and resources to policy makers, administrators, practitioners, and families </li>
                    <li>In-person training and skill building   </li>
                </ul>
                <p> Our custom-built TA strategies consider adult learning practices and are based on the needs of the target audience. Additionally, we have the flexibility to respond to ad hoc needs and requests, provide continuous refinement of services, and offer sustained TA when needed.   </p>
                
                

                <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>


                   </div>
                </div>
         
        </div>


<!-- End Collapse -->

            </div>
        </div>
    </div>

  

    <div class="expertise_policy_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p> We provide comprehensive research services and technical assistance to guide program decisions and build local capacity.  </p>
    </div>

</asp:Content>


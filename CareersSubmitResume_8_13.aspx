﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CareersSubmitResume.aspx.cs" Inherits="CareersSubmitResume" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-5">
                <h1 class="bigh1">Submit Resume</h1>
            </div>
               <div class="col-md-6 submitresume">
                <p>
                    <div class="label-align"><asp:Label ID="lblName" runat="server" Text="Name"></asp:Label></div>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                </p>
                <p>
                    <div class="label-align"><asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label></div>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </p>
                 <p>
                    <div class="label-align"><asp:Label ID="lblPhone" runat="server" Text="Phone"></asp:Label></div>
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                </p>
                 <p>
                    <div class="label-align"><asp:Label ID="lblPosition" runat="server" Text="Position"></asp:Label></div>
                    <asp:TextBox ID="txtPosition" runat="server"></asp:TextBox>
                </p>
                 <p>
                    <div class="label-align"><asp:Label ID="lblSalaryRequirement" runat="server" Text="Salary Requirement"></asp:Label></div>
                    <asp:TextBox ID="txtSalaryRequirement" runat="server"></asp:TextBox><br />
                     (NOTE: Applications that lack salary requirement information will not be accepted)</p>
         
                <p class="formnopad">
                     <asp:Label ID="lblCover" runat="server" Text="Upload Cover Letter (only .doc or.pdf)"></asp:Label>
                    <asp:FileUpload ID="FU1CoverLetter" runat="server" /></p>
                <asp:RegularExpressionValidator ID="revCover" runat="server"
                    ControlToValidate="FU1CoverLetter" ErrorMessage="only .doc or.pdf files are allowed"
                    ValidationExpression="(.*?)\.(doc|pdf|DOC|PDF|)$" ForeColor="Red" ValidationGroup="Save">
                </asp:RegularExpressionValidator>
                 <p class="formnopad">
                      <asp:Label ID="lblResume" runat="server" Text="Upload Resume (only .doc or.pdf)"></asp:Label>
                    <asp:FileUpload ID="FU2Resume" runat="server" />
                     <asp:RegularExpressionValidator ID="revResume" runat="server"
                    ControlToValidate="FU2Resume" ErrorMessage="only .doc or.pdf files are allowed"
                    ValidationExpression="(.*?)\.(doc|pdf|DOC|PDF|)$" ForeColor="Red" ValidationGroup="Save">
                </asp:RegularExpressionValidator>
                 </p>

                <p><asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Save" OnClick="btnSubmit_Click" CssClass="btn btn-primary" /></p>
                 <p><asp:Label ID="lblcoverMess" runat="server"></asp:Label></p>
                 <p><asp:Label ID="lblResMess" runat="server"></asp:Label></p>
                <p><asp:Label ID="lblMessage" runat="server"></asp:Label></p>
            </div>
        </div>
    </div>

</asp:Content>


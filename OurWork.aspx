﻿<%@ Page Title="Our Work. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="OurWork.aspx.cs" Inherits="OurWork" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div id="interior_landing" class="ourwork_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<div class="container">
		<div class="row">
        	<div class="col-md-12">
				<div id="interior_landing_text">
					<h1>our<br /><span class="land_purpleh1">work</span></h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
        	<%--<div class="basic_content">--%>
              <%--  <h2>Synergy builds partnerships that last.</h2>--%>
                  <p>Since 2003, Synergy has expanded from helping one federal agency to helping 12 federal agencies meet their missions in the public health and public education arenas. And within each of these client agencies our productive relationships have expanded into new Offices, Centers, and Branches: Our global experience, cultural knowledge, and multilingual skills add depth to our support and guarantee success to our clients, who demand innovation and creative collaboration.</p>        
           <%-- </div>--%>
        </div>
    </div>
</div> 
<div class="container landing_icons">
	<div class="row 3colrow">
    	<div class="col-sm-4">
        	<div class="landing_card">
            	<a href="CaseStudies.aspx"><img alt="Case StudiesIcon"  src="images/case_studies.png" class="landicon" /></a>
                <h4><a href="CaseStudies.aspx">Case Studies</a></h4>
            </div>
        </div>
        <div class="col-sm-4">
        	<div class="landing_card">
            	<a href="SubjectMatterExperts.aspx"><img alt="Subject Matter Icon" src="images/subject_expert.png" class="landicon" /></a>
                <h4><a href="SubjectMatterExperts.aspx">Subject Matter Experts</a></h4>
            </div>
        </div>
        <div class="col-sm-4">
        	<div class="landing_card">
            	<a href="AwardsAndRecognition.aspx"><img alt="Awards Icon" src="images/awards.png" class="landicon" /></a>
                <h4><a href="AwardsAndRecognition.aspx">Awards and Recognition</a></h4>
            </div>
        </div>
    </div>
</div>

  
    
</asp:Content>


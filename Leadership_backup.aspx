﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Leadership.aspx.cs" Inherits="Leadership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div class="container landing_padding_double">
	<div class="row">
    	<div class="col-md-5">
        	<h1 class="bigh1">Leadership</h1>
        </div>
        <div class="col-md-7">
        	<p>Suspendisse eleifend nulla vitae ante pellentesque, quis ornare sapien vestibulum. Maecenas eleifend augue ipsum, et pellentesque elit consectetur at. Suspendisse efficitur porta pellentesque. Proin quis gravida elit, at pulvinar turpis. Praesent sed posuere nulla, et porta odio.</p>
        </div>
    </div>
</div>

<div class="container leadercontent">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
        	<div class="prachee">
        	<h2>Prachee Davidas</h2>
            <h3>President and CEO of Synergy Enterprises, Inc.</h3>
            	<p>Prachee Devadas has a career marked by more than two decades of success in Federal contracting. Since 1996, she has managed contracts totaling more than $100 million. She founded Synergy in 2003 and since then the company has steadily grown its client portfolios, expanded its core services, and introduced innovative and cutting-edge technologies—interrelated aspects of business that enable the company to provide the maximum rate of return to each client. The insight, dedication, and comprehensive understanding of client-centered service that Ms. Devadas brings to each Synergy project allowed the company to expand from serving clients in a single federal agency to more than a dozen agencies. Her attention to client needs has allowed Synergy to maintain existing relationships and expand into new Offices, Branches, and Centers because her company builds partnerships that last. Ms. Devadas’ reputation for conducting business with integrity and consistently high performance is the primary reasons that Synergy enjoys a track record of happy clients and partners, and loyal staff. In addition to membership in several professional associations, Ms. Devadas is a speaker for the National Association of Professional Asian Women; serves on the board of directors for So Others Might Eat; and is active with the NIH Children’s Inn, which provides residential respite for terminally ill children and their families.</p>
            </div>
        </div>
    </div>
	<div class="row">
    	<div class="col-md-5 col-md-offset-1">
        	<div class="leaders">
        	<h3>Christine Hartley</h3>
            <h4>Vice President of Operations and Director of Synergy’s Division of Conferences and Peer Review disaster recovery, risk mitigation and security test plans, quality assurance standards, and configuration management plans.</h4>
				<p>Christine Hartley has more than 25 years of Federal contract management experience and currently manages a staff of 35 meeting professionals and grants specialists who support more than 100 meetings and grant activities annually. Ms. Hartley has developed quality-assurance policies and standard operating procedures that cut across all grant-review and conference activities at Synergy and ensure that quality services within contract-stipulated guidelines are delivered to all clients. She has streamlined activities and increased cost efficiencies on all of SEI’s conference and grant-review contracts. Ms. Hartley is well-versed in both technical and financial administration of contracts and has managed multiyear, multimillion-dollar contracts covering a broad range of services, including technical assistance, training, evaluation, publications, and conference management.</p>
			</div>
        </div>
        <div class="col-md-5">
        	<div class="leaders">
        	<h3>Roy Walker</h3>
            <h4>Director of Synergy’s Division of Evaluation and Assessment</h4>
				<p>Roy Walker, M.B.A., has more has 30 years’ experience in program management, including 20 years directing federal projects that provide training, technical assistance (TA), health education, and information dissemination. He has provided management oversight and supervision for federal health-related projects and grants with a combined value of more than $40 million for HHS, ED, other federal agencies and community-based health and education associations. For these projects, he managed tasks that required training and TA, curricula development, maintenance of databases and websites, electronic communication forums, and the development of information dissemination, materials, and outreach strategies. He has presented on health- and education-related topics, including effective models for peer education programs and cultural approaches to substance abuse prevention, at more than 15 national conferences.</p>
			</div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-5 col-md-offset-1">
        	<div class="leaders">
        	<h3>Patrick Zickler</h3>
            <h4>Director of Synergy’s Division of Communication Services</h4>
				<p>Patrick Zickler manages a team of writers, editors, graphic designers, 508 compliance specialists, animators, and other communications professionals. Mr. Zickler has extensive experience working with senior federal staff at the NIH and other HHS institutes and agencies to develop, plan, and manage communications projects that effectively blend scientific and editorial accuracy with the important creative contributions of writers and designers. The Division’s work has been recognized with nine Blue Pencil and Gold Screen awards from the National Association of Government Communicators. Mr. Zickler has more than three decades of experience in research, writing, and communications management in the fields of biomedical research and public health. He has special expertise in behavioral health, particularly the areas of drug addiction and abuse, tobacco-related issues, HIV/AIDS; and health care disparities.</p>
            </div>
        </div>
        <div class="col-md-5">
        	<div class="leaders">
        	<h3>Robbie Levenberry</h3>
            <h4>Director of Synergy’s Division of IT Strategy </h4>
				<p>Rob Levenberry leads and manages a technical team of more than 20 information technology (IT) professionals and ensures operations plans, policies, procedures, and transition/migration plans are consistent with the overall company goals and objectives. Mr. Levenberry has more than 15 years of experience in government contracting environments and has a sophisticated understanding of project management, strategy, and IT governance. He is responsible for staffing and budgeting projections on a company-wide and IT project specific basis. He advises Project Directors regarding contract-specific IT requirements, including development of timelines, appropriate technology, and general project guidance. He develops and maintains policies, guidelines, standards, and procedures for IT operations including system security,</p>
			</div>
        </div>
    </div>
</div>
 
</asp:Content>


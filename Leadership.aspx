﻿<%@ Page Title="Leadership. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Leadership.aspx.cs" Inherits="Leadership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-4">
                <h1 class="bigh1">Leadership</h1>
            </div>
            <div class="col-md-8">
                
                <p>Synergy’s leadership staff embodies decades of professional excellence and a shared commitment: We are shaping Synergy’s excellent performance and steady growth into services that help our clients succeed.  </p>
            </div>
        </div>
    </div>
             <!-- Start Collapse-->

 
                <div class="fancy-collapse-panel leadership">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="container leadercontent">
    
    
        
        <div class="row leaderrow prachee">
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Prachee Devadas" src="images/leaders_prachee.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Prachee J. Devadas</h3>
                        <h4>President and CEO of Synergy Enterprises, Inc.</h4>  
                                      
                        <p> Prachee J. Devadas has a career marked by more than two decades of success in federal contracting. Since 1994, she has provided oversight for contracts totaling more than $600 million. Under her leadership, Synergy has expanded its reach from assisting one federal agency to assisting 12 federal agencies as they fulfill their missions, primarily in the public health and public education arenas. Ms. Devadas ensures that Synergy is implementing innovative technologies and practices that connect and serve people, programs, and communities. Under her leadership, the company was ranked 14th on Washington Technology's 2008 List of Top 25 Nationwide 8(a) Firms.</p> 
                        
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                               
                        
                        <p>Ms. Devadas was honored as the 2009 Washington Metropolitan Area District Small Business “Person of the Year” by the U.S. Small Business Administration. In 2009, she was awarded a Proclamation from the Montgomery County Council in Maryland, where Synergy resides. As a result of her insight into federal contract management and her reputation for conducting business with integrity, Ms. Devadas was named one of the top five “Executives of the Year” by GovCon in 2010. Synergy was named the 2010 “Contractor of the Year” by GovCon—one of only five firms to achieve this honor. </p>                   
                 
                       <p>Her consistent drive and unfailing commitment to ensuring client goals was recognized in 2011 by President Obama during a press conference in the Rose Garden, where he singled out Synergy’s success as representative of the promise of opportunity in America. Ms. Devadas was also one of 15 business owners invited to the White House on September 27, 2011, to meet with President Barack Obama, Treasury Secretary Timothy F. Geithner, Maryland Governor Martin O'Malley, and members of Congress when the President signed the Small Business Jobs Act, designed to help small businesses hire new workers. Following the signing, Ms. Devadas was interviewed about small business concerns in a press conference at the White House. </p>
                    
                        <p>In addition to memberships in several professional associations, Ms. Devadas is a speaker for the National Association of Professional Asian Women. She has been featured several times on Executive Leaders Radio, serves on the Board of Directors for So Others Might Eat (S.O.M.E.), and is on the Advisory Board of BB&T Bank. Ms. Devadas is active with the NIH Children’s Inn, which provides residential respite for terminally ill children and their families.  </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    
                                </div>
                            </div>
                        </div>   
                </div>
            </div>
        </div>
        
        
        <div class="row leaderrow">
        <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Christin Hartley" src="images/leaders_christine.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Christine Hartley</h3>
                    <h4>Vice President of Operations and Director of Synergy’s Division of Conferences and Peer Review</h4>
                    <p>Christine Hartley has more than 25 years of federal contract management experience. She leads a staff of meeting professionals and grant review specialists that support more than 100 meetings annually. During the past 15 years, she has served as project director or corporate monitor for public health projects with a combined value of more than $40 million for federal agencies such as the Centers for Disease Control and Prevention (CDC) and the National Institutes of Health (NIH). She developed and implemented cost-saving international grant review programs within the Department of State and has streamlined activities and increased cost efficiency on all our conference and grant review contracts.</p> 
                    
                       <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">   
                             
                    <p>Ms. Hartley is well-versed in both the technical and financial administration of contracts. She has managed multiyear, multimillion-dollar contracts covering a broad range of services—including technical assistance, training, evaluation, publications, and conference management. She has managed high-profile, Cabinet-level activities in the U.S. and internationally. These projects include conferences related to the NIH Roadmap, Broader Middle East and North Africa (BMENA) Initiative meetings in Jordan and Morocco, and the 2012 G8-BMENA Initiative 9th Forum for the Future in Tunisia. Ms. Hartley has received numerous commendations for her expertise in contracts and fiscal management from project officers, contract officers, and task leaders. </p>
                    
                                                        <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>   
                </div>
            </div>
        </div>

        <div class="row leaderrow">
        <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Janet Blank" src="images/leaders_janet.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Janet L. Blank, PHR</h3>
                    <h4>Director of Synergy’s Human Resources Department</h4>                   
                    <p>Janet Blank and her staff are responsible for Synergy’s recruiting and hiring processes, benefits program, employee relations, employee engagement, diversity, and wage and hour compliance. She has created a strong team with low turnover, which reflects an organization that provides a congenial working environment, generous benefits, and advancement opportunities for staff. </p>
                    
                                           <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">   
                    
                    
                    <p>Ms. Blank began her career in human resources providing benefits support for a large Virginia-based real estate investment trust, helping to streamline the open enrollment process and find creative solutions to increase 401(k) enrollment. Ms. Blank has also worked in the not-for-profit industry, leading the human resources department at Independent Community Bankers of America (ICBA). She joined ICBA shortly after the banking crisis and assisted the organization with the resulting fluctuations in staffing needs and budget requirements.</p>
                    
                    <p>Ms. Blank is a graduate of Barnard College and George Washington University Law School. She is admitted to the Maryland bar and has used her knowledge of employment law to enhance her role as a human resources professional. Ms. Blank has been certified as a Professional in Human Resources (PHR) since 2005. She continues her education through her membership and active participation in the Society of Human Resource Management (SHRM), where she has been a member since 2003.</p>
                    
                                                                            <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>   
                </div>
            </div>
        </div>

        <div class="row leaderrow">
            <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Rob Levenberry" src="images/leaders_robbie.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Robbie Levenberry</h3>
                    <h4>Director of Synergy’s Division of IT Strategy </h4>
                    <p>Rob Levenberry, M.S., PMP, leads a technically diverse team of information technology (IT) professionals. He is responsible for aligning technology development and corporate strategy so that Synergy can provide tailored technical solutions for each client. He supports the development of strategic partnerships and incorporates new and emerging technologies. Mr. Levenberry has served as an advisor to numerous senior executives and has successfully managed over $60 million in IT projects across the federal sector. During his 15 years of experience in government contracting and consulting, Mr. Levenberry has led major IT initiatives for federal clients such as the U.S. Departments of Agriculture, Justice, and Treasury. </p>
                    
                                                  <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">             
                    
                    <p>In addition, Mr. Levenberry is responsible for staffing and budget projections for both IT projects and the company overall. He advises project directors on IT requirements for each contract, including timelines and necessary technology. He develops and maintains policies, guidelines, standards, and procedures for IT operations—from system security to disaster recovery, risk mitigation, security test plans, quality assurance, and configuration management. As a consultant, he designed several proprietary Project Management and Application Development Solutions (Enhanced PMO Delivery System [EPMODS] and the Rapid Assessment and Application Development Solutions (RAADS) that were leveraged to increase application development and deployment velocity. Mr. Levenberry is an active member of the Project Management Institute (PMI) and Information Systems Audit and Control Association (ISACA) and is actively engaged in community and family activities.</p>
                    
                                                                            <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>   

                </div>
            </div>
        </div>
        <div class="row leaderrow">
            <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Roy Walker" src="images/leaders_roy.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Roy Walker</h3>
                    <h4>Director of Synergy’s Division of Assessment and Evaluation Services</h4>
                    <p>Roy Walker, M.B.A., has more has 30 years of experience in program management, including 20 years directing federal projects that provide training, technical assistance (TA), health education, and information dissemination. He has provided oversight and supervision for federal health-related projects and grants with a combined value of more than $40 million for the Department of Health and Human Services (DHHS), the Department of Education (ED), other federal agencies, and community-based health and education associations.  </p>
                    
                    
                      <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">   
                    
                    
                    <p>Mr. Walker primarily leads contracts that focus on public health and education. For example, he serves as Corporate Monitor for an effort by the U.S. Department of Transportation to counter a growing health disparity: the underuse of seat belts by African Americans. Mr. Walker also serves as Corporate Monitor and Subject Matter Expert for the Substance Abuse and Mental Health Services Administration (SAMHSA’s) National Campaigns contract and as Project Director for the National Institute on Drug Abuse (NIDA’s) AIDS Research Program (ARP) support contract. 
                       Additionally, he directs the Afterschool Best Practices in Early Childhood Education project, part of ED’s 21st Century Community Learning Centers (21st CCLC) initiative.</p>
                    <p>The projects under Mr. Walker’s management involve training and TA, curriculum development, maintenance of databases and websites, electronic communication forums, information dissemination, materials development, and outreach strategies. Mr. Walker has in-depth subject matter expertise in cultural competency and knowledge of racial/ethnic populations; substance abuse prevention, treatment, and recovery; AIDS research; capacity building; and public-private partnership development. His expertise in education is grounded in experience as a classroom science teacher 
                       and encompasses early childhood; students with disabilities; science, technology, engineering, and mathematics (STEM); afterschool and homeless education; and emergency management planning and response. He has presented on numerous health and education topics, including effective models for peer education and cultural approaches to substance abuse prevention, at more than 15 national conferences. Mr. Walker is a former Director of Hillcrest Children and Family Center, a not-for-profit behavioral health care and social services agency founded in 1815 by First Lady Dolly Madison. 
                       It provides behavioral health prevention and treatment and community and family support services. </p>
                       
                        <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>   
                       
                       
                </div>
            </div>
        </div>

    <%--    <div class="row leaderrow">
            <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Moses Yogarah" src="images/leaders_moses.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Moses J. Yogaraj</h3>
                    <h4>Controller</h4>
                    <p>Moses Yogaraj is a chartered accountant/cost accountant with substantial experience in financial and cost accounting, cost and management audits, and public accounting. He has more than 20 years of experience managing the accounting departments of medium-sized companies in the federal government arena.</p>
                    
                      <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">  
                    
                    
                    <p>Mr. Yogaraj has been with Synergy since 2005. He is well-versed in government contract accounting systems, DCAA audits, indirect cost rates, business and cost proposal pricing, and establishment of new accounting systems using the Deltek Government Contract Accounting software. He also coordinates with company auditors, audit agencies, company attorneys, and bankers.</p>

<h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>   

                </div>
            </div>
        </div>--%>

   <%--     <div class="row leaderrow">
            <div class="leaderline"></div>
            <div class="col-md-3 col-md-offset-1">
                <img alt="Photo Patrick Zickler"  src="images/leaders_patrick.png" class="leaderpic" />
            </div>
            <div class="col-md-7">
                <div class="leaders">
                    <h3>Patrick Zickler</h3>
                    <h4>Director of Synergy’s Division of Communication Services</h4>
                    <p>Patrick Zickler leads Synergy’s team of writers, editors, graphic designers, 508 compliance specialists, animators, and other communications professionals. Under his leadership, the company has been awarded nine Blue Pencil and Gold Screen awards from the National Association of Government Communicators. Mr. Zickler has extensive experience working with senior federal staff to develop, plan, and manage communications projects that blend scientific and editorial accuracy with the important creative contributions of writers and designers.  </p>
                   
                    <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                               <h4 class="panel-title text-left xmore">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">. . . .</a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">   
                   
                   
                    <p>Mr. Zickler has more than three decades of experience in research, writing, and communications management in the fields of biomedicine and public health. He brings special expertise in behavioral health, particularly drug addiction and abuse, tobacco-related issues, HIV/AIDS, and health care disparities. He was selected by the Tobacco Control Research Branch of the National Cancer Institute (NCI) to be a delegate to the 2009 World Conference on Tobacco held in Mumbai, India. He has written science, medicine, and technology content for elementary and
                       middle school textbooks and was selected as a subject matter expert and principal writer for the tobacco chapter (“Toward a Tobacco-Free Society”) for the 10th (2005) and 11th (2009) editions of McGraw-Hill’s widely-used undergraduate text, Core Concepts in Health. He also was chosen to serve as subject matter expert and principal writer for the Drug Abuse and Addiction chapter of the 12th (2013) and 13th (2015) editions of Core Concepts.   </p>
                    <p>Mr. Zickler won the 2010 National Institutes of Health (NIH) Plain Language and Clear Communication Award for work done for the National Institute on Diabetes and Digestive and Kidney Diseases (NIDDK) and the 2008 Gold Hermes Creative Award from the Association of Marketing and Communications Professionals for work done for NCI.</p>
                    <p>Mr. Zickler served as the first Director of Miriam’s Kitchen—a Washington, DC not-for-profit agency that helps meet the short-term needs of chronically homeless men and women and he has remained involved with the organization for 30 years. He has made four trips to India, where he and his family provide support to Reality Gives, a Mumbai-based nongovernmental organization that improve the lives of people in Dharavi, India’s largest slum. His support has helped to develop Dharavi’s first community library, establish a children’s art and photography program, 
                      and expand English-language learning and computer proficiency programs for young adults.</p>
                      
                      <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>

                                </div>
                            </div>
                        </div>  


                </div>
            </div>
        </div>--%>
              </div>

            </div>
        </div>





</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

     <script type="text/javascript">
         function checkWordLen(obj, wordLen, cid) {
             var len = obj.value.split(/[\s]+/);
             $("#" + cid).val(len.length.toString());
             if (len.length > wordLen) {
                 alert("You've exceeded the " + wordLen + " word limit for this field!");
                 obj.value = obj.SavedValue;
                 len = obj.value.split(/[\s]+/);
                 $("#" + cid).val(len.length.toString());
                 return false;
             } else {
                 obj.SavedValue = obj.value;
             }
             return true;
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

        <div class="container landing_padding_double">
	<div class="row">
    	<div class="col-md-3">
        	<div class="addressinfo">
        		<h2>Main Office</h2>
					<p>Main Office<br />8757 Georgia Avenue<br />Suite 1440<br />Silver Spring, MD 20910<br /><br />Phone: (240) 485-1700<br />Fax: (240) 485-1717<br />E-mail: <a href="mailto:info@SEIservices.com" target="_blank">info@SEIservices.com</a></p>
        	</div>
        </div>
        <div class="col-md-9">
        	<div class="gettinghere">
            	<h3>Map Directions</h3>
                        <p>Click on the map to get door-to-door driving directions, <a href="https://www.google.com/maps?f=q&source=embed&hl=en&geocode&q=8757+Georgia+Avenue,+Silver+Spring,+MD&aq=1&sll=37.0625,-95.677068&sspn=53.477264,66.621094&ie=UTF8&hq&hnear=8757+Georgia+Ave,+Silver+Spring,+Montgomery,+Maryland+20910&ll=39.006579,-77.027893&spn=0.023344,0.036478&z=14&iwloc=A" target="_blank" title="View Larger Map">View Larger Map</a> or use the instructions below.</p>
                        <p>From points North, West, or East, take the Washington, DC Beltway (I-495), to exit 31, Georgia Avenue South. Synergy is located about 1 mile from the exit, just past Spring Street, on the left. Metered parking is available on Spring Street, or in the parking garage behind the Sheraton Hotel. </p>
                        <p>From Washington, DC, go North on North Capitol Street and continue onto Blair Road NW until you reach Georgia Avenue. Turn right onto Georgia Avenue and go approximately 1 mile. Synergy is located on the right. Metered parking is available on Spring Street or in the parking garage behind the Sheraton Hotel. </p>

                        <h4>Metro and Walking Directions</h4>
                        <p>Synergy is conveniently located 4 blocks from the Silver Spring Metro Station on the red line. Additionally, many buses drop off at the station and/or on Georgia Avenue near our office. </p>
                        <p>To walk to Synergy from the Metro station, exit on the bus' side and walk uphill on Colesville Road. Turn left onto Georgia Avenue. Synergy is located 3 blocks up on the right side of the street.</p>
            </div>
        </div>
    </div>
</div>

<div class="google-maps">
	<iframe title="Google Map Direction to SEI" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3100.7320594815783!2d-77.0299615!3d38.9986111!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c8b77a8313eb%3A0x885af858c6bbadb9!2s8757+Georgia+Ave%2C+Silver+Spring%2C+MD+20910!5e0!3m2!1sen!2sus!4v1429196859892" width="800" height="600" frameborder="0" style="border:0"></iframe>
</div>

<%--<div class="container contactform"><a name="cf_anchor"></a>
	<div class="col-md-8 col-md-offset-2">
    	<div class="cf_contain">
           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                <h1>Contact Synergy</h1>
                <p>
                    <asp:Label ID="lblName" runat="server" Text="Your First Name" ></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="name" ControlToValidate="txtName" runat="server" ErrorMessage="First Name is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="lblLastName" runat="server" Text="Your Last Name"></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLastName" ControlToValidate="txtLastName" runat="server" ErrorMessage="Last Name is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="lblEmail" runat="server" Text="Your Email" ></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="Email address is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                     <asp:RegularExpressionValidator ID="revEmail" runat="server"
                    ControlToValidate="txtEmail" ErrorMessage="Email address is not valid"
                    ValidationExpression="[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}" ForeColor="Red" ValidationGroup="Save">
                </asp:RegularExpressionValidator>
                   
                </p>
                <p>
                    <asp:Label ID="lblCompantName" runat="server" Text="Your Company Name"></asp:Label>
                    <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="100"></asp:TextBox>
                </p>

                <p>
                    <asp:Label ID="lblyourMessage" runat="server" Text="Your Message "></asp:Label> (<i>Maxminum 275 words are allowed </i>)<span style="color: red">*</span><br />

                    <asp:TextBox ID="txtMessage" runat="server" Width="300px" Height="150px" TextMode="MultiLine" 
                         SavedValue="" MaximumLength="275" onkeyup="checkWordLen(this, 275, 'stmFidelityGoalcnt');"  ></asp:TextBox>
                     <input type="text" disabled="disabled" size="2" id="stmFidelityGoalcnt" />  
                   
                    <asp:RequiredFieldValidator ID="rfvMessage" ControlToValidate="txtMessage" runat="server" ErrorMessage="Message is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regmess" runat="server" ControlToValidate="txtMessage"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,1999}$" ErrorMessage="Maximum 2000 characters or 275 words are allowed"
                        ForeColor="Red">Maximum 2000 characters or 275 words are allowed
                    </asp:RegularExpressionValidator>
                </p>

                  <p >
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                            <asp:Image ID="imgCaptcha" runat="server" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </p>
                <p>
                    <asp:Label ID="lblCaptcha" runat="server" Text="Enter the characters shown in the image"></asp:Label><span style="color: red">*</span><br />
                    <asp:TextBox ID="txtCaptcha" MaxLength="15" runat="server" Width="200px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="reqCaptcha" ControlToValidate="txtCaptcha" runat="server" ErrorMessage="Characters shown in the image is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>


              

                <div>
                    <p>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Save" OnClick="btnSubmit_Click" CssClass="btn btn-primary" />
                    </p>
                    <p>
                        <asp:Label ID="conMessage" runat="server" ForeColor="Red"></asp:Label>
                    </p>

                </div>

                <asp:ValidationSummary ID="valsumm" ValidationGroup="Save" runat="server" DisplayMode="BulletList" ForeColor="Red" />

           


        </div>
    </div>
</div>--%>


</div>
    
</asp:Content>


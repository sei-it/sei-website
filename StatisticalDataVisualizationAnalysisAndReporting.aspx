﻿<%@ Page Title="Statistical Data Visualization, Analysis and Reporting. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StatisticalDataVisualizationAnalysisAndReporting.aspx.cs" Inherits="ExpertiseStatisticalVisualization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">Statistical Data Visualization, Analysis, and Reporting</h1>
                <div class="cs_button_holder">
                <%-- <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success" />--%>

                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
            </div>
            <div class="col-md-6">
                <h3>Interactive data displays</h3>
                <p>Our IT specialists designed, tested, and deployed an interactive School District Demographics System (SDDS) that displays detailed school district demographic survey data from throughout the United States and Puerto Rico. We used U.S. Census and American Community Survey data integrated with Common Core of Data information on 879 U.S. school districts as the foundation for an interactive website. Our portal and associated tools allow National Center for Education Statistics (NCES) to display and disseminate data in an accessible and informative manner.</p>
                <h3>Grant data management</h3>
                <p>We developed and created an Evaluation and Technical Assistance Center (ETAC) Web portal that currently hosts information on K-12 Military Partnership grantees in 412 schools throughout 67 districts across 25 states. Our IT specialists configured the ETAC server to comply with Defense Information Systems Agency guidelines. Specially designed Web data collection instruments for quarterly and annual reports and key grant performance indicators allow both Synergy and ETAC project staff to retrieve and print reports on grantees' progress and budget status.</p>
                <h3>Report design</h3>
                <p>Our team of highly skilled desktop publishers, quality assurance reviewers, and designers provide document preparation services for the U.S. Department of Education's Condition of Education, Digest of Education Statistics, and Projections of Education Statistics—and a variety of other recurring and one-time analytic, indicator, and tabular reports on education statistics. Synergy's team members ensure that the publications' content—including some 450 figures and tables—conforms to NCES Statistical Standards. </p>
                <h3>Statistical analyses</h3>
                <p>We conduct analyses to develop reports using Restricted Use Data (RUD) for The U.S. Department of Education's NCES to prepare the annual School Survey on Crime and Safety and School Crime Supplement to the National Crime Victimization Survey.</p>
                <h3>Accessible formats</h3>
                <p>Technical assistance specialists within the Magnet Schools Assistance Program (MSAP) researched and wrote a targeted newsletter designed by our senior graphic designers. Our Web and communications specialists ensure that these newsletters and other content on the MSAP website—whether developed by Synergy, provided by the U.S. Department of Education, or obtained from vetted third-party sources—are fully accessible and Section 508 compliant. We also support the development of materials for ED's communities of practice as part of several initiatives, including Investing in Innovation (i3), the Office of Special Education Programs' technical assistance centers, and the Promise Neighborhoods. </p>


            </div>
        </div>
    </div>

    <div class="expertise_stats_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Our analysis and visualization skills turn dry data into engaging information.</p>
    </div>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ContactSEI.aspx.cs" Inherits="ContactSEI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        function checkWordLen(obj, wordLen, cid) {
            var len = obj.value.split(/[\s]+/);
            $("#" + cid).val(len.length.toString());
            if (len.length > wordLen) {
                alert("You've exceeded the " + wordLen + " word limit for this field!");
                obj.value = obj.SavedValue;
                len = obj.value.split(/[\s]+/);
                $("#" + cid).val(len.length.toString());
                return false;
            } else {
                obj.SavedValue = obj.value;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container clearfix landing_intro">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="grid_12">
            <div class="basic_content">

                <h1>Contact SEI</h1>
                <p>
                    <asp:Label ID="lblName" runat="server" Text="Your First Name" ></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="name" ControlToValidate="txtName" runat="server" ErrorMessage="First Name is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="lblLastName" runat="server" Text="Your Last Name"></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLastName" ControlToValidate="txtLastName" runat="server" ErrorMessage="Last Name is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="lblEmail" runat="server" Text="Your Email" ></asp:Label><span style="color: red">*</span>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="Email address is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                     <asp:RegularExpressionValidator ID="revEmail" runat="server"
                    ControlToValidate="txtEmail" ErrorMessage="Email address is not valid"
                    ValidationExpression="[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}" ForeColor="Red" ValidationGroup="Save">
                </asp:RegularExpressionValidator>
                   
                </p>
                <p>
                    <asp:Label ID="lblCompantName" runat="server" Text="Your Company Name"></asp:Label>
                    <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="100"></asp:TextBox>
                </p>

                <p>
                    <asp:Label ID="lblyourMessage" runat="server" Text="Your Message "></asp:Label> (<i>Maxminum 275 words are allowed </i>)<span style="color: red">*</span><br />

                    <asp:TextBox ID="txtMessage" runat="server" Width="600px" Height="150px" TextMode="MultiLine" 
                         SavedValue="" MaximumLength="275" onkeyup="checkWordLen(this, 275, 'stmFidelityGoalcnt');"  ></asp:TextBox>
                     <input type="text" disabled="disabled" size="2" id="stmFidelityGoalcnt" />  
                   
                    <asp:RequiredFieldValidator ID="rfvMessage" ControlToValidate="txtMessage" runat="server" ErrorMessage="Message is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regmess" runat="server" ControlToValidate="txtMessage"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,1999}$" ErrorMessage="Maximum 2000 characters or 275 words are allowed"
                        ForeColor="Red">Maximum 2000 characters or 275 words are allowed
                    </asp:RegularExpressionValidator>
                </p>

                  <p >
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                            <asp:Image ID="imgCaptcha" runat="server" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </p>
                <p>
                    <asp:Label ID="lblCaptcha" runat="server" Text="Enter the characters shown in the image"></asp:Label><span style="color: red">*</span><br />
                    <asp:TextBox ID="txtCaptcha" MaxLength="15" runat="server" Width="200px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="reqCaptcha" ControlToValidate="txtCaptcha" runat="server" ErrorMessage="Characters shown in the image is required"
                        ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
                </p>


              

                <div>
                    <p>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Save" OnClick="btnSubmit_Click" CssClass="btn btn-primary" />
                    </p>
                    <p>
                        <asp:Label ID="conMessage" runat="server" ForeColor="Red"></asp:Label>
                    </p>

                </div>

                <asp:ValidationSummary ID="valsumm" ValidationGroup="Save" runat="server" DisplayMode="BulletList" ForeColor="Red" />

            </div>
        </div>
     </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.IO;


public partial class ContactSEI : System.Web.UI.Page
{
    int lngPkID;
    string message;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {           
            FillCapctha();
        }
        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
          
        }
        if (Page.Request.QueryString["Message"] != null)
        {
            message = Page.Request.QueryString["Message"];
            conMessage.Text = message;
        }

        if (ViewState["IsLoaded1"] == null)
        {
            //loadDropdown();
            //displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void FillCapctha()
    {
        try
        {
            Random random = new Random();

            string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            StringBuilder captcha = new StringBuilder();

            for (int i = 0; i < 6; i++)

                captcha.Append(combination[random.Next(combination.Length)]);

            Session["captcha"] = captcha.ToString();

            imgCaptcha.ImageUrl = "GenerateCaptcha.aspx?" + DateTime.Now.Ticks.ToString();
        }

        catch
        {
            throw;
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Session["captcha"].ToString() != txtCaptcha.Text)
        {

            conMessage.Text = "Invalid Captcha Code";
        }
        else
        {
            updateUser();
            conMessage.Text = "Your Information has been submitted successfully!";
            sEmail();
            Response.Redirect("ContactSEI.aspx?Message=" + conMessage.Text);
        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["Message"] != null)))
        {
            message = Convert.ToString(this.ViewState["Message"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["Message"] = lngPkID;
        return (base.SaveViewState());
    }

    private void sEmail()
    {
        try
        {
            string strFrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
            string strTo = "Vkothale@seiservices.com";
            MailMessage objMailMsg = new MailMessage(strFrom, strTo);
            objMailMsg.CC.Add(new MailAddress("FLacerda@seiservices.com"));

            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.Subject = "Confirmation: Information has been submitted successfully!";

            //-------Email body-----------------------

            objMailMsg.Body = "Your Information has been submitted successfully!";
            //-------------------
         
            objMailMsg.Priority = MailPriority.Low;
            objMailMsg.IsBodyHtml = true;

            //--prepare to send mail via SMTP transport
            SmtpClient objSMTPClient = new SmtpClient();

            objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
            NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            objSMTPClient.Send(objMailMsg);

        }
        catch (System.Exception ex)
        {
            //ILog Log = LogManager.GetLogger("EventLog");
            //Log.Fatal("Send report email.", ex);
        }
    }
    private void updateUser()
    {
        bool blNew = false;
       
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            P_ContactSei oCase = (from c in db.P_ContactSeis where c.SEIcontactID == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new P_ContactSei();
                blNew = true;
            }
            oCase.FirstName = txtName.Text;
            oCase.LastName = txtLastName.Text;
            oCase.Email = txtEmail.Text;
            oCase.CompanyName = txtCompanyName.Text;
            oCase.Message = txtMessage.Text;         
            oCase.CreatedDate = DateTime.Now;
            oCase.CreatedBy = txtEmail.Text; 

            if (blNew == true)
            {
                db.P_ContactSeis.InsertOnSubmit(oCase);
            }
            db.SubmitChanges();
            lngPkID = oCase.SEIcontactID;
        }

        conMessage.Text = "Your Information has been submitted successfully!";
    }
  
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        FillCapctha();
    }
}
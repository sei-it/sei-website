﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class MobileImageServer : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? CaseStudiesID;
    int? CaseStudiesMobileImagesID;
    string imageName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CaseStudiesID"] != null)
            {
                CaseStudiesID = Convert.ToInt32(Page.Request.QueryString["CaseStudiesID"]);
            }
            if (Page.Request.QueryString["CaseStudiesMobileImagesID"] != null)
            {
                CaseStudiesMobileImagesID = Convert.ToInt32(Page.Request.QueryString["CaseStudiesMobileImagesID"]);
            }
        }
        GetMobileImageName(CaseStudiesID);
        GetMobileImageName2(CaseStudiesMobileImagesID);   
    }

    private void GetMobileImageName2(int? CaseStudiesMobileImagesID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.vw_CaseStudyAndMobileImages
                           where p.CaseStudiesMobileImagesID == CaseStudiesMobileImagesID
                            select p;
            foreach (var oCase in vCourses)
            {
                imageName = oCase.PhysicalFileName;
                Response.ContentType = "image/jpeg"; // for JPEG file
                string physicalFileName = ConfigurationManager.AppSettings["MobileImagePath"].ToString();

                physicalFileName = physicalFileName + imageName;
                Response.WriteFile(physicalFileName);
            }
        }
    }

    private void GetMobileImageName(int? CaseStudiesID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = (from p in db.vw_CaseStudyAndMobileImages
                           where p.CaseStudiesID == CaseStudiesID
                           select p).Take(1);
            foreach (var oCase in vCourses)
            {
                imageName = oCase.PhysicalFileName;
                Response.ContentType = "image/jpeg"; // for JPEG file
                string physicalFileName = ConfigurationManager.AppSettings["MobileImagePath"].ToString();

                physicalFileName = physicalFileName + imageName;
                Response.WriteFile(physicalFileName);
            }
        }
    }


    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["CaseStudiesID"] != null)))
        {
            CaseStudiesID = Convert.ToInt32(this.ViewState["CaseStudiesID"]);
        }


    }
    protected override object SaveViewState()
    {
        this.ViewState["CaseStudiesID"] = CaseStudiesID;

        return (base.SaveViewState());
    }


}
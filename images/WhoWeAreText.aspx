﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="WhoWeAre.aspx.cs" Inherits="WhoWeAre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
 
<div id="interior_landing" class="whoweare_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
	<div class="container">
		<div class="row">
        	<div class="col-md-12">
				<div id="interior_landing_text">
					<h1>who<br /><span class="land_purpleh1">we are</span></h1>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="container">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
        	<div class="basic_content">
                 <p>Synergy is a team of dedicated individuals who bring subject matter expertise, customized technology tools, and award-winning communications services to projects that help connect and strengthen people, programs, communities, and countries.</p>
            </div>
        </div>
    </div>
</div>

<div class="container landing_icons">
	<div class="row">
    	<div class="col-md-4">
        	<div class="landing_card">
            	<a href="TheCompany.aspx"><img src="images/company_icon.png" class="landicon" /></a>
                <h4><a href="TheCompany.aspx">The Company</a></h4>
            </div>
        </div>
        <div class="col-md-4">
        	<div class="landing_card">
            	<a href="ContractVehicles.aspx"><img src="images/contract_icon.png" class="landicon" /></a>
                <h4><a href="ContractVehicles.aspx">Contract Vehicles &amp; Certifications</a></h4>
            </div>
        </div>
        <div class="col-md-4">
        	<div class="landing_card">
            	<a href="Leadership.aspx"><img src="images/leadership_icon.png" class="landicon" /></a>
                <h4><a href="Leadership.aspx">Leadership</a></h4>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-4 col-md-offset-2">
        	<div class="landing_card">
            	<a href="AwardsRecognition.aspx"><img src="images/awards.png" class="landicon" /></a>
                <h4><a href="AwardsRecognition.aspx">Awards &amp; Recognition</a></h4>
            </div>
        </div>
        <div class="col-md-4">
        	<div class="landing_card">
            	<a href="ClientsPartners.aspx"><img src="images/partners_icon.png" class="landicon" /></a>
                <h4><a href="ClientsPartners.aspx">Clients &amp; Partners</a></h4>
            </div>
        </div>
    </div>
</div>

</asp:Content>


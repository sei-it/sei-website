﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProjectPortfolio.aspx.cs" Inherits="ProjectPortfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="interior_landing_content">
        <div class="container clearfix landing_intro">
            <div class="grid_12">

                <h1>Project Portfolio</h1>
                <p>
                    Suspendisse eleifend nulla vitae ante pellentesque, quis ornare sapien vestibulum. Maecenas eleifend
                            augue ipsum, et pellentesque elit consectetur at. Suspendisse efficitur porta pellentesque. Proin quis
                            gravida elit, at pulvinar turpis. Praesent sed posuere nulla, et porta odio.
                </p>
                <p>
                    Filter By: 
                    <asp:DropDownList ID="ddlExpertise" runat="server">
                        <asp:ListItem>Expertise</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlAgency" runat="server">
                        <asp:ListItem>Agency/Office</asp:ListItem>
                    </asp:DropDownList>
                </p>
                <p>
                    <asp:Label ID="lblxyz" runat="server"></asp:Label>
                    Projects with
                    <asp:Label ID="lblAgency" runat="server"></asp:Label>
                </p>
            </div>
        </div>

        <div class="container clearfix cardgrid">
           <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                <ItemTemplate>
                    <div class="grid_3">
                        <b><%# Eval("ProjectPortTitle") %></b><br>

                        <!-- start child repeater -->
                        <asp:Repeater ID="Repeater2" runat="server">
                            <ItemTemplate>
                                <ul>
                                    <li>
                                        <%# Eval("AgencyName")%>
                                    </li>
                                </ul>
                            </ItemTemplate>
                        </asp:Repeater>
                        <!-- end child repeater -->
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <!-- end parent repeater -->
        </div>
    </div>
</asp:Content>


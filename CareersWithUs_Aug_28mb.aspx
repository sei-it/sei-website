﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CareersWithUs.aspx.cs" Inherits="CareersWithUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>

       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
  </script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
    
    
        <div class="row">
            <div class="col-md-5">
            
            
                <h1 class="bigh1">Careers with us</h1>
            </div>
            
                        
            <div class="col-md-6">
            

                 <p> Synergy staff members are advancing solutions in public health and public education systems across the country. 
                If you want to do work that matters, explore the information below to find out more about employee recognition, generous benefits, 
                and current openings at Synergy. </p>
            

             <!-- Start Collapse-->
</div>
</div>
<div class="row">
          
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Synergy Offers:</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                    <li>A dynamic, international, and diverse workforce</li>
                    <li>A reputation for integrity and excellence</li>
                    <li>A professional and supportive environment</li>
                    <li>A comfortable office environment with easy access to transit and services</li>
                </ul>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Benefits at Synergy:</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>(for all full-time and certain part-time employees)</p>
                <ul>
                    <li>Health, Dental, and Vision plans available</li>
                    <li>401(k) with generous company match</li>
                    <li>Employee Stock Option Plan (vests with years of service)</li>
                    <li>Short-Term Disability, Long-Term Disability, and Group Term Life at no employee cost</li>
                </ul>
                
                <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>
                        
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Awards at Synergy: </a>
                                </h3>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>Synergy strives to achieve excellence for our clients, and we encourage and reward achievement and innovation in our staff. Our awards include:</p>
              <ul>
                  <li>Employee of the Quarter</li>
                  <li>Employee of the Year</li>
                  <li>Rookie of the Year</li>
                  <li>The Glen Fischer Achievement Award – given annually to recognize an employee’s commitment, dedication, and enthusiasm towards Synergy and the community at large. </li>
              </ul>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Current Openings:</a>
                                </h3>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <p>Synergy is currently looking for dynamic candidates with a passion for their field and a drive to meet the needs of our clients. If we have an opening that interests you, please apply by clicking one of the links below. We will need to see your resume, and a cover letter that includes your qualifications, salary requirements, and contact information. Reach us at <a href="mailto:jobs@SEIservices.com" target="_blank">jobs@SEIservices.com</a> or apply by <a href="CareersSubmitResume.aspx">clicking here</a>.</p>
                                    
                                                    <div>
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                        <ItemTemplate> 
                                                      
                           <%-- <asp:LinkButton ID="imgBtnEdit" CssClass="btn_careers" runat="server" CommandName="CareersDetails" Visible="true" CommandArgument='<%# Eval("CareersID") %>'>
                                    <%# Eval("JobTitle") %></asp:LinkButton><br />  --%>  

                            <ul>
                                <li><a class="btn_careers" href="CareersCurrentOpeningDetails.aspx?CareersId=<%# Eval("CareersID")%>"><%# Eval("JobTitle") %></a></li>                     
                            </ul>
                        </ItemTemplate>
                    </asp:Repeater>

                 </div> 
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a> 
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
         
        </div>
<!--End Collapse -->
            <!--  <h3  class="expand"  onclick=" ExpandCollapse('list1');">Synergy offers: </h3>
              <div id="list1" style="display:none"> 
                <ul>
                    <li>A dynamic, international, and diverse workforce</li>
                    <li>A reputation for integrity and excellence</li>
                    <li>A professional and supportive environment</li>
                    <li>A comfortable office environment with easy access to transit and services</li>
                </ul>
              </div>
               
              <h3  class="expand"  onclick=" ExpandCollapse('list2');">Benefits at Synergy: </h3>
              <div id="list2" style="display:none">   
                <p>(for all full-time and certain part-time employees)</p>
                <ul>
                    <li>Health, Dental, and Vision plans available</li>
                    <li>401(k) with generous company match</li>
                    <li>Employee Stock Option Plan (vests with years of service)</li>
                    <li>Short-Term Disability, Long-Term Disability, and Group Term Life at no employee cost</li>
                </ul>
             </div>
                 
              <h3  class="expand"  onclick=" ExpandCollapse('list3');">Awards at Synergy: </h3>
              <div id="list3" style="display:none">   
               <p>Synergy strives to achieve excellence for our clients, and we encourage and reward achievement and innovation in our staff. Our awards include:</p>
              <ul>
                  <li>Employee of the Quarter</li>
                  <li>Employee of the Year</li>
                  <li>Rookie of the Year</li>
                  <li>The Glen Fischer Achievement Award – given annually to recognize an employee’s commitment, dedication, and enthusiasm towards Synergy and the community at large. </li>
              </ul>
             </div> 

            <h3  class="expand"  onclick=" ExpandCollapse('list4');">Current Openings: </h3>
              <div id="list4" style="display:none">   
                <p>Synergy is currently looking for dynamic candidates with a passion for their field and a drive to meet the needs of our clients. If we have an opening that interests you, please apply by clicking one of the links below. We will need to see your resume, and a cover letter that includes your qualifications, salary requirements, and contact information. Reach us at <a href="mailto:jobs@SEIservices.com" target="_blank">jobs@SEIservices.com</a> or apply by <a href="CareersSubmitResume.aspx">clicking here</a>.</p> 
                
                -->

              </div>

            </div>
        </div>
    </div>

    <div class="careers_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy strives for excellence in our work for clients, fostered by a supportive, professional work environment. </p>
    </div>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;

public partial class SiteMaster : MasterPage
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int iCaseCount = 0;
    int iTotalCaseCount = 0;
    int iTotalCaseCountTmp = 0;
    int sHighlightsID;
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        loadrecords(); 
    }

    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCourses = from p in db.vw_HighlightsAndImages
                           where (p.DisplayOrderID_FK < 3) && (p.ActiveStatus == true)       
                           orderby p.DisplayOrderID_FK 
                           select p;
           
            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();

            //Repeater2.DataSource = vCourses;
            //Repeater2.DataBind();
        }
    }

    public static string FormatText(string testString)
    {
        testString = testString.Replace("<p>", "");
        return testString.Replace("</p>", "");
    }
    private DataTable ProductData(int sHighlightsID)
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT  HighlightsTitle, HighlightsShortDescription, HighlightsLongDescription FROM Highlights WHERE HighlightsID=" + sHighlightsID;

        SqlCommand cmd = new SqlCommand(sql, con);

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        con.Close();

        return dt;
    }
   

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "HighlightsDetails")
        {

            int sHighlightsID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("HighlightsDetails.aspx?HighlightsID=" + sHighlightsID);


        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ArchivedHighlights.aspx.cs" Inherits="ArchivedHighlights" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">


     <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-4">
                <h1 class="bigh1">Archived Highlights</h1>
            </div>
            <div class="col-md-7">
               
                <%-- <h1>SEI Highlights</h1>--%>
                   <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                      <ItemTemplate>
                          <ul> 
                            <li><h4> <a  href="HighlightsDetails.aspx?HighlightsID=<%# Eval("HighlightsID")%>"><%# Eval("HighlightsTitle") %></a><br /> </h4></li>
                          </ul> 
                      </ItemTemplate>
                 </asp:Repeater>
                
           </div>
        </div>
    </div>



</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="jQueryReady" Runat="Server">

</asp:Content>


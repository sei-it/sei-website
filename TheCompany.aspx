﻿<%@ Page Title="The Company. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TheCompany.aspx.cs" Inherits="TheCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-4">
                <h1 class="bigh1">The Company</h1>
            </div>
            <div class="col-md-8">
                <%--<h3>A SMALL BUSINESS THAT BRINGS BIG SKILLS AND DEEP COMMITMENT TO PUBLIC HEALTH, PUBLIC EDUCATION, & OTHER CRITICAL SOCIAL INITIATIVES.</h3>--%>
                <p>Synergy was founded in 2003. Only 7 years later, President Barack Obama invited its President and CEO, Prachee J. Devadas, to a meeting in the Oval Office,
                   where he singled out Synergy as representing the promise of opportunity in America. In a short time, we have grown from a staff of two working at a kitchen table
                   to a small business with more than 100 full-time employees.</p>
                
                <p>As a group, our staff represents a powerful blend of subject matter expertise and experience in areas such as:  </p>
                   <ul>
                   	<li><a href="ProgramEvaluationAndImplementation.aspx">Research and Evaluation and Technical Assistance</a></li>
                    <li><a href="MultimediaCommunication.aspx">Multimedia Communication</a></li>
                    <li><a href="InformationTechnologyIntegration.aspx">Information Technology Integration</a></li>
                    <li><a href="WebDesignApplicationDevelopmentandE-Learning.aspx">Web Design, Application Development, and E-Learning</a></li>
                    <li><a href="ManagementConsultingAndLogistics.aspx">Management Consulting and Logistics</a></li>
                   </ul>       

                <p>As individuals, we are deeply committed to supporting organizations—in our neighborhoods, our nation, and across the world—that help people in need.
                   Our staff members assist homeless families throughout the Washington metropolitan area, support innovations in substance abuse research, work with 
                   paralyzed veterans, and fund education in the Mumbai slums. </p>

                <p><b>Get to Know Synergy</b></p>

                   <p>As you explore this site, you will find that Synergy’s approach to public health and public education combines nimble, small business practices with powerful,
                      big business skills and expertise. Whether you are interested in joining our team, partnering with us, or contracting in specialized business areas, 
                      we invite you to learn more about our dedicated staff and outstanding professional performance.</p>
                 </div>
        </div>
    </div>

    <div class="company_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
       <p>Synergy’s staff members speak 26 languages and represent 27 countries. Our diverse backgrounds bring important perspectives to the services we deliver.</p>
          
     
    </div>

</asp:Content>


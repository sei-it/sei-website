﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseStudies.aspx.cs" Inherits="CaseStudies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div class="container landing_padding">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
        	<div class="basic_content">
                <h1>Case Studies</h1>
                <p>Suspendisse eleifend nulla vitae ante pellentesque, quis ornare sapien vestibulum. Maecenas eleifend augue ipsum, et pellentesque elit consectetur at. Suspendisse efficitur porta pellentesque. Proin quis gravida elit, at pulvinar turpis. Praesent sed posuere nulla, et porta odio.</p>
                <div class="sort_contain">
                    <div class="sort">
                        <h3>Filter By:</h3>
                    </div>
                    <div class="sort_box col-md-4">
                            <asp:DropDownList ID="ddlExpertise" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExpertise_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Expertise</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div class="sort_box col-md-4">
                            <asp:DropDownList ID="ddlAgency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Agency/Office</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
		</div>
	</div>
</div>
    
<div class="container sort_row">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
            <div class="sort_contain">
                <h2><asp:Label ID="lblxyz" runat="server"></asp:Label> projects with <asp:Label ID="lblAgency" runat="server"></asp:Label></h2>     
			</div>
		</div>
	</div>
</div>

 
    
    <div class="container">
    	<div class="row csrow">
        <asp:HiddenField ID="rval" runat="server" />
        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand" >
            <ItemTemplate>
        	<div class="col-md-4">
            	<div class="casestudy_card">
            	<%--<a href='<%= string.Format("C_CaseStudiesDetails.aspx?CaseStudiesID={0}", rval.Value) %>'>--%>
                         <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="CaseStudyDetails" visible="true"   CommandArgument='<%# Eval("CaseStudiesID") %>'>
                        <h4><%# Eval("CaseStudyTitle") %></h4></asp:LinkButton>
                        <ul>
                        <!-- start child repeater -->
                        <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>
                        	<li>
                            	<%# Eval("AgencyName")%>
                            </li>                             
                        </ItemTemplate>
                        </asp:Repeater>
                        <!-- end child repeater -->
                        </ul>
            	</div>
            </div>
            </ItemTemplate>
        </asp:Repeater>
        </div>
    </div>

    <%-- <div class="interior_landing_content">
        

        <div class="container clearfix cardgrid">

            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                <ItemTemplate>
                    <div class="grid_3">
                        <h4><%# Eval("CaseStudyTitle") %></h4>
                        <!-- start child repeater -->
                        <asp:Repeater ID="Repeater2" runat="server">
                            <ItemTemplate>
                                <ul>
                                    <li>
                                        <%# Eval("AgencyName")%>
                                    </li>
                                </ul>
                            </ItemTemplate>
                        </asp:Repeater>
                        <!-- end child repeater -->
                    </div>
                </ItemTemplate>

            </asp:Repeater>

        </div>


    </div>--%>
</asp:Content>


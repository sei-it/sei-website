﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;


public partial class CaseStudies : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["SEI_Site2015ConnectionString"].ToString();
    int? ProjectId;
    string sText;
    string MyVaraible;
    string expertise;
    string agency;
    string strService;
    Object a;
    int iCaseCount = 0;
    int iTotalCaseCount = 0;
    int iTotalCaseCountTmp = 0;
    string strNameCond;
    int tCount = 0;
    String strItem;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Page.Request.QueryString["CourseId"] != null)
            {
                ProjectId = Convert.ToInt32(Page.Request.QueryString["CourseId"]);
            }


            if (Page.Request.QueryString["strService"] != null)
            {
                expertise = Convert.ToString(Page.Request.QueryString["strService"]);
                lblxyz.Text = strService;
            }
            else
            {
                strService = "";
                lblxyz.Text = strService;
            }

            if (Page.Request.QueryString["agency"] != null)
            {
                agency = Convert.ToString(Page.Request.QueryString["agency"]);
                lblAgency.Text = agency;
            }
            else
            {
                agency = "";
                lblAgency.Text = agency;
            }

            //Display filter default text



            if (Page.Request.QueryString["expertise"] == null && Page.Request.QueryString["agency"] == null)
            {
                sText = "All Projects";
                //lbltext.Text = sText;
            }


            loadrecords();

            //Repeater1.DataSource = CaseStudyProjectData(strService, agency);
            //Repeater1.DataBind();
            LoadCaseStudies();

        }
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void LoadCaseStudies()
    {
        if (string.IsNullOrEmpty(strService) && string.IsNullOrEmpty(agency))
        {
            Repeater1.DataSource = CaseStudyProjectData1();
            Repeater1.DataBind();
        }
        else if (string.IsNullOrEmpty(strService) && (!string.IsNullOrEmpty(agency)))
        {
            Repeater1.DataSource = CaseStudyProjectDataOnlyAgency(agency);
            Repeater1.DataBind();
        }
        else
        {            
            Repeater1.DataSource = CaseStudyProjectData(strService, agency);
            Repeater1.DataBind();
           
        }
    }

    private object CaseStudyProjectDataOnlyAgency(string agency)
    {
        SqlConnection con = new SqlConnection(connString);
        con.Open();
        //string sql = "SELECT distinct CaseStudiesID, CaseStudyTitle FROM vw_CStudy_try where ActiveStatus=1 and Expertise like '%' + @Expertise + '%' and AgencyName like '%' + @AgencyName + '%'";
        string sql = "SELECT distinct CaseStudiesID, CaseStudyTitle FROM View_CS_CaseStudyServiceArea where  AgencyName like '%'+@AgencyName+'%'";
        SqlCommand cmd = new SqlCommand(sql, con);
       // cmd.Parameters.Add("@Service", strService);
        cmd.Parameters.Add("@AgencyName", agency);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            a = dt.Rows[0]["CaseStudiesID"];
        }
        else
        {
            a = 0;
        }
        rval.Value = Convert.ToString(a);
        con.Close();
        iTotalCaseCount = dt.Rows.Count;
        return dt;
    }
    private object CaseStudyProjectData1()
    {
        SqlConnection con = new SqlConnection(connString);
        con.Open();
       // string sql = "SELECT distinct SubMatterID, SubMatterExTitle, SubMattDescription,SubMatterExLastName FROM vw_SME_try where ActiveStatus=1  order by SubMatterExLastName";
        string sql = "SELECT distinct CaseStudiesID, CaseStudyTitle FROM View_CS_CaseStudyServiceArea order by CaseStudyTitle";
     
        SqlCommand cmd = new SqlCommand(sql, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        iTotalCaseCount = dt.Rows.Count;
        return dt;
    }
    private object CaseStudyProjectData(string strService, string agency)
    {
       
        SqlConnection con = new SqlConnection(connString);
        con.Open();       

      //  SqlCommand cmd = new SqlCommand("get_CaseStudySearch_SA", con); 
        SqlCommand cmd = new SqlCommand("get_CaseStudySearch_SA2", con); 
        cmd.Parameters.Add("@Service", strService);
        cmd.Parameters.Add("@AgencyName", agency);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);   

        if (dt.Rows.Count > 0)
        {
            a = dt.Rows[0]["CaseStudiesID"];
        }
        else
        {
            a = 0;
        }
        rval.Value = Convert.ToString(a);
        con.Close();
        iTotalCaseCount = dt.Rows.Count;
        return dt;              

    }

    private void loadProjectList()
    {

        object objVal = null;

        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            // Project oP = (from c in db.Projects where c.ProjectID == ProjectId select c).FirstOrDefault();
            var vCourses = from p in db.Projects
                           where p.ProjectID <= 4
                           select p;
            Repeater1.DataSource = vCourses;
            Repeater1.DataBind();
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["ProjectId"] != null)))
        {
            ProjectId = Convert.ToInt32(this.ViewState["CourseId"]);
        }
        if (((this.ViewState["agency"] != null)))
        {
            agency = Convert.ToString(this.ViewState["agency"]);
        }

        if (((this.ViewState["strService"] != null)))
        {
            expertise = Convert.ToString(this.ViewState["strService"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["agency"] = agency;
        this.ViewState["strService"] = strService;
        this.ViewState["ProjectId"] = ProjectId;
        return (base.SaveViewState());
    }

    private void loadrecords()
    {


        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry2 = from pg in db.ServiceAreas
                       select pg.Service;
            lbServiceArea.DataSource = qry2;
            lbServiceArea.DataBind();
          
            
            //var qry1 = from pg in db.Agencies
            //           where pg.AgencyName != "Other"
            //           orderby pg.AgencyName 
            //           select pg;
            //ddlAgency.DataSource = qry1;
            //ddlAgency.DataTextField = "AgencyName";
            //ddlAgency.DataValueField = "AgencyID";
            //ddlAgency.DataBind();
            //ddlAgency.Items.Insert(0, "Select Agency");
            //ddlAgency.Items.Insert(ddlAgency.Items.Count, new ListItem("Other")); 

            var AgencyID = new int[] { 1, 4, 7, 13, 14, 17, 19 };
            var qry1 = from pg in db.Agencies
                       where AgencyID.Contains(pg.AgencyID)
                       orderby pg.AgencyName
                       select pg;
            ddlAgency.DataSource = qry1;
            ddlAgency.DataTextField = "AgencyName";
            ddlAgency.DataValueField = "AgencyID";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, "Select Agency");
            //  ddlAgency.Items.Insert(ddlAgency.Items.Count, new ListItem("Other")); 
           
           
        }
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //DataRowView drv = (DataRowView)e.Item.DataItem;
            //int caseStudiesID = Convert.ToInt32(drv["CaseStudiesID"]);
            //Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater2");
            //Repeater2.DataSource = CaseStudyProjectData(caseStudiesID);
            //Repeater2.DataBind();

            iCaseCount = iCaseCount + 1;
            iTotalCaseCountTmp = iTotalCaseCountTmp + 1;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                Literal litHeader = (Literal)e.Item.FindControl("litHeader");
                Literal litFooter = (Literal)e.Item.FindControl("litFooter");

                if (iCaseCount == 1)
                {

                    litHeader.Text = @"    	<div class=""row"">";
                    litFooter.Text = @"";
                }

                if (iCaseCount == 3)
                {

                    iCaseCount = 0;
                    litHeader.Text = @"  ";
                    litFooter.Text = @"    	    </div>";
                }

                if (iTotalCaseCount == iTotalCaseCountTmp)
                {
                    litHeader.Text = @"    	<div class=""row"">";
                    litFooter.Text = @"";
                }


            }
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int caseStudiesID = Convert.ToInt32(drv["CaseStudiesID"]);
            //Repeater Repeater2 = (Repeater)e.Item.FindControl("Repeater2");
            //Repeater2.DataSource = CaseStudyProjectData(caseStudiesID);
            //Repeater2.DataBind();


        }

    }
    private object CaseStudyProjectData(int caseStudiesID)
    {
        SqlConnection con = new SqlConnection(connString);

        con.Open();

        string sql = "SELECT AgencyName FROM vw_CaseStudyProjectAgency WHERE CaseStudiesID=" + caseStudiesID;

        SqlCommand cmd = new SqlCommand(sql, con);

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        con.Close();

        return dt;
    }


    //Get the officeID of the C_CaseStudies record using the given CaseStudiesID 
    //OfficeID = 3  is 'Other'  currently only used by NASMHPD
    //if the office is 'Other' then do not show the office on CaseStudiesDetails page.
    private int GetCaseStudiesOfficeByID(int caseStudiesID)
    {
        SqlConnection con = new SqlConnection(connString);

        int officeID = 0;
        string sqlString = "SELECT Office FROM C_CaseStudies where CaseStudiesID = @ID";
                         
        SqlConnection Con = new SqlConnection(connString);
        SqlDataReader rdr = null;
        try
        {
            Con.Open();
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.CommandText = sqlString;
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = caseStudiesID;
            Cmd.CommandType = CommandType.Text;

            rdr = Cmd.ExecuteReader();

            while (rdr.Read())
                officeID = int.Parse(rdr[0].ToString());

            return officeID;
        }
        catch (Exception ex)
        {
          
            return 0;
        }
        finally
        {
            if (!rdr.IsClosed)
                rdr.Close();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }

    }


    //If the selected Case Study has officeID = 3 (Other), then pass 'Other' to CaseStudiesDetail and
    //do not show the office on CaseStudiesDetails page.
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        int officeID = 0; 
                        
        if (e.CommandName == "CaseStudyDetails")
        {

            int caseStudiesID = Convert.ToInt32(e.CommandArgument);

            officeID = GetCaseStudiesOfficeByID(caseStudiesID);

            if (officeID == 3)
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID + "&Other=" + "Other");
            else
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID);


        }

        if (e.CommandName == "CaseStudyLogo")
        {

            int caseStudiesID = Convert.ToInt32(e.CommandArgument);

            officeID = GetCaseStudiesOfficeByID(caseStudiesID);

            if (officeID == 3)
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID + "&Other=" + "Other");
            else
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID);


        }

        if (e.CommandName == "buy")
        {

            int caseStudiesID = Convert.ToInt32(e.CommandArgument);

            officeID = GetCaseStudiesOfficeByID(caseStudiesID);

            if (officeID == 3)
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID + "&Other=" + "Other");
            else
                Response.Redirect("CaseStudiesDetail.aspx?CaseStudiesID=" + caseStudiesID);


        }
    }

    //protected void ddlExpertise_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DisplayFilterText();
    //    Repeater1.DataSource = CaseStudyProjectData(expertise, agency);
    //    Repeater1.DataBind();
    //}

    protected void ddlServiceArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayFilterText();
        Repeater1.DataSource = CaseStudyProjectData(strService, agency);
        Repeater1.DataBind();
    }
    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayFilterText();
        //Repeater1.DataSource = CaseStudyProjectData(strService, agency);
        //Repeater1.DataBind();
        LoadCaseStudies();
    }

#region old code
    //private void DisplayFilterText()
    //{

    //    foreach (int index in lbServiceArea.GetSelectedIndices())
    //    {
    //        tCount = tCount + 1;
    //        if (strItem == null)
    //        {
    //            strItem = lbServiceArea.Items[index].Value;
    //        }
    //        else
    //        {
    //            strItem = (strItem + "," + lbServiceArea.Items[index].Value);
    //        }
    //    }

    //    if (!string.IsNullOrEmpty(strItem))
    //    {
    //        lbllist.Text = strItem.ToString();
    //    }
    //    else
    //    {
    //        lbllist.Text = "";
    //    }
    //    if ((lbllist.Text == "Select Service Areas" || lbllist.Text == "") && ddlAgency.SelectedItem.Text == "Select Agency")
    //    {
    //        strService = "";
    //        //lblxyz.Text = expertise;

    //        // sText = "All Subject Matter Experts";
    //        //  lbltext.Text = sText;

    //        agency = "";
    //        //  lblAgency.Text = agency;
    //    }
    //    else if (lbllist.Text != "Select Service Areas" && ddlAgency.SelectedItem.Text != "Select Agency")
    //    {
    //        strService = lbllist.Text;
    //        // lblxyz.Text = expertise;

    //        //sText = "Subject Matter Experts with the";
    //        // lbltext.Text = sText;

    //        agency = ddlAgency.SelectedItem.Text;
    //        // lblAgency.Text = agency;
    //    }
    //    else if ((lbllist.Text == "Select Service Areas" || lbllist.Text == "") && ddlAgency.SelectedItem.Text != "Select Agency")
    //    {
    //        agency = ddlAgency.SelectedItem.Text;
    //        // lblxyz.Text = agency;

    //        //   sText = "Subject Matter Experts";
    //        //   lbltext.Text = sText;

    //        strService = "";
    //        //   lblAgency.Text = expertise;

    //    }
    //    else if (lbllist.Text != "Select Service Areas" && ddlAgency.SelectedItem.Text == "Select Agency")
    //    {
    //        strService = lbllist.Text;
    //        //lblxyz.Text = expertise;

    //        // sText = "Subject Matter Experts";
    //        // lbltext.Text = sText;

    //        agency = "";
    //        //  lblAgency.Text = agency;

    //    }
    //}

#endregion


         private void DisplayFilterText()
    {
        
        foreach (int index in lbServiceArea.GetSelectedIndices())
        {
            tCount = tCount + 1;
            if (strItem == null)
            {
                strItem = lbServiceArea.Items[index].Value;
            }
            else
            {
                strItem = (strItem + "," + lbServiceArea.Items[index].Value);
            }
        }

        if (!string.IsNullOrEmpty(strItem))
        {
            lbllist.Text = strItem.ToString();
        }
        else
        {
            lbllist.Text = "";
        }
        if ((lbllist.Text =="") && ddlAgency.SelectedItem.Text == "Select Agency")
        {
            strService = "";
            //lblxyz.Text = expertise;

            // sText = "All Subject Matter Experts";
            //  lbltext.Text = sText;

            agency = "";
            //  lblAgency.Text = agency;
        }
        else if (ddlAgency.SelectedItem.Text != "Select Agency")
        {
            strService = lbllist.Text;
            // lblxyz.Text = expertise;

            //sText = "Subject Matter Experts with the";
            // lbltext.Text = sText;

            agency = ddlAgency.SelectedItem.Text;
            // lblAgency.Text = agency;
        }
        else if ((lbllist.Text =="") && ddlAgency.SelectedItem.Text != "Select Agency")
        {
            agency = ddlAgency.SelectedItem.Text;
            // lblxyz.Text = agency;

            //   sText = "Subject Matter Experts";
            //   lbltext.Text = sText;

            strService = "";
            //   lblAgency.Text = expertise;

        }
        else if (ddlAgency.SelectedItem.Text == "Select Agency")
        {
            strService = lbllist.Text;
            //lblxyz.Text = expertise;

            // sText = "Subject Matter Experts";
            // lbltext.Text = sText;

            agency = "";
            //  lblAgency.Text = agency;

        }



    }
    protected void lbServiceArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayFilterText();
        //Repeater1.DataSource = CaseStudyProjectData(strService, agency);
        // Repeater1.DataBind();
        LoadCaseStudies();
    }
}
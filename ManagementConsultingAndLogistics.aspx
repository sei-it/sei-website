﻿<%@ Page Title="Management Consulting and Logistics. Synergy Enterprises Inc." Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ManagementConsultingAndLogistics.aspx.cs" Inherits="ExpertiseManagConsultingLogis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

<style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>

       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
  </script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div class="container landing_padding_double stick-wrapper">
        <div class="row"> 
            <div id="side-sticky" class="col-md-6" >
               <div class="theiaStickySidebar"> <h1 class="bigh1">Management Consulting and Logistics</h1>
                <div class="cs_button_holder">
                    <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success" />--%>

                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
              </div>  
            </div>
            <div class="col-md-6" id="main-content">
                <%-- <p>The U.S. Department of Education (ED) projects that U.S. public school enrollment will increase by more than 53 million students in the next decade. Synergy supports K-12 programs and helps identify interventions that ensure student success from kindergarten through the start of their careers. Our subject matter experience can inform your program development to help you exceed strategic performance goals. We create data visualizations using neighborhood-level geographic data to help inform ground-level, daily lesson plans. </p>
                <h3>Survey design and management</h3>
                <p>We provide program management support for Crime and Safety Surveys' work in the National Center for Education Statistics (NCES) Elementary/Secondary and Library Studies Division. To help NCES study trends in private school tuition, we are also designing a plan to analyze specific data collected between 1987–88 and 2007–08 on private school tuition models. </p>
                <h3>Technical assistance centers</h3>
                <p>We manage ED's Magnet Schools Assistance Program Technical Assistance Center, which provides 37 grantees in 153 schools across 15 states with guidance, strategies, and assistance in program implementation and management, including marketing and student recruitment, magnet theme integration, family and community engagement, and program and fiscal sustainability.</p>
                <h3>Custom reports and white papers</h3>
                <p>We write, edit, and perform quality assurance reviews on statistical briefs, annual reports, congressionally mandated publications, and white papers for various centers within ED and members of the Asia-Pacific Economic Cooperation that help the group make key decisions.</p>
                <h3>Event planning and management</h3>
                <p>Synergy staff provides global event planning support for the Asia-Pacific Economic Cooperation's international symposia and working-group meetings involving high-level representatives from various education ministries and departments.</p>--%>

                <p>Synergy’s management consulting specialties include meetings management, grants management, and peer review services. These service areas are built on proven best practices and industry-standard procedures that can be applied with flexibility and accuracy across projects. We work closely with clients throughout these processes to maximize efficiency and cost-effectiveness.   </p>
                
                
                     <!-- Start Collapse-->


           
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Meeting and Facilitation Services</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p> Synergy’s certified meeting managers, planners, and coordinators offer a range of support for conferences and meetings of all types (e.g., virtual, in-person, or multimedia), sizes (from 10-person focus groups to 4,000-person conventions), and duration (1-day to 2-week-long events). We have convened meetings at single and multiple locations locally, nationally, and globally. Synergy has managed pre-, onsite, and post-meeting support in China, Australia, Vietnam, Russia, Bali, Singapore, Hong Kong, and Chile.  </p>
                
                                    <p> Our staff routinely facilitates industry panels and policy academies for organizations such as the Convention Industry Council; the Society of Government Meeting Professionals, National Capital Chapter; the National Institute on Drug Abuse; and the Substance Abuse and Mental Health Services Administration.   </p>

                                    <p> Synergy collaborates with clients to produce a full range of materials, including program content, concise and cogent abstracts, quick-turnaround working notes for breakout sessions, and congressionally mandated reports and summaries.  </p>

                                    <p> At a minimum, we provide:  </p>
                                    <ul>
                                        <li>Secure online registration database design, maintenance, and reporting  </li>
                                        <li>Domestic and international travel support   </li>
                                        <li>Hotel selection and booking  </li>
                                        <li>Materials development  </li>
                                        <li>Webinars   </li>
                                        <li>Facilitation  </li>
                                        <li>Translation  </li>
                                        <li>Special needs accommodations  </li>
                                        <li>Pre-event and onsite support  </li>
                                        <li>Post-event support   </li>
                                        <li>Evaluation  </li>
                                        <li>Report preparation  </li>
                                       
                                    </ul>


                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Grant Management and Peer Review Services</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p> Synergy’s specialized staff supports grant competitions and reviews that range from 1-day teleconferences and electronic reviews for one panel to highly complex week-long meetings requiring multiple panels that review hundreds of grant applications. Our team of competition specialists, review coordinators, grant supervisors, screeners, and system architects support grant programs that allow clients to efficiently identify qualified reviewers, develop program announcements, solicit applications, track grants through the review process, and provide management throughout the life of the grant. Our innovative grant implementation and management services include:  </p>

                                    <ul>
                                        <li>Tailored databases and Web applications for reviewer and grantee registration</li>
                                        <li>Support for panel reviews </li>
                                        <li>Continuous tracking and management of applicant data</li>
                                        <li>Statistical tools to evaluate applicant strengths and weaknesses, which helps federal officials make funding decisions  </li>
                                        <li>Support for bidder conferences and reviewer trainings prior to grant reviews  </li>
                                        <li>Creation of grantee guides, brochures, and post-review evaluation instruments  </li>
                                        <li>Training and consultation to help grantees develop measurable project objectives, performance measures, and data collection activities for reporting </li>

                                    </ul>


                
                <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>
                        
                     
                    </div>
                </div>
          
        </div>
<!--End Collapse -->

 <!-- 	<h3  class="expand"  onclick=" ExpandCollapse('list1');">Meeting and Facilitation Services</h3>
           <div id="list1" style="display:none"> 
                <p>
                    Synergy’s certified and seasoned managers, planners, and coordinators offer a range of support for conferences and meetings of all types (virtual, in-person, or multi-media), sizes (10 person focus groups to 4,000 person mini-conventions), and durations (one-day to two-week-long events), convened at single or multiple locations locally, nationally, and globally. Synergy has managed pre-, on-site, and post-meeting support for meetings held in China, Australia, Vietnam, Russia, Bali, Singapore, Hong Kong, and Chile.
                </p>
                <p>
                    Our staff routinely facilitates industry panels and policy academies for organizations such as the Convention Industry Council; Society of Government Meeting Professionals, National Capital Chapter; and the Substance Abuse and Mental Health Services Administration.
                </p>
                <p>
                    Synergy collaborates with clients to produce a full range of materials, including program content, concise and cogent abstracts, quick-turnaround working notes for breakout sessions, and congressionally mandated reports and summaries. We provide the following conference and meeting services:
                </p>
                <ul>
                    <li>Secure online registration design, maintenance, and reporting </li>
                    <li>Domestic and international travel support </li>
                    <li>Webinars </li>
                    <li>Facilitation </li>
                    <li>Translation </li>
                    <li>Special needs accommodation </li>
                    <li>Pre-event and on-site support</li>
                    <li>Materials development </li>
                    <li>On-site support  </li>
                    <li>Post-event support </li>
                    <li>Evaluation and report preparation</li>
                </ul>
            </div>
			
		  <h3  class="expand"  onclick=" ExpandCollapse('list2');">Grant Management and Peer Review Services</h3>
             <div id="list2" style="display:none"> 
               
                <p>
                    Synergy’s competition specialists support grant competitions and reviews that range from simple 1-day teleconferences and electronic reviews for a single review panel to highly complex weeklong meetings requiring multiple panels and involving hundreds of grant applicants. Our team of competition specialists, review coordinators, grant supervisors, screeners, and system architects support grant programs that can allow you to more efficiently identify qualified reviewers, develop program announcements, solicit applications, track grants through review, and provide management through the life of the grant. Innovative grant implementation and management  includes:
                </p>

                <ul>
                    <li>Tailored databases and Web applications for reviewer and grantee registration</li>
                    <li>Panel reviews</li>
                    <li>Applicant data, with continuous tracking and management </li>
                    <li>Use of statistical tools to evaluate applicant program strengths and weaknesses that help federal program officials decide on funding </li>
                    <li>Support with bidder conferences and reviewer trainings before grant reviews  </li>
                    <li>Creation of post-review evaluation instruments, grantee guides, and brochures</li>
                    <li>Training and consultation to help grantees develop their own measurable project objectives, performance measures, and data collection activities for reports</li>
                </ul>

              </div> -->
            </div>
        </div>
    </div>


    <div class="expertise_manag_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy’s innovative grant review and logistics services help advance social initiatives worldwide.</p>
    </div>

</asp:Content>


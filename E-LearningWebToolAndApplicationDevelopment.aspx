﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="E-LearningWebToolAndApplicationDevelopment.aspx.cs" Inherits="ExpertiseELearning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="container landing_padding_double">
        <div class="row">
            <div class="col-md-6">
                <h1 class="bigh1">E-Learning, Web Tool, and Application Development</h1>
                <div class="cs_button_holder">
                <%--<asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary" />
                    <asp:Button ID="btnGetFactSheet" runat="server" Text="Get the Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success" />--%>
                     <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
            </div>
            <div class="col-md-6">
                <p>Synergy’s approach is simple:  The user experience should be the primary design consideration. Our approach is also comprehensive: Synergy’s web specialists provide complete design and development services—needs and requirements assessments, graphic design, developing wireframes, architecture design, user-interface development, and usability testing. </p>
                 <h3>Web Design and Development</h3>
                <p>Synergy designs and builds fully responsive 508 compliant sites and applications. We use the system development life cycle process to develop Web applications and offer high-availability hosting solutions in an operating environment with multiple continuously monitored high-speed application and data servers that meet exacting Authority to Operate specifications for various federal agencies, and provide 99 percent uptime for client sites.</p>
                <p>We develop reliable data-driven applications that draw on multiple data sets. We determine the best formats and mediums to design, store, and secure data and we develop the most effective way to integrate and visually interpret scientific and statistical information. Synergy’s IT professionals conduct gap analyses to determine how data need to be restructured and designed for optimal project performance. Our data analysts work with our web designers and programmers to develop engaging and effective ways to present complex information: 2D animations that explain school safety statistics; 3D animations that explain neuroscience and drug abuse; geographic information systems and mapping displays that use national data sources to describe complex socioeconomic, educational, and cultural interactions at a neighborhood level.   </p>
                
                <h3>Interactive e-learning programs</h3>
                <p>Our information technology, communication, and doctorate level instructional design experts use industry best practices to build state-of-the-art tools that teach key skills and concepts in a variety of subject areas. Our blended approach integrates deep subject matter expertise with innovative technology and effective instructional design. We start with learner analysis, needs analysis, objective and assessment development, and media selection. Our subject matter experts and plain language writers help develop storyboards for each instructional step. We use multiplatform and multiscreen design techniques when developing engaging, customized information architecture plans for self-paced and timed courses that often contain multiple modules. Our experts also ensure that download times are manageable, and that the privacy and security of module users are protected at all times. Our instructional developers work hand in hand with our internal user experience experts to provide the best possible look and for each e-learning project we deliver.</p>
                

            </div>
        </div>
    </div>

    <div class="expertise_elearning_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy blends technical IT experience, award-winning design skills, and deep subject matter expertise to produce Web portals and digital tools that allow our clients to deliver services and transfer knowledge widely, efficiently, and accurately.</p>
    </div>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SubjectMatterExperts.aspx.cs" Inherits="SubjectMatterExperts" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

<div class="container landing_padding">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
            <div class="basic_content">
                <h1>Subject Matter Experts</h1>
                	<p>We know that the qualifications and experience of our staff determine the success of every project for every client. We are committed to developing and retaining a highly skilled and credentialed staff with the experience necessary to meet project goals efficiently. The Synergy staff represents a powerful blend of subject matter expertise and experience:</p>
                	<p>Advanced degrees: 19 doctoral degrees and 34 master's degrees;</p>
                	<p>Certifications in conference planning, project management, and government meeting planning; and</p>
                	<p>IT credentials that include certifications in Information Systems Security, Information Security Management, Risk and Information Systems Control, and Cisco and Microsoft Network Certification.</p>
                	<p>Learn more about the expertise and contributions of some of our key staff:</p>
                <div class="sort_contain">
                    <div class="sort">
                        <h3>Filter By:</h3>
                    </div>
                    <div class="sort_box col-md-4">
                            <asp:DropDownList ID="ddlExpertise" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlExpertise_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Expertise</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div class="sort_box col-md-4">
                            <asp:DropDownList ID="ddlAgency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem>Agency/Office</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
        	<div class="sort_contain">
            	<h2><asp:Label ID="lblxyz" runat="server"></asp:Label> projects with <asp:Label ID="lblAgency" runat="server"></asp:Label></h2>
            </div>
        </div>
	</div>
</div>

<div class="container">
	<div class="row smrow">
	<asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
	<ItemTemplate>
    	<div class="col-md-4">
        	<div class="subject_card">
            	<h4><%# Eval("SubMatterExTitle") %></h4>
                	<ul>
                        <!-- start child repeater -->
                        <asp:Repeater ID="Repeater2" runat="server">
                        <ItemTemplate>                              
                        	<li>
                            	<%# Eval("Expertise")%>
                            </li>                               
                        </ItemTemplate>
                        </asp:Repeater>
                        <!-- end child repeater -->
                   	</ul>
            </div>
        </div>
	</ItemTemplate>
	</asp:Repeater>
    </div>
</div>

      
  
</asp:Content>


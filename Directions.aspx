﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Directions.aspx.cs" Inherits="Directions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="container landing_padding_double">
	<div class="row">
    	<div class="col-md-4">
        	<div class="addressinfo">
        		<h2>Main Office</h2>
					<p>Main Office<br />8757 Georgia Avenue<br />Suite 1440<br />Silver Spring, MD 20910<br /><br />Phone: (240) 485-1700<br />Fax: (240) 485-1717<br />E-mail: <a href="mailto:info@SEIservices.com" target="_blank">info@SEIservices.com</a></p>
        	</div>
        </div>
        <div class="col-md-8">
        	<div class="gettinghere">
            	<h3>Map Directions</h3>
                        <p>Click on the map to get door-to-door driving directions, <a href="https://www.google.com/maps?f=q&source=embed&hl=en&geocode&q=8757+Georgia+Avenue,+Silver+Spring,+MD&aq=1&sll=37.0625,-95.677068&sspn=53.477264,66.621094&ie=UTF8&hq&hnear=8757+Georgia+Ave,+Silver+Spring,+Montgomery,+Maryland+20910&ll=39.006579,-77.027893&spn=0.023344,0.036478&z=14&iwloc=A" target="_blank" title="View Larger Map">View Larger Map</a> or use the instructions below.</p>
                        <p>From points North, West, or East, take the Washington, DC Beltway (I-495), to exit 31, Georgia Avenue South. Synergy is located about 1 mile from the exit, just past Spring Street, on the left. Metered parking is available on Spring Street, or in the parking garage behind the Sheraton Hotel. </p>
                        <p>From Washington, DC, go North on North Capitol Street and continue onto Blair Road NW until you reach Georgia Avenue. Turn right onto Georgia Avenue and go approximately 1 mile. Synergy is located on the right. Metered parking is available on Spring Street or in the parking garage behind the Sheraton Hotel. </p>

                        <h4>Metro and Walking Directions</h4>
                        <p>Synergy is conveniently located 4 blocks from the Silver Spring Metro Station on the red line. Additionally, many buses drop off at the station and/or on Georgia Avenue near our office. </p>
                        <p>To walk to Synergy from the Metro station, exit on the bus' side and walk uphill on Colesville Road. Turn left onto Georgia Avenue. Synergy is located 3 blocks up on the right side of the street.</p>
            </div>
        </div>
    </div>
</div>

<div class="google-maps">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3100.7320594815783!2d-77.0299615!3d38.9986111!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c8b77a8313eb%3A0x885af858c6bbadb9!2s8757+Georgia+Ave%2C+Silver+Spring%2C+MD+20910!5e0!3m2!1sen!2sus!4v1429196859892" width="800" height="600" frameborder="0" style="border:0"></iframe>
</div>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MultimediaCommunication.aspx.cs" Inherits="MultimediaCommunications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   <style type="text/css">
             
       .expand 
       {
            cursor: pointer;
              }
              
       </style>

       <script type="text/javascript">

           function ExpandCollapse(theDiv) {
               el = document.getElementById(theDiv);
               if (el.style.display == 'none') { el.style.display = ''; }
               else { el.style.display = 'none'; } return false;

           }
  </script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="container landing_padding_double stick-wrapper">
        <div class="row">
            <div id="side-sticky" class="col-md-6">
              <div class="theiaStickySidebar"> 
                <h1 class="bigh1">Multimedia Communication</h1>
                <div class="cs_button_holder">
                    <asp:Button ID="btnViewCaseStudy" runat="server" Text="View Case Studies" OnClick="btnViewCaseStudy_Click" CssClass="btn btn-primary vcs_btn" />
                    <asp:Button ID="btnGetFactSheet" visible="false" runat="server" Text="Get Fact Sheet" OnClick="btnGetFactSheet_Click" CssClass="btn btn-success gfs_btn " />
                </div>
              </div>  
            </div>
            <div class="col-md-6" id="main-content">
                <p>Synergy produces award-winning communications products that bring our clients' visions to life on paper and on screen. Products include 2-D and 3-D animations, videos, and Public Service Announcements (PSAs) that take into account branding considerations and technical specifications. Our print packages meet all Government Printing Office (GPO) requirements. Every web page and document is tested to ensure compliance with Section 508 of the Rehabilitation Act of 1973.</p>
                
                
 <!-- Start Collapse-->

          
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Promotional Campaigns</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>Synergy provides integrated marketing and communications services that help our clients design, enhance, and promote national outreach campaigns. We lay the foundation for each campaign by developing a strategic communications and marketing plan (SCMP). These “blueprints” specify the steps necessary to reach audiences and change targeted attitudes and behaviors. We use a mixed methodology to gather information about the target audience, including interviews and environmental scans. The campaign’s branding is then customized to appeal to key audiences. In addition, Synergy’s communications strategies effectively mobilize partners and stakeholders to expand the reach of the campaign and enhance the impact of messaging. We have the capability to design, test, and evaluate products and conduct market analysis and testing upon request.   </p>

                                           
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Writing and Editing</a>
                                    
                                </h3>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p> Whether it is a congressionally mandated report, white paper, or educational video, our writers, editors, and graphic designers are equipped with the skills and expertise to create visually appealing multimedia products. Synergy’s researchers, writers, and editors bring a range of subject matter expertise to client projects; they have supported more than 20 federal agencies and have published in 200 journals, books, and magazines. They routinely draft and polish information tailored to a variety of audiences, whether creating a meeting summary or writing about complex technical and scientific subjects.  </p>
                                    <p> Synergy reviews all publications for grammar, style, consistency, and substance using agency-specific guides and industry-standard style manuals. We perform meticulous and accurate quality control reviews throughout the development process and prior to delivery of products to our clients.  </p>
                
                <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                </div>
                            </div>
                        </div>
                        
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Graphic Design</a>
                                </h3>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                   <p> Synergy’s staff of graphic designers ensures that communications products convey messages in a visually engaging and culturally sensitive format. Our graphic designers use a variety of state-of-the-art illustration tools to add clarity and style to a range of products, including reports, exhibits, campaign materials, books, conference materials, posters, and other promotional products.  </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h3 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Section 508 Compliance </a>
                                </h3>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <p> Synergy employs full-time Section 508 compliance staff members to ensure universal access to information housed on our clients’ government websites. Our web developers and designers, as well as graphic designers and publications specialists, continually evaluate the latest technical developments and trends in accessibility. These efforts ensure that we provide our clients with the best and most cost-effective methods for fully compliant static and dynamic web pages.  </p>

                                    <p> Our 508 specialists conduct quality assurance reviews using up-to-date versions of “gold standard” tools, including Job Access with Speech (JAWS) and Adobe Professional 508 compliance diagnostic software. We evaluate a full range of hardware compatibility considerations, such as usability testing with a keyboard and mouse. Additionally, we can provide clients with formal training or simple tips and guidance to help simplify 508 compliance testing and assurance.  </p>
                
                                    
                                    <h4 class="text-center xnormal"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><img src="images/closex.png" width="20" height="20" alt="close"/> Close</a></h4>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
<!--End Collapse -->

           

          </div>

    </div>


   <div class="expertise_stats_lander" data-0="background-position:0px 0px;" data-end="background-position:0px -500px;">
        <p>Synergy brings creativity and technical expertise to a full range of multimedia products.</p>
    </div>
</asp:Content>

